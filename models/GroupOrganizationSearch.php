<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * 团体组织搜索
 */
class GroupOrganizationSearch extends GroupOrganization
{

    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_members_count', 'person_members_count', 'director_count', 'executive_director_count', 'certificate_issue_datetime', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by', 'category'], 'integer'],
            [['name', 'english_name', 'english_short_name', 'address', 'legal_person_username', 'legal_person_mobile_phone', 'legal_person_work_information', 'certificate_number', 'intro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GroupOrganization::find();
        $where = ['type' => self::TYPE_GROUP];
        $identity = Yii::$app->getUser()->getIdentity();
        if ($identity->role == User::ROLE_USER) {
            $where['input_region_id'] = $identity->region_id;
        }
        $query->andWhere($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'company_members_count' => $this->company_members_count,
            'person_members_count' => $this->person_members_count,
            'director_count' => $this->director_count,
            'executive_director_count' => $this->executive_director_count,
            'certificate_issue_datetime' => $this->certificate_issue_datetime,
            'input_region_id' => $this->input_region_id,
            'enabled' => $this->enabled,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'english_name', $this->english_name])
            ->andFilterWhere(['like', 'english_short_name', $this->english_short_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'legal_person_username', $this->legal_person_username])
            ->andFilterWhere(['like', 'legal_person_mobile_phone', $this->legal_person_mobile_phone])
            ->andFilterWhere(['like', 'legal_person_work_information', $this->legal_person_work_information])
            ->andFilterWhere(['like', 'certificate_number', $this->certificate_number])
            ->andFilterWhere(['like', 'intro', $this->intro]);

        if ($this->category) {
            $query->andWhere(['IN', 'id', (new \yii\db\Query())->select('organization_id')->from('{{%organization_category}}')->where(['category_id' => $this->category])]);
        }

        \app\modules\admin\components\QueryCondition::set('Organization', $query);

        return $dataProvider;
    }

}
