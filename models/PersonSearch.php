<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Person;

/**
 * PersonSearch represents the model behind the search form about `app\models\Person`.
 */
class PersonSearch extends Person
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'category', 'sex', 'nation', 'full_time_education_level', 'in_service_education_level', 'enabled', 'region_id'], 'integer'],
            [['username', 'native_place', 'join_job_date', 'professional_title', 'mobile_phone', 'full_time_education_school', 'in_service_education_school', 'social_title', 'intro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();
        $identity = Yii::$app->getUser()->getIdentity();
        if ($identity->role == User::ROLE_USER) {
            $where = ['region_id' => $identity->region_id];
        } else {
            $where = [];
        }
        $query->andWhere($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            \app\modules\admin\components\QueryCondition::set('PERSON', $query);
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'category' => $this->category,
            'region_id' => $this->region_id,
            'sex' => $this->sex,
            'nation' => $this->nation,
            'full_time_education_level' => $this->full_time_education_level,
            'in_service_education_level' => $this->in_service_education_level,
            'enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'native_place', $this->native_place])
            ->andFilterWhere(['like', 'join_job_date', $this->join_job_date])
            ->andFilterWhere(['like', 'professional_title', $this->professional_title])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'full_time_education_school', $this->full_time_education_school])
            ->andFilterWhere(['like', 'in_service_education_school', $this->in_service_education_school])
            ->andFilterWhere(['like', 'social_title', $this->social_title])
            ->andFilterWhere(['like', 'intro', $this->intro]);

        \app\modules\admin\components\QueryCondition::set('PERSON', $query);

        return $dataProvider;
    }

}
