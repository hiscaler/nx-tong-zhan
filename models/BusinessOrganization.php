<?php

namespace app\models;

use Yii;

/**
 * 商会组织
 */
class BusinessOrganization extends Organization
{

    const CATEGORY_XIANG_ZHEN = 1; // 乡镇商会
    const CATEGORY_HANG_YE = 2; // 行业商会
    const CATEGORY_LIAN_HE = 3; // 联合商会
    const CATEGORY_YI_DI = 4; // 异地商会
    const CATEGORY_OTHER = 5; // 其他

    public static function categoryOptions()
    {
        return [
            static::CATEGORY_XIANG_ZHEN => '乡镇商会',
            static::CATEGORY_HANG_YE => '行业商会',
            static::CATEGORY_LIAN_HE => '联合商会',
            static::CATEGORY_YI_DI => '异地商会',
            static::CATEGORY_OTHER => '其他',
        ];
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->type = self::TYPE_BUSINESS;
            }

            return true;
        } else {
            return false;
        }
    }

}
