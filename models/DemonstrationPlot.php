<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "www_demonstration_plot".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $region_id
 * @property string $name
 * @property string $address
 * @property string $elements
 * @property integer $county_level_awarding_datetime
 * @property integer $city_level_awarding_datetime
 * @property integer $province_level_awarding_datetime
 * @property string $superintendent_informations
 * @property string $superintendent_mobile_phone
 * @property string $honors_awards
 * @property string $intro
 * @property integer $input_region_id
 * @property integer $enabled
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class DemonstrationPlot extends \yii\db\ActiveRecord
{

    const TYPE_TONG_XIN = 1; // “同心园区”“同心项目”“同心社区”“同心乡村”
    const TYPE_1035 = 2; //十三五
    const TYPE_OTHER = 3; //其他

    /**
     * 所属类型
     *
     * @var array
     */

    public $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%demonstration_plot}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'region_id', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'address', 'elements', 'superintendent_informations', 'superintendent_mobile_phone', 'intro'], 'required'],
            [['county_level_awarding_datetime', 'city_level_awarding_datetime', 'province_level_awarding_datetime'], 'date', 'format' => 'php:Y-m-d'],
            [['honors_awards', 'intro'], 'string'],
            [['name'], 'string', 'max' => 30],
            [['address', 'superintendent_informations'], 'string', 'max' => 100],
            [['elements'], 'string', 'max' => 200],
            [['superintendent_mobile_phone'], 'string', 'max' => 12],
            ['category', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => '类型',
            'category' => '类别',
            'region_id' => '所属乡镇（街道）、园区',
            'region.name' => '所属乡镇（街道）、园区',
            'name' => '创建点名称',
            'address' => '详细地址',
            'elements' => '统战元素',
            'county_level_awarding_datetime' => '县级授牌时间',
            'city_level_awarding_datetime' => '市级授牌时间',
            'province_level_awarding_datetime' => '省级授牌时间',
            'superintendent_informations' => '负责人姓名及职务',
            'superintendent_mobile_phone' => '联系方式',
            'honors_awards' => '所获荣誉奖励',
            'intro' => '创建工作情况简介',
            'input_region_id' => '录入单位',
            'inputRegion.name' => '录入单位',
            'enabled' => '激活',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'updated_at' => '更新时间',
            'updated_by' => '更新人',
        ];
    }

    /**
     * 分类名称集合
     *
     * @return string
     */
    public function getCategoryNames()
    {
        $names = [];
        $options = static::categoryOptions();
        $categories = Yii::$app->getDb()->createCommand('SELECT [[category_id]] FROM {{%demonstration_plot_category}} WHERE [[demonstration_plot_id]] = :id', [':id' => $this->id])->queryColumn();
        foreach ($categories as $categoryId) {
            $names[] = $options[$categoryId];
        }

        return implode('、', $names);
    }

    /**
     * 所属乡镇
     *
     * @return ActiveRecord
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * 录入单位
     *
     * @return ActiveRecord
     */
    public function getInputRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'input_region_id']);
    }

    // Events
    public function afterFind()
    {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->category = Yii::$app->getDb()->createCommand('SELECT [[category_id]] FROM {{%demonstration_plot_category}} WHERE [[demonstration_plot_id]] = :demonstrationPlotId', [':demonstrationPlotId' => $this->id])->queryColumn();
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->input_region_id = Yii::$app->getUser()->getIdentity()->region_id;
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = Yii::$app->getUser()->getId();
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            $this->county_level_awarding_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->county_level_awarding_datetime);
            $this->city_level_awarding_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->city_level_awarding_datetime);
            $this->province_level_awarding_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->province_level_awarding_datetime);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $personTypes = $this->category;
        if (!is_array($personTypes)) {
            $personTypes = [];
        }
        $cmd = Yii::$app->getDb()->createCommand();
        if (!$insert) {
            $cmd->delete('{{%demonstration_plot_category}}', ['demonstration_plot_id' => $this->id])->execute();
        }
        $batchInsertRows = [];
        foreach ($personTypes as $type) {
            $batchInsertRows[] = [$this->id, $type];
        }
        $cmd->batchInsert('{{%demonstration_plot_category}}', ['demonstration_plot_id', 'category_id'], $batchInsertRows)->execute();
    }

}
