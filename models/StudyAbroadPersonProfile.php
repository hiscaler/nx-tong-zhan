<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%study_abroad_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $country
 * @property string $school
 * @property integer $type
 */
class StudyAbroadPersonProfile extends \yii\db\ActiveRecord
{

    const TYPE_PUBLIC = 1;
    const TYPE_PRIVATE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%study_abroad_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'school'], 'required'],
            [['person_id', 'type'], 'integer'],
            [['country'], 'string', 'max' => 10],
            [['school'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'country' => '留学国家',
            'school' => '留学学校',
            'type' => '留学类别',
        ];
    }

    /**
     * 留学类别
     * @return array
     */
    public static function typeOptions()
    {
        return [
            self::TYPE_PUBLIC => '公费',
            self::TYPE_PRIVATE => '自费',
        ];
    }

}
