<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person_family_social_relation}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $person_id
 * @property string $title
 * @property string $username
 * @property integer $birthday
 * @property integer $party
 * @property string $work_information
 * @property string $tel
 */
class PersonFamilySocialRelation extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person_family_social_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'person_id', 'party'], 'integer'],
            [['person_id', 'title', 'username'], 'required'],
            [['title', 'username'], 'string', 'max' => 10],
            [['work_information', 'tel'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => '类型',
            'person_id' => '人员 id',
            'title' => '称谓',
            'username' => '姓名',
            'birthday' => '出生年月',
            'party' => '党派',
            'work_information' => '工作单位及职务',
            'tel' => '联系电话',
        ];
    }

}
