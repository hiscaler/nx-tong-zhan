<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%religion_activity_place}}".
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $name
 * @property string $address
 * @property integer $staff_people_count
 * @property integer $other_people_count
 * @property string $superintendent_informations
 * @property string $superintendent_mobile_phone
 * @property string $certificate_number
 * @property integer $certificate_issue_datetime
 * @property string $historical_relic_protected_level
 * @property string $intro
 * @property integer $input_region_id
 * @property integer $enabled
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ReligionActivityPlace extends \yii\db\ActiveRecord
{

    const CATEGORY_FO_JIAO = 1; // 佛教活动场所
    const CATEGORY_DAO_JIAO = 2; // 道教活动场所
    const CATEGORY_JI_DU_JIAO = 3; // 基督教活动场所
    const CATEGORY_MING_JIAN_XIN_YANG = 4; // 民间信仰活动场所

    public static function categoryOptions()
    {
        return [
            static::CATEGORY_FO_JIAO => '佛教活动场所',
            static::CATEGORY_DAO_JIAO => '道教活动场所',
            static::CATEGORY_JI_DU_JIAO => '基督教活动场所',
            static::CATEGORY_MING_JIAN_XIN_YANG => '民间信仰活动场所',
        ];
    }

    /**
     * 所属类别
     *
     * @var array
     */
    public $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%religion_activity_place}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'superintendent_informations', 'superintendent_mobile_phone'], 'required'],
            [['region_id', 'staff_people_count', 'other_people_count', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['staff_people_count', 'other_people_count'], 'default', 'value' => 0],
            [['enabled'], 'default', 'value' => Constant::BOOLEAN_TRUE],
            [['certificate_issue_datetime'], 'date', 'format' => 'php:Y-m-d'],
            [['intro'], 'string'],
            [['name', 'certificate_number', 'historical_relic_protected_level'], 'string', 'max' => 30],
            [['address', 'superintendent_informations'], 'string', 'max' => 100],
            [['superintendent_mobile_phone'], 'string', 'max' => 12],
            [['category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'region_id' => '所属乡镇',
            'region.name' => '所属乡镇',
            'name' => '场所名称',
            'category' => '类别',
            'address' => '详细地址',
            'staff_people_count' => '教职人员数',
            'other_people_count' => '其他人员数',
            'superintendent_informations' => '负责人姓名及职务',
            'superintendent_mobile_phone' => '联系方式',
            'certificate_number' => '登记证书号',
            'certificate_issue_datetime' => '发证日期',
            'historical_relic_protected_level' => '文物保护级别',
            'intro' => '场所情况简介',
            'input_region_id' => '录入单位',
            'inputRegion.name' => '录入单位',
            'enabled' => '激活',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'updated_at' => '更新时间',
            'updated_by' => '更新人',
        ];
    }

    /**
     * 分类名称集合
     *
     * @return string
     */
    public function getCategoryNames()
    {
        $names = [];
        $options = static::categoryOptions();
        $categories = Yii::$app->getDb()->createCommand('SELECT [[category_id]] FROM {{%religion_activity_place_category}} WHERE [[place_id]] = :id', [':id' => $this->id])->queryColumn();
        foreach ($categories as $categoryId) {
            $names[] = $options[$categoryId];
        }

        return implode('、', $names);
    }

    /**
     * 所属乡镇
     *
     * @return ActiveRecord
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * 录入单位
     *
     * @return ActiveRecord
     */
    public function getInputRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'input_region_id']);
    }

    // Events
    public function afterFind()
    {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->category = Yii::$app->getDb()->createCommand('SELECT [[category_id]] FROM {{%demonstration_plot_category}} WHERE [[demonstration_plot_id]] = :demonstrationPlotId', [':demonstrationPlotId' => $this->id])->queryColumn();
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->input_region_id = Yii::$app->getUser()->getIdentity()->region_id;
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = Yii::$app->getUser()->getId();
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            $this->certificate_issue_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->certificate_issue_datetime);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $categories = $this->category;
        if (!is_array($categories)) {
            $categories = [];
        }
        $cmd = Yii::$app->getDb()->createCommand();
        if (!$insert) {
            $cmd->delete('{{%religion_activity_place_category}}', ['place_id' => $this->id])->execute();
        }
        $batchInsertRows = [];
        foreach ($categories as $category) {
            $batchInsertRows[] = [$this->id, $category];
        }
        $cmd->batchInsert('{{%religion_activity_place_category}}', ['place_id', 'category_id'], $batchInsertRows)->execute();
    }

}
