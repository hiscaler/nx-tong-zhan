<?php

namespace app\models;

/**
 * 少数民族人士
 */
class MinorityPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_SHAO_SHU_MING_ZU],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
