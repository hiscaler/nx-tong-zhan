<?php

namespace app\models;

/**
 * 非公有制经济代表人士
 */
class NonpublicEconomicPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'work_information' => '创办公司名称',
            'professional_title' => '担任职务',
        ]);
    }

}
