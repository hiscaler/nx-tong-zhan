<?php

namespace app\models;

use Yii;

class Option
{

    /**
     * Boolean options
     * @return array
     */
    public static function booleanOptions()
    {
        return [
            Constant::BOOLEAN_FALSE => '否',
            Constant::BOOLEAN_TRUE => '是',
        ];
    }

    /**
     * 排序下拉列表框数据
     * @param integer $start
     * @param integer $max
     * @return array
     */
    public static function orderingOptions($start = 0, $max = 60)
    {
        $options = [];
        for ($i = $start; $i <= $max; $i++) {
            $options[$i] = $i;
        }
        return $options;
    }

    /**
     * 人员类型
     * @return array
     */
    public static function personTypeOptions()
    {
        return [
            Constant::TYPE_MING_ZHU_DANG_PAI => '民主党派成员',
            Constant::TYPE_WU_DANG_PAI => '无党派人士',
            Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI => '党外知识分子',
            Constant::TYPE_SHAO_SHU_MING_ZU => '少数民族人士',
            Constant::TYPE_ZONG_JIAO_JIE => '宗教界人士',
            Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO => '非公有制经济代表人士',
            Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI => '新的社会阶层人士',
            Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN => '出国和归国留学人员',
            Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO => '香港同胞、澳门同胞',
            Constant::TYPE_TAI_BAO_TAI_SHU => '台胞台属',
            Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN => '华侨、归侨、侨眷',
            Constant::TYPE_OTHER => '其他需要联系和团结的人员',
        ];
    }

    public static function sexOptions()
    {
        return [
            Constant::SEX_FEMALE => '女',
            Constant::SEX_MALE => '男',
        ];
    }

    /**
     * 学历选项
     *
     * @return array
     */
    public static function educationLevelOptions()
    {
        return [
            Constant::EDUCATION_LEVEL_UNKNOWN => '',
            Constant::EDUCATION_LEVEL_XIAO_XUE => '小学',
            Constant::EDUCATION_LEVEL_CHU_ZHONG => '初中',
            Constant::EDUCATION_LEVEL_GAO_ZHONG => '高中',
            Constant::EDUCATION_LEVEL_ZHONG_ZHUAN => '中专',
            Constant::EDUCATION_LEVEL_DA_ZHUANG => '大专',
            Constant::EDUCATION_LEVEL_BEN_KE => '本科',
            Constant::EDUCATION_LEVEL_YANG_JIU_SHENG => '研究生',
            Constant::EDUCATION_LEVEL_BO_SHI => '博士',
            Constant::EDUCATION_LEVEL_BO_SHI_HOU => '博士后',
        ];
    }

    /**
     * 民族选项
     * @return array
     */
    public static function nationOptions()
    {
        return [
            Constant::NATION_HAN_ZU => '汉族',
            Constant::NATION_ZHUAN_ZU => '壮族',
            Constant::NATION_MAN_ZU => '满族',
            Constant::NATION_HUI_ZU => '回族',
            Constant::NATION_MIAO_ZU => '苗族',
            Constant::NATION_WEI_WU_ER_ZU => '维吾尔族',
            Constant::NATION_TU_JIA_ZU => '土家族',
            Constant::NATION_YI_ZU => '彝族',
            Constant::NATION_MENG_GU_ZU => '蒙古族',
            Constant::NATION_ZAN_ZU => '藏族',
            Constant::NATION_BU_YI_ZU => '布依族',
            Constant::NATION_TONG_ZU => '侗族',
            Constant::NATION_YAO_ZU => '瑶族',
            Constant::NATION_CHAO_XIAN_ZU => '朝鲜族',
            Constant::NATION_BAI_ZU => '白族',
            Constant::NATION_HA_NI_ZU => '哈尼族',
            Constant::NATION_HA_AS_KE_ZU => '哈萨克族',
            Constant::NATION_LI_ZU => '黎族',
            Constant::NATION_DAI_ZU => '傣族',
            Constant::NATION_SHE_ZU => '畲族',
            Constant::NATION_LI_LI_ZU => '傈僳族',
            Constant::NATION_QI_LAO_ZU => '仡佬族',
            Constant::NATION_DONG_XIANG_ZU => '东乡族',
            Constant::NATION_GAO_SHANG_ZU => '高山族',
            Constant::NATION_LA_GU_ZU => '拉祜族',
            Constant::NATION_SHUI_ZU => '水族',
            Constant::NATION_WA => '佤族',
            Constant::NATION_NA_XI_ZU => '纳西族',
            Constant::NATION_JIANG_ZU => '羌族',
            Constant::NATION_TU_ZU => '土族',
            Constant::NATION_MU_LAO_ZU => '仫佬族',
            Constant::NATION_XI_BO_ZU => '锡伯族',
            Constant::NATION_KE_ER_KE_TZU_ZU => '柯尔克孜族',
            Constant::NATION_DA_HANG_ER_ZU => '达斡尔族',
            Constant::NATION_JING_PO_ZU => '景颇族',
            Constant::NATION_MAO_NANG_ZU => '毛南族',
            Constant::NATION_SA_LA_ZU => '撒拉族',
            Constant::NATION_BU_LANG_ZU => '布朗族',
            Constant::NATION_TA_JI_KE_ZU => '塔吉克族',
            Constant::NATION_A_CHANG_ZU => '阿昌族',
            Constant::NATION_PU_MI_ZU => '普米族',
            Constant::NATION_ER_WEN_KE_ZU => '鄂温克族',
            Constant::NATION_NV_ZU => '怒族',
            Constant::NATION_JING_ZU => '京族',
            Constant::NATION_JI_NUO_ZU => '基诺族',
            Constant::NATION_DE_AN_ZU => '德昂族',
            Constant::NATION_BAO_AN_ZU => '保安族',
            Constant::NATION_ER_LUO_SI_ZU => '俄罗斯族',
            Constant::NATION_YU_GU_ZU => '裕固族',
            Constant::NATION_WU_SUO_BIE_KE_ZU => '乌孜别克族',
            Constant::NATION_MENG_BA_ZU => '门巴族',
            Constant::NATION_ER_LUNG_CHUN_ZU => '鄂伦春族',
            Constant::NATION_DU_LONG_ZU => '独龙族',
            Constant::NATION_TA_TA_ER_ZU => '塔塔尔族',
            Constant::NATION_HE_ZHE_ZU => '赫哲族',
            Constant::NATION_LUO_BA_ZU => '珞巴族',
        ];
    }

    /**
     * 国籍选项
     * @return array
     */
    public static function nationalityOptions()
    {
        return [
            Constant::NATIONALITY_PRC => '中国',
            Constant::NATIONALITY_FOREIGN_COUNTRY => '外国',
        ];
    }

    /**
     * 党派选项
     * @return array
     */
    public static function partyOptions($all = true)
    {
        $items = [
            Constant::PARTY_ZGGMD => '中国国民党革命委员会',
            Constant::PARTY_ZGMZTM => '中国民主同盟',
            Constant::PARTY_ZGMZJGH => '中国民主建国会',
            Constant::PARTY_ZGMZCJH => '中国民主促进会',
            Constant::PARTY_ZGNGMZD => '中国农工民主党',
            Constant::PARTY_ZGZCD => '中国致公党',
            Constant::PARTY_93XS => '九三学社',
            Constant::PARTY_TWMZZZTM => '台湾民主自治同盟',
        ];
        if ($all) {
            $items[Constant::PARTY_GCD] = '中国共产党';
            $items[Constant::PARTY_WDP] = '无党派';
        }

        return $items;
    }

    /**
     * 宗教选项
     * @return array
     */
    public static function religionOptions()
    {
        $items = [
            Constant::RELIGION_JDJ => '基督教',
            Constant::RELIGION_YSLJ => '伊斯兰教',
            Constant::RELIGION_FJ => '佛教',
            Constant::RELIGION_YJ => '儒教',
            Constant::RELIGION_DJ => '道教',
        ];

        return $items;
    }

}
