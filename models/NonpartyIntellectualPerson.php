<?php

namespace app\models;

/**
 * 党外知识分子
 */
class NonpartyIntellectualPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
