<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%democratic_party_organization}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $found_datetime
 * @property string $address
 * @property integer $people_count
 * @property string $honour_reward
 * @property string $superintendent_username
 * @property string $superintendent_mobile_phone
 * @property string $superintendent_work_information
 * @property string $political_arrangements
 * @property string $intro
 * @property integer $input_region_id
 * @property integer $enabled
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class DemocraticPartyOrganization extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%democratic_party_organization}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'found_datetime', 'address', 'superintendent_username', 'superintendent_mobile_phone', 'superintendent_work_information'], 'required'],
            [['people_count', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['found_datetime'], 'date', 'format' => 'php:Y-m-d'],
            [['intro'], 'string'],
            [['name', 'address'], 'string', 'max' => 100],
            [['honour_reward', 'political_arrangements'], 'string', 'max' => 255],
            [['superintendent_username'], 'string', 'max' => 16],
            [['superintendent_mobile_phone'], 'string', 'max' => 12],
            [['superintendent_work_information'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'found_datetime' => '成立时间',
            'address' => '办公（活动）场所',
            'people_count' => '党（会）员人数',
            'honour_reward' => '所获荣誉奖励',
            'superintendent_username' => '负责人姓名',
            'superintendent_mobile_phone' => '联系方式',
            'superintendent_work_information' => '负责人工作单位及职务',
            'political_arrangements' => '政治安排',
            'intro' => '简介',
            'input_region_id' => '录入单位',
            'enabled' => '激活',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'updated_at' => '更新时间',
            'updated_by' => '更新人',
        ];
    }

    // 关联关系定义
    public function getInputRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'input_region_id']);
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->input_region_id = Yii::$app->getUser()->getIdentity()->region_id;
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = Yii::$app->getUser()->getId();
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            $this->found_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->found_datetime);

            return true;
        } else {
            return false;
        }
    }

}
