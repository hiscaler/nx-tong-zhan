<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DemocraticPartyOrganization;

/**
 * DemocraticPartyOrganizationSearch represents the model behind the search form about `app\models\DemocraticPartyOrganization`.
 */
class DemocraticPartyOrganizationSearch extends DemocraticPartyOrganization
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'found_datetime', 'people_count', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'address', 'honour_reward', 'superintendent_username', 'superintendent_mobile_phone', 'superintendent_work_information', 'political_arrangements', 'intro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DemocraticPartyOrganization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'found_datetime' => $this->found_datetime,
            'people_count' => $this->people_count,
            'input_region_id' => $this->input_region_id,
            'enabled' => $this->enabled,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'honour_reward', $this->honour_reward])
            ->andFilterWhere(['like', 'superintendent_username', $this->superintendent_username])
            ->andFilterWhere(['like', 'superintendent_mobile_phone', $this->superintendent_mobile_phone])
            ->andFilterWhere(['like', 'superintendent_work_information', $this->superintendent_work_information])
            ->andFilterWhere(['like', 'political_arrangements', $this->political_arrangements])
            ->andFilterWhere(['like', 'intro', $this->intro]);

        \app\modules\admin\components\QueryCondition::set('DemocraticPartyOrganization', $query);

        return $dataProvider;
    }

}
