<?php

namespace app\models;

/**
 * “同心园区”“同心项目”“同心社区”“同心乡村”信息登记表
 */
class DemonstrationPlotTongXin extends DemonstrationPlot
{

    const CATEGORY_PROVINCE_YQ = 1; // 省级同心园区
    const CATEGORY_CITY_YQ = 2; // 市级同心园区
    const CATEGORY_COUNTY_YQ = 3; // 县级同心园区
    const CATEGORY_PROVINCE_XM = 4; // 省级同心项目
    const CATEGORY_CITY_XM = 5; // 市级同心项目
    const CATEGORY_COUNTY_XM = 6; // 县级同心项目
    const CATEGORY_PROVINCE_SQ = 7; // 省级同心社区
    const CATEGORY_CITY_SQ = 8; // 市级同心社区
    const CATEGORY_COUNTY_SQ = 9; // 县级同心社区
    const CATEGORY_PROVINCE_XC = 10; // 省级同心乡村
    const CATEGORY_CITY_XC = 11; // 市级同心乡村
    const CATEGORY_COUNTY_XC = 12; // 县级同心乡村

    public static function categoryOptions()
    {
        return [
            static::CATEGORY_PROVINCE_YQ => '省级同心园区',
            static::CATEGORY_CITY_YQ => '市级同心园区',
            static::CATEGORY_COUNTY_YQ => '县级同心园区',
            static::CATEGORY_PROVINCE_XM => '省级同心项目',
            static::CATEGORY_CITY_XM => '市级同心项目',
            static::CATEGORY_COUNTY_XM => '县级同心项目',
            static::CATEGORY_PROVINCE_SQ => '省级同心社区',
            static::CATEGORY_CITY_SQ => '市级同心社区',
            static::CATEGORY_COUNTY_SQ => '县级同心社区',
            static::CATEGORY_PROVINCE_XC => '省级同心乡村',
            static::CATEGORY_CITY_XC => '市级同心乡村',
            static::CATEGORY_COUNTY_XC => '县级同心乡村',
        ];
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->type = self::TYPE_TONG_XIN;
            }

            return true;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), []);
    }

}
