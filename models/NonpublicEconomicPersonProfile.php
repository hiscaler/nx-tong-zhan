<?php

namespace app\models;

/**
 * This is the model class for table "{{%nonpublic_economic_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $evaluation_date
 * @property string $evaluation_level
 * @property string $enterprise_intro
 */
class NonpublicEconomicPersonProfile extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%nonpublic_economic_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['person_id'], 'required'],
            [['person_id'], 'integer'],
            [['evaluation_date', 'evaluation_level', 'enterprise_intro'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'evaluation_date' => '综合评价时间',
            'evaluation_level' => '综合评价等级',
            'enterprise_intro' => '公司情况介绍',
        ];
    }

}
