<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DemonstrationPlotSearch represents the model behind the search form about `app\models\DemonstrationPlot`.
 */
class DemonstrationPlotOtherSearch extends DemonstrationPlotOther
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'county_level_awarding_datetime', 'city_level_awarding_datetime', 'province_level_awarding_datetime', 'input_region_id', 'enabled', 'category'], 'integer'],
            [['name', 'address', 'elements', 'superintendent_informations', 'superintendent_mobile_phone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DemonstrationPlotOther::find();
        $where = ['type' => self::TYPE_OTHER];
        $identity = Yii::$app->getUser()->getIdentity();
        if ($identity->role == User::ROLE_USER) {
            $where['region_id'] = $identity->region_id;
        }
        $query->andWhere($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'county_level_awarding_datetime' => $this->county_level_awarding_datetime,
            'city_level_awarding_datetime' => $this->city_level_awarding_datetime,
            'province_level_awarding_datetime' => $this->province_level_awarding_datetime,
            'input_region_id' => $this->input_region_id,
            'enabled' => $this->enabled,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'elements', $this->elements])
            ->andFilterWhere(['like', 'superintendent_informations', $this->superintendent_informations])
            ->andFilterWhere(['like', 'superintendent_mobile_phone', $this->superintendent_mobile_phone]);

        if ($this->category) {
            $query->andWhere(['IN', 'id', (new \yii\db\Query())->select('demonstration_plot_id')->from('{{%demonstration_plot_category}}')->where(['category_id' => (int) $this->category])]);
        }

        \app\modules\admin\components\QueryCondition::set('DemonstrationPlot', $query);

        return $dataProvider;
    }

}
