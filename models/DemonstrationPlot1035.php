<?php

namespace app\models;

/**
 * “十三五”行动示范点信息登记表
 */
class DemonstrationPlot1035 extends DemonstrationPlot
{

    const CATEGORY_PROVINCE = 1; // 省级示范点
    const CATEGORY_CITY = 2; // 市级示范点
    const CATEGORY_COUNTY = 3; // 县级示范点

    public static function categoryOptions()
    {
        return [
            static::CATEGORY_PROVINCE => '省级示范点',
            static::CATEGORY_CITY => '市级示范点',
            static::CATEGORY_COUNTY => '县级示范点',
        ];
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->type = self::TYPE_1035;
            }

            return true;
        } else {
            return false;
        }
    }

}
