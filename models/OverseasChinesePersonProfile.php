<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%overseas_chinese_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $leave_date
 * @property string $living_place
 * @property string $return_date
 * @property string $live_address
 * @property string $tel
 * @property string $speciality
 */
class OverseasChinesePersonProfile extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%overseas_chinese_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id'], 'integer'],
            [['leave_date', 'return_date', 'tel'], 'string', 'max' => 20],
            [['living_place'], 'string', 'max' => 100],
            [['live_address'], 'string', 'max' => 60],
            [['speciality'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'leave_date' => '何时出境',
            'living_place' => '旅居地',
            'return_date' => '归国时间',
            'live_address' => '现居住地址',
            'tel' => '固定电话',
            'speciality' => '熟悉专长或专攻方向',
        ];
    }

}
