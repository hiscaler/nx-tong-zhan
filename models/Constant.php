<?php

namespace app\models;

/**
 * 常量定义
 * @package app\models
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class Constant
{

    /**
     * 布尔值定义
     */
    const BOOLEAN_FALSE = 0;
    const BOOLEAN_TRUE = 1;

    /**
     * 人员类型
     */
    const TYPE_MING_ZHU_DANG_PAI = 1;
    const TYPE_WU_DANG_PAI = 2;
    const TYPE_DANG_WAI_ZHI_SHI_FEN_ZI = 3;
    const TYPE_SHAO_SHU_MING_ZU = 4;
    const TYPE_ZONG_JIAO_JIE = 5;
    const TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO = 6;
    const TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI = 7;
    const TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN = 8;
    const TYPE_XIANG_GANG_AO_MENG_TONG_BAO = 9;
    const TYPE_TAI_BAO_TAI_SHU = 10;
    const TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN = 11;
    const TYPE_OTHER = 12;

    /**
     * 性别
     */
    const SEX_FEMALE = 0;
    const SEX_MALE = 1;

    /**
     * 民族
     */
    const NATION_HAN_ZU = 1; // 汉族
    const NATION_ZHUAN_ZU = 2; // 壮族
    const NATION_MAN_ZU = 3; // 满族
    const NATION_HUI_ZU = 4; // 回族
    const NATION_MIAO_ZU = 5; // 苗族
    const NATION_WEI_WU_ER_ZU = 6; // 维吾尔族
    const NATION_TU_JIA_ZU = 7; // 土家族
    const NATION_YI_ZU = 8; // 彝族
    const NATION_MENG_GU_ZU = 9; // 蒙古族
    const NATION_ZAN_ZU = 10; // 藏族
    const NATION_BU_YI_ZU = 11; // 布依族
    const NATION_TONG_ZU = 12; // 侗族
    const NATION_YAO_ZU = 13; // 瑶族
    const NATION_CHAO_XIAN_ZU = 14; // 朝鲜族
    const NATION_BAI_ZU = 15; // 白族
    const NATION_HA_NI_ZU = 16; // 哈尼族
    const NATION_HA_AS_KE_ZU = 17; // 哈萨克族
    const NATION_LI_ZU = 18; // 黎族
    const NATION_DAI_ZU = 19; // 傣族
    const NATION_SHE_ZU = 20; // 畲族
    const NATION_LI_LI_ZU = 21; // 傈僳族
    const NATION_QI_LAO_ZU = 22; // 仡佬族
    const NATION_DONG_XIANG_ZU = 23; // 东乡族
    const NATION_GAO_SHANG_ZU = 24; // 高山族
    const NATION_LA_GU_ZU = 25; // 拉祜族
    const NATION_SHUI_ZU = 26; // 水族
    const NATION_WA = 27; // 佤族
    const NATION_NA_XI_ZU = 28; // 纳西族
    const NATION_JIANG_ZU = 29; // 羌族
    const NATION_TU_ZU = 30; // 土族
    const NATION_MU_LAO_ZU = 31; // 仫佬族
    const NATION_XI_BO_ZU = 32; // 锡伯族
    const NATION_KE_ER_KE_TZU_ZU = 33; // 柯尔克孜族
    const NATION_DA_HANG_ER_ZU = 34; // 达斡尔族
    const NATION_JING_PO_ZU = 35; // 景颇族
    const NATION_MAO_NANG_ZU = 36; // 毛南族
    const NATION_SA_LA_ZU = 37; // 撒拉族
    const NATION_BU_LANG_ZU = 38; // 布朗族
    const NATION_TA_JI_KE_ZU = 39; // 塔吉克族
    const NATION_A_CHANG_ZU = 40; // 阿昌族
    const NATION_PU_MI_ZU = 41; // 普米族
    const NATION_ER_WEN_KE_ZU = 42; // 鄂温克族
    const NATION_NV_ZU = 43; // 怒族
    const NATION_JING_ZU = 44; // 京族
    const NATION_JI_NUO_ZU = 45; // 基诺族
    const NATION_DE_AN_ZU = 46; // 德昂族
    const NATION_BAO_AN_ZU = 47; // 保安族
    const NATION_ER_LUO_SI_ZU = 48; // 俄罗斯族
    const NATION_YU_GU_ZU = 49; // 裕固族
    const NATION_WU_SUO_BIE_KE_ZU = 50; // 乌孜别克族
    const NATION_MENG_BA_ZU = 51; // 门巴族
    const NATION_ER_LUNG_CHUN_ZU = 52; // 鄂伦春族
    const NATION_DU_LONG_ZU = 53; // 独龙族
    const NATION_TA_TA_ER_ZU = 54; // 塔塔尔族
    const NATION_HE_ZHE_ZU = 55; // 赫哲族
    const NATION_LUO_BA_ZU = 56; // 珞巴族

    /**
     * 学历
     */
    const EDUCATION_LEVEL_UNKNOWN = 0;
    const EDUCATION_LEVEL_XIAO_XUE = 1;
    const EDUCATION_LEVEL_CHU_ZHONG = 2;
    const EDUCATION_LEVEL_GAO_ZHONG = 3;
    const EDUCATION_LEVEL_ZHONG_ZHUAN = 4;
    const EDUCATION_LEVEL_DA_ZHUANG = 5;
    const EDUCATION_LEVEL_BEN_KE = 6;
    const EDUCATION_LEVEL_YANG_JIU_SHENG = 7;
    const EDUCATION_LEVEL_BO_SHI = 8;
    const EDUCATION_LEVEL_BO_SHI_HOU = 9;

    /**
     * 国籍
     */
    const NATIONALITY_PRC = 1;  // 中国
    const NATIONALITY_FOREIGN_COUNTRY = 2;  // 外国

    /**
     * 党派
     */
    const PARTY_UNKNOWN = 0; // 未知
    const PARTY_ZGGMD = 1;  // 中国国民党革命委员会
    const PARTY_ZGMZTM = 2;  // 中国民主同盟
    const PARTY_ZGMZJGH = 3;  // 中国民主建国会
    const PARTY_ZGMZCJH = 4;  // 中国民主促进会
    const PARTY_ZGNGMZD = 5;  // 中国农工民主党
    const PARTY_ZGZCD = 6;  // 中国致公党
    const PARTY_93XS = 7;  // 九三学社
    const PARTY_TWMZZZTM = 8;  // 台湾民主自治同盟
    const PARTY_GCD = 9;  // 中国共产党
    const PARTY_WDP = 10;  // 无党派

    /**
     * 宗教
     */
    const RELIGION_JDJ = 1; //基督教
    const RELIGION_YSLJ = 2; //伊斯兰教
    const RELIGION_FJ = 3; //佛教
    const RELIGION_YJ = 4; //儒教
    const RELIGION_DJ = 5; //道教

    /**
     * 家庭成员关系
     */
    const FAMILY_SOCIAL_RELATIONS_PRIMARY = 0;
    const FAMILY_SOCIAL_RELATIONS_SECONDARY = 1;

}
