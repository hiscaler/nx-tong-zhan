<?php

namespace app\models;

/**
 * 新的社会阶层人士
 */
class NewSocialStratumPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
