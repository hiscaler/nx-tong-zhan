<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%hk_macao_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $main_live_address
 * @property string $tel
 * @property string $speciality
 */
class HkMacaoPersonProfile extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%hk_macao_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id'], 'integer'],
            [['main_live_address'], 'string', 'max' => 100],
            [['tel'], 'string', 'max' => 20],
            [['speciality'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'main_live_address' => '现主要居住地',
            'tel' => '固定电话',
            'speciality' => '熟悉专业有何特长',
        ];
    }

}
