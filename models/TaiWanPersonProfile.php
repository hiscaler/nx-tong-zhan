<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tai_wan_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $tel
 * @property string $live_address
 * @property string $speciality
 */
class TaiWanPersonProfile extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tai_wan_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id'], 'integer'],
            [['tel'], 'string', 'max' => 20],
            [['live_address'], 'string', 'max' => 60],
            [['speciality'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'tel' => '固定电话',
            'live_address' => '现居住地',
            'speciality' => '熟悉专长或专攻方向',
        ];
    }

}
