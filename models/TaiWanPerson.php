<?php

namespace app\models;

/**
 * 台胞（台属）
 */
class TaiWanPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_TAI_BAO_TAI_SHU],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id_card_number' => '证件',
            'intro' => '台胞个人简介（是企业负责人的含企业情况简介）',
        ]);
    }

}
