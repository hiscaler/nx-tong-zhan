<?php

namespace app\models;

use app\models\Region;
use yadjet\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class RegionSearch extends Region
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'level', 'status'], 'integer'],
            [['alias', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $rawData = (new Query())->from('{{%region}}')->orderBy(['ordering' => SORT_ASC])->all();

        if ($rawData) {
            $rawData = static::sortItems(['children' => ArrayHelper::toTree($rawData, 'id')]);
            unset($rawData[0]);
        }

        return new ArrayDataProvider([
            'allModels' => $rawData,
            'key' => 'id',
            'pagination' => false,
        ]);
    }

}
