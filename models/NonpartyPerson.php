<?php

namespace app\models;

/**
 * 无党派
 */
class NonpartyPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_WU_DANG_PAI],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
