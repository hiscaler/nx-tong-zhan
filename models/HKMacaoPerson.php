<?php

namespace app\models;

/**
 * 香港同胞、澳门同胞
 */
class HKMacaoPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'full_time_education_level' => '学历学位',
        ]);
    }

}
