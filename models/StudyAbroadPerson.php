<?php

namespace app\models;

use Yii;

/**
 * 出国和归国留学人员
 */
class StudyAbroadPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
