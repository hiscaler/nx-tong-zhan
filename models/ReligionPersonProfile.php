<?php

namespace app\models;

/**
 * This is the model class for table "{{%religion_person_profile}}".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $position_certificate_number
 */
class ReligionPersonProfile extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%religion_person_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['person_id'], 'required'],
            [['person_id'], 'integer'],
            [['position_certificate_number'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_id' => '人员 id',
            'position_certificate_number' => '教职证书编号',
        ];
    }

}
