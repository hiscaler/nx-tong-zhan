<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "www_organization".
 *
 * @property integer $id
 * @property integer $ordering
 * @property integer $type
 * @property string $name
 * @property string $english_name
 * @property string $english_short_name
 * @property string $address
 * @property integer $company_members_count
 * @property integer $person_members_count
 * @property integer $director_count
 * @property integer $executive_director_count
 * @property string $legal_person_username
 * @property string $legal_person_mobile_phone
 * @property string $legal_person_work_information
 * @property string $certificate_number
 * @property integer $certificate_issue_datetime
 * @property string $intro
 * @property integer $input_region_id
 * @property integer $enabled
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Organization extends \yii\db\ActiveRecord
{

    const TYPE_BUSINESS = 1; // 商会组织
    const TYPE_GROUP = 2; // 统战团体组织

    /**
     * 所属类型
     *
     * @var array
     */

    public $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%organization}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ordering', 'name', 'english_name', 'english_short_name', 'address', 'legal_person_username', 'legal_person_mobile_phone', 'legal_person_work_information'], 'required'],
            [['type', 'company_members_count', 'person_members_count', 'director_count', 'executive_director_count', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['company_members_count', 'person_members_count', 'director_count', 'executive_director_count'], 'default', 'value' => 0],
            [['certificate_issue_datetime'], 'date', 'format' => 'php:Y-m-d'],
            [['enabled'], 'default', 'value' => Constant::BOOLEAN_TRUE],
            [['intro'], 'string'],
            [['name', 'address'], 'string', 'max' => 100],
            [['english_name'], 'string', 'max' => 160],
            [['english_short_name'], 'string', 'max' => 60],
            [['legal_person_username'], 'string', 'max' => 20],
            [['legal_person_mobile_phone'], 'string', 'max' => 12],
            [['legal_person_work_information'], 'string', 'max' => 200],
            [['certificate_number'], 'string', 'max' => 30],
            ['category', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ordering' => '排序',
            'type' => '类别',
            'name' => '中文名称',
            'english_name' => '英文名称',
            'english_short_name' => '英文缩写',
            'category' => '类别',
            'address' => '住所',
            'company_members_count' => '单位会员数',
            'person_members_count' => '个人会员数',
            'director_count' => '理事数',
            'executive_director_count' => '常务理事数',
            'legal_person_username' => '法定代表人姓名',
            'legal_person_mobile_phone' => '联系方式',
            'legal_person_work_information' => '法定代表人工作单位及职务',
            'certificate_number' => '登记证书号',
            'certificate_issue_datetime' => '发证日期',
            'intro' => '商会概况',
            'input_region_id' => '录入单位',
            'enabled' => '激活',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'updated_at' => '更新时间',
            'updated_by' => '更新人',
        ];
    }

    public function getCategoryNameString()
    {
        $string = '';
        if ($this->category) {
            $categories = [];
            foreach (static::categoryOptions() as $key => $category) {
                if (in_array($key, $this->category)) {
                    $categories[] = $category;
                }
            }

            $string = implode('、', $categories);
        }

        return $string;
    }

    /**
     * 录入单位
     *
     * @return ActiveRecord
     */
    public function getInputRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'input_region_id']);
    }

    // Events
    public function afterFind()
    {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->category = Yii::$app->getDb()->createCommand('SELECT [[category_id]] FROM {{%organization_category}} WHERE [[organization_id]] = :organizationId', [':organizationId' => $this->id])->queryColumn();
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->input_region_id = Yii::$app->getUser()->getIdentity()->region_id;
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = Yii::$app->getUser()->getId();
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            $this->certificate_issue_datetime = \yadjet\helpers\DatetimeHelper::mktime($this->certificate_issue_datetime);

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $personTypes = $this->category;
        if (!is_array($personTypes)) {
            $personTypes = [];
        }
        $cmd = Yii::$app->getDb()->createCommand();
        if (!$insert) {
            $cmd->delete('{{%organization_category}}', ['organization_id' => $this->id])->execute();
        }
        $batchInsertRows = [];
        foreach ($personTypes as $type) {
            $batchInsertRows[] = [$this->id, $type];
        }
        $cmd->batchInsert('{{%organization_category}}', ['organization_id', 'category_id'], $batchInsertRows)->execute();
    }

}
