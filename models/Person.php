<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "{{%person}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $category
 * @property string $username
 * @property integer $sex
 * @property integer $nation
 * @property integer $birthday
 * @property integer $nationality
 * @property string $native_place
 * @property string $birthplace
 * @property integer $party
 * @property integer $join_party_date
 * @property string $registered_residence_address
 * @property integer $join_job_date
 * @property string $mobile_phone
 * @property string $id_card_number
 * @property string $photo
 * @property integer $full_time_education_level
 * @property string $full_time_education_school
 * @property integer $in_service_education_level
 * @property string $in_service_education_school
 * @property string $professional_title
 * @property string $work_information
 * @property string $social_title
 * @property string $intro
 * @property string $political_arrangements
 * @property string $remark
 * @property integer $enabled
 * @property integer $region_id
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Person extends \yii\db\ActiveRecord
{

    public $relations;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'category', 'sex', 'nationality', 'party', 'full_time_education_level', 'in_service_education_level', 'enabled', 'region_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['category', 'username', 'nation', 'nationality', 'native_place'], 'required'],
            [['full_time_education_level', 'in_service_education_level'], 'default', 'value' => 0],
            [['sex'], 'default', 'value' => Constant::SEX_FEMALE],
            [['birthday'], 'date', 'format' => 'php:Y-m-d', 'timestampAttribute' => 'birthday', 'message' => '出生年月有误（请检查身份证号码是否正确）。'],
            [['intro', 'political_arrangements', 'remark'], 'string'],
            [['username'], 'string', 'max' => 12],
            [['native_place', 'birthplace'], 'string', 'max' => 20],
            [['birthday'], 'required', 'when' => function ($model) {
                    return empty($model->id_card_number);
                }
            ],
            [['sex'], 'required', 'when' => function ($model) {
                    return empty($model->id_card_number);
                }
            ],
            [['birthday'], 'checkBirthday', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['join_job_date'], 'date', 'format' => 'php:Y-m-d', 'timestampAttribute' => 'join_job_date'],
            [['join_party_date'], 'date', 'format' => 'php:Y-m-d', 'timestampAttribute' => 'join_party_date',
                'when' => function ($model) {
                    return $model->party != Constant::PARTY_UNKNOWN && $model->party != Constant::PARTY_WDP;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#" . \yii\helpers\Html::getInputId($this, 'party') . "').val() != 0 && $('#" . \yii\helpers\Html::getInputId($this, 'party') . "').val() != 10;
                }"
            ],
            [['join_party_date'], 'required',
                'when' => function ($model) {
                    return $model->party != Constant::PARTY_UNKNOWN && $model->party != Constant::PARTY_WDP;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#" . \yii\helpers\Html::getInputId($this, 'party') . "').val() != 0 && $('#" . \yii\helpers\Html::getInputId($this, 'party') . "').val() != 10;
                }",
            ],
            [['registered_residence_address'], 'string', 'max' => 60],
            [['mobile_phone'], 'string', 'max' => 50],
            [['id_card_number'], 'string', 'max' => 18],
            [['full_time_education_school', 'in_service_education_school'], 'string', 'max' => 100],
            [['professional_title', 'work_information', 'social_title'], 'string', 'max' => 255],
            ['photo', 'file',
                'extensions' => 'jpg,png,jpeg',
                'minSize' => 1,
                'maxSize' => 204800,
                'tooSmall' => Yii::t('app', 'The file "{file}" is too small. Its size cannot be smaller than {limit}.', [
                    'limit' => \app\modules\admin\components\ApplicationHelper::friendlyFileSize(1),
                ]),
                'tooBig' => Yii::t('app', 'The file "{file}" is too big. Its size cannot exceed {limit}.', [
                    'limit' => \app\modules\admin\components\ApplicationHelper::friendlyFileSize(204800),
                ]),
            ],
            ['relations', 'safe'],
        ];
    }

    public function checkIdCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!empty($this->id_card_number)) {
                if (!\yadjet\helpers\IdentityCardHelper::isValid($this->id_card_number)) {
                    $this->addError('id_card_number', '请输入有效的身份证号码。');
                }
            } else {
                if (empty($this->birthday)) {
                    $this->addError('birthday', '请填写出生年月。');
                }
            }
        }
    }

    /**
     * 验证出生年月是否必填
     *
     * @param string $attribute
     * @param array $params
     */
    public function checkBirthday($attribute, $params)
    {
        if (empty($this->birthday) && (empty($this->id_card_number) || !\yadjet\helpers\IdentityCardHelper::isValid($this->id_card_number))) {
            $this->addError('birthday', '请填写出生年月。');
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yadjet\behaviors\ImageUploadBehavior::className(),
                'attribute' => 'photo'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => '类型',
            'category' => '类别',
            'username' => '姓名',
            'sex' => '性别',
            'nation' => '民族',
            'birthday' => '出生年月',
            'nationality' => '国籍',
            'native_place' => '籍贯',
            'birthplace' => '出生地点',
            'party' => '党派',
            'join_party_date' => '加入党派年月',
            'registered_residence_address' => '户籍所在地',
            'join_job_date' => '参加工作时间',
            'mobile_phone' => '移动电话',
            'id_card_number' => '身份证号码',
            'photo' => '照片',
            'full_time_education_level' => '全日制教育学历学位',
            'full_time_education_school' => '毕业院校系及专业',
            'in_service_education_level' => '在职教育学历学位',
            'in_service_education_school' => '毕业院校系及专业',
            'professional_title' => '职称（职级）',
            'work_information' => '工作单位及职务',
            'social_title' => '主要社会职务',
            'intro' => '个人简历及获奖情况',
            'political_arrangements' => '政治安排',
            'remark' => '备注',
            'enabled' => '激活',
            'region_id' => '所属区域',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'updated_at' => '更新时间',
            'updated_by' => '更新人',
        ];
    }

    public static function categoryOptions()
    {
        return [
            1 => '代表人士',
            2 => '一般人士',
        ];
    }

    // 数据关联定义
    /**
     * 所属区域
     *
     * @return ActiveRecord
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * 家庭主要成员及社会关系
     */
    public function getFamilySocialRelations()
    {
        return $this->hasMany(PersonFamilySocialRelation::className(), ['person_id' => 'id'])->where(['type' => Constant::FAMILY_SOCIAL_RELATIONS_PRIMARY]);
    }

    /**
     * 主要眷属
     *
     * @return ActiveRecord
     */
    public function getFamilySocialRelationsSecondary()
    {
        return $this->hasMany(PersonFamilySocialRelation::className(), ['person_id' => 'id'])->where(['type' => Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY]);
    }

    /**
     * 宗教界
     */
    public function getReligionPersonProfile()
    {
        return $this->hasOne(ReligionPersonProfile::className(), ['person_id' => 'id']);
    }

    /**
     * 非公有制经济代表人士
     */
    public function getNonpublicEconomicPersonProfile()
    {
        return $this->hasOne(NonpublicEconomicPersonProfile::className(), ['person_id' => 'id']);
    }

    /**
     * 出国和归国留学人员
     */
    public function getStudyAbroadPersonProfile()
    {
        return $this->hasOne(StudyAbroadPersonProfile::className(), ['person_id' => 'id']);
    }

    /**
     * 香港同胞、澳门同胞
     */
    public function getHkMacaoPersonProfile()
    {
        return $this->hasOne(HkMacaoPersonProfile::className(), ['person_id' => 'id']);
    }

    /**
     * 台胞（台属）
     */
    public function getTaiWanPersonProfile()
    {
        return $this->hasOne(TaiWanPersonProfile::className(), ['person_id' => 'id']);
    }

    /**
     * 华侨（归侨、侨眷）
     */
    public function getOverseasChinesePersonProfile()
    {
        return $this->hasOne(OverseasChinesePersonProfile::className(), ['person_id' => 'id']);
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $user = Yii::$app->getUser();
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = $user->getId();
                $this->region_id = $user->getIdentity()->region_id;
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            // 根据省份证获取出生年月和性别
            if (!empty($this->id_card_number) && \yadjet\helpers\IdentityCardHelper::isValid($this->id_card_number)) {
                $birthday = strlen($this->id_card_number) == 15 ? ('19' . substr($this->id_card_number, 6, 6)) : substr($this->id_card_number, 6, 8);
                $this->birthday = (new DateTime(substr($birthday, 0, 4) . '-' . substr($birthday, 4, 2) . '-' . substr($birthday, 6, 2)))->getTimestamp();
                $this->sex = substr($this->id_card_number, (strlen($this->id_card_number) == 15 ? -1 : -2), 1) % 2 ? Constant::SEX_MALE : Constant::SEX_FEMALE;
            }

            $joinPartyDate = $this->join_party_date;
            if (!empty($joinPartyDate)) {
                $this->join_party_date = \yadjet\helpers\DatetimeHelper::mktime($joinPartyDate);
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $cmd = Yii::$app->getDb()->createCommand();
        $batchInsertRows = [];
        $deleteIds = [];
        $existsIds = [];
        if (!$insert) {
            $rawExistsIds = Yii::$app->getDb()->createCommand('SELECT [[id]] FROM {{%person_family_social_relation}} WHERE [[person_id]] = :personId', [':personId' => $this->id])->queryColumn();
            foreach ($rawExistsIds as $id) {
                $existsIds[$id] = $id;
            }
        }

        foreach ($this->relations as $relation) {
            if (empty($relation['title']) || empty($relation['username'])) {
                if (!$insert) {
                    $deleteIds[] = $relation['id'];
                }
                continue;
            }
            $birthday = isset($relation['birthday']) ? $relation['birthday'] : null;
            if (!empty($birthday)) {
                try {
                    $birthday = (new DateTime($birthday));
                    $birthday = $birthday->getTimestamp();
                } catch (Exception $exc) {
                    $birthday = null;
                }
            } else {
                $birthday = null;
            }
            $columns = [
                'person_id' => $this->id,
                'type' => $relation['type'],
                'title' => $relation['title'],
                'username' => $relation['username'],
                'birthday' => $birthday,
                'party' => $relation['party'] ?: 0,
                'work_information' => $relation['work_information'],
            ];
            if ($insert) {
                $batchInsertRows[] = $columns;
            } else {
                if ($relation['id']) {
                    if (isset($existsIds[$relation['id']])) {
                        unset($existsIds[$relation['id']]);
                    }
                    $cmd->update('{{%person_family_social_relation}}', $columns, ['id' => $relation['id']])->execute();
                } else {
                    $batchInsertRows[] = $columns;
                }
            }
        }

        if (!$insert) {
            $deleteIds = array_merge($deleteIds, $existsIds);
            if ($deleteIds) {
                $where = ['person_id' => $this->id];
                $where = ['and', $where, ['in', 'id', $deleteIds]];
                $cmd->delete('{{%person_family_social_relation}}', $where)->execute();
            }
        }
        if ($batchInsertRows) {
            $cmd->batchInsert('{{%person_family_social_relation}}', array_keys($batchInsertRows[0]), $batchInsertRows)->execute();
        }
    }

}
