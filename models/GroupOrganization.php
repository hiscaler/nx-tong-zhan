<?php

namespace app\models;

/**
 * 团体组织
 */
class GroupOrganization extends Organization
{

    const CATEGORY_XUE_SHU_XING = 1; // 学术性
    const CATEGORY_HANG_YE_XING = 2; // 行业性
    const CATEGORY_ZHUANG_YE_XING = 3; // 专业性
    const CATEGORY_LIAN_HE_XING = 4; // 联合性
    const CATEGORY_KE_JI_LEI = 5; // 科技类
    const CATEGORY_GONG_YI_CI_SHAN_LEI = 6; // 公益慈善类
    const CATEGORY_CHENG_XIANG_SHE_QU_FU_WU_LEI = 7; // 城乡社区服务类
    const CATEGORY_OTHER = 8; // 其他

    public static function categoryOptions()
    {
        return [
            static::CATEGORY_XUE_SHU_XING => '学术性',
            static::CATEGORY_HANG_YE_XING => '行业性',
            static::CATEGORY_ZHUANG_YE_XING => '专业性',
            static::CATEGORY_LIAN_HE_XING => '联合性',
            static::CATEGORY_KE_JI_LEI => '科技类',
            static::CATEGORY_GONG_YI_CI_SHAN_LEI => '公益慈善类',
            static::CATEGORY_CHENG_XIANG_SHE_QU_FU_WU_LEI => '城乡社区服务类',
            static::CATEGORY_OTHER => '其他',
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'intro' => '团体组织概况',
        ]);
    }

    // Events
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->type = self::TYPE_GROUP;
            }

            return true;
        } else {
            return false;
        }
    }

}
