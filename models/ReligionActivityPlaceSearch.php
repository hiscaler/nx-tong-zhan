<?php

namespace app\models;

use app\models\ReligionActivityPlace;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ReligionActivityPlaceSearch represents the model behind the search form about `app\models\ReligionActivityPlace`.
 */
class ReligionActivityPlaceSearch extends ReligionActivityPlace
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'staff_people_count', 'other_people_count', 'certificate_issue_datetime', 'input_region_id', 'enabled', 'created_at', 'created_by', 'updated_at', 'updated_by', 'category'], 'integer'],
            [['name', 'address', 'superintendent_informations', 'superintendent_mobile_phone', 'certificate_number', 'historical_relic_protected_level', 'intro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReligionActivityPlace::find();
        $identity = Yii::$app->getUser()->getIdentity();
        if ($identity->role == User::ROLE_USER) {
            $where = ['region_id' => $identity->region_id];
        } else {
            $where = [];
        }
        $query->andWhere($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'staff_people_count' => $this->staff_people_count,
            'other_people_count' => $this->other_people_count,
            'certificate_issue_datetime' => $this->certificate_issue_datetime,
            'input_region_id' => $this->input_region_id,
            'enabled' => $this->enabled,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'superintendent_informations', $this->superintendent_informations])
            ->andFilterWhere(['like', 'superintendent_mobile_phone', $this->superintendent_mobile_phone])
            ->andFilterWhere(['like', 'certificate_number', $this->certificate_number])
            ->andFilterWhere(['like', 'historical_relic_protected_level', $this->historical_relic_protected_level])
            ->andFilterWhere(['like', 'intro', $this->intro]);

        if ($this->category) {
            $query->andWhere(['IN', 'id', (new \yii\db\Query())->select('demonstration_plot_id')->from('{{%demonstration_plot_category}}')->where(['category_id' => (int) $this->category])]);
        }

        \app\modules\admin\components\QueryCondition::set('ReligionActivityPlace', $query);

        return $dataProvider;
    }

}
