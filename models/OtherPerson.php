<?php

namespace app\models;

/**
 * 其他需要联系和团结的人员
 */
class OtherPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_OTHER],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

}
