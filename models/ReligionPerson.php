<?php

namespace app\models;

/**
 * 宗教人士
 */
class ReligionPerson extends Person
{

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['type'], 'default', 'value' => Constant::TYPE_SHAO_SHU_MING_ZU],
            [['id_card_number'], 'required'],
            [['id_card_number'], 'string', 'min' => 15, 'max' => 18],
            [['id_card_number'], 'checkIdCardNumber'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'party' => '信仰宗教类别',
            'join_party_date' => '入教年月',
            'professional_title' => '教职',
            'work_information' => '主要活动场所',
        ]);
    }

}
