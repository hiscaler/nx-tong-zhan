<?php

namespace app\models;

use yadjet\helpers\ArrayHelper;
use yadjet\helpers\TreeFormatHelper;
use Yii;
use yii\db\Query;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 * @property integer $parent_id
 * @property integer $level
 * @property string $parent_ids
 * @property string $parent_names
 * @property integer $ordering
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Region extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id', 'level', 'ordering', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['parent_id'], 'default', 'value' => 0],
            [['alias', 'name'], 'trim'],
            [['alias'], 'string', 'max' => 60],
            [['name'], 'string', 'max' => 30],
            [['name'], 'unique'],
            [['parent_ids'], 'string', 'max' => 100],
            [['parent_names'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => '别名',
            'name' => '区域名称',
            'parent_id' => '上级',
            'level' => '等级',
            'parent_ids' => '上级 id',
            'parent_names' => '上级分类',
            'status' => '状态',
            'ordering' => '排序',
        ]);
    }

    public static function getMap($all = false, $owned = false)
    {
        $where = [];
        if (!$all) {
            $where['status'] = Constant::BOOLEAN_TRUE;
        }
        $query = (new Query())->select('name')->from(static::tableName())->where($where);
        if ($owned) {
            $query->andWhere(['IN', 'alias', (new Query())->select('alias')->from('{{%user_region}}')->where(['user_id' => Yii::$app->getUser()->getId()])]);
        }

        return $query->orderBy(['ordering' => SORT_ASC])->indexBy('alias')->column();
    }

    /**
     * 获取所有父节点数据
     * @param mixed|integer $id
     * @return array
     */
    public static function getParents($id)
    {
        $parents = [];
        $row = Yii::$app->getDb()->createCommand('SELECT * FROM {{%region}} WHERE [[id]] = :id', [':id' => $id])->queryOne();
        $parents[] = $row;
        if ($row['parent_id']) {
            $parents = array_merge($parents, static::getParents($row['parent_id']));
        }

        return ArrayHelper::sortByCol($parents, 'parent_id');
    }

    /**
     * 判断是否有子节点
     * @param integer $id
     * @return boolean
     */
    private static function hasChildren($id)
    {
        return Yii::$app->getDb()->createCommand('SELECT COUNT(*) FROM {{%region}} WHERE parent_id = :parentId', [':parentId' => (int) $id])->queryScalar();
    }

    /**
     * 获取所有子节点数据
     * @param mixed|integer $parent
     * @return array
     */
    public static function getChildren($parent = null)
    {
        $children = [];
        $sql = 'SELECT * FROM {{%region}}';
        if ($parent) {
            $sql .= ' WHERE [[parent_id]] = ' . (int) $parent;
        }
        $sql .= ' ORDER BY [[ordering]] ASC';
        $rawData = Yii::$app->getDb()->createCommand($sql)->queryAll();
        foreach ($rawData as $data) {
            $children[] = $data;
            if (static::hasChildren($data['id'])) {
                $children = array_merge($children, static::getChildren($data['id']));
            }
        }

        return $children;
    }

    /**
     * 获取所有子节点 id 集合
     * @param mixed|integer $parent
     * @return array
     */
    public static function getChildrenIds($parent = null)
    {
        $children = static::getChildren($parent);

        return $children ? \yii\helpers\ArrayHelper::getColumn($children, 'id') : [];
    }

    /**
     * 获取分类项目
     * @return array
     */
    public static function getTree($top = null, $all = false, $parentId = null)
    {
        $items = [];
        if ($parentId) {
            $rawData = self::getChildren($parentId);
        } else {
            if ($top) {
                $items[] = $top;
            }
            $bindValues = [];
            $sql = 'SELECT [[id]], [[name]], [[parent_id]] FROM {{%region}}';
            if (!$all) {
                $sql .= ' WHERE status = :status';
                $bindValues[':status'] = Constant::BOOLEAN_TRUE;
            }
            $rawData = Yii::$app->getDb()->createCommand($sql)->bindValues($bindValues)->queryAll();
        }
        if ($rawData) {
            $data = TreeFormatHelper::dumpArrayTree(ArrayHelper:: toTree($rawData, 'id', 'parent_id'));
            foreach ($data as $value) {
                $items[$value['id']] = $value ['levelstr'] . $value['name'];
            }
        }

        return $items;
    }

    protected static function sortItems($tree)
    {
        $ret = [];
        if (isset($tree['children']) && is_array($tree['children'])) {
            $children = $tree['children'];
            unset($tree['children']);
            $ret[] = $tree;
            foreach ($children as $child) {
                $ret = array_merge($ret, self::sortItems($child, 'children'));
            }
        } else {
            unset($tree['children']);
            $ret[] = $tree;
        }

        return $ret;
    }

    // Events
    private $_parent_id;

    public function afterFind()
    {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->_parent_id = $this->parent_id;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->alias = Inflector::humanize(Inflector::slug($this->name, '_'), true);
            $level = 0;
            if ($this->parent_id) {
                $level = Yii::$app->getDb()->createCommand('SELECT [[level]] FROM {{%region}} WHERE [[id]] = :parentId', [':parentId' => $this->parent_id])->queryScalar();
                $level += 1;
            }
            $this->level = $level;

            if ($insert) {
                $this->created_at = $this->updated_at = time();
                $this->created_by = $this->updated_by = Yii::$app->getUser()->getId();
            } else {
                $this->updated_at = time();
                $this->updated_by = Yii::$app->getUser()->getId();
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert || (!$insert && isset($changedAttributes['name'])) || (!$insert && $this->_parent_id !== $this->parent_id)) {
            $parents = static::getParents($this->id);
            $parentIds = implode('/', \yii\helpers\ArrayHelper::getColumn($parents, 'id'));
            $parentNames = implode('/', \yii\helpers\ArrayHelper::getColumn($parents, 'name'));
            Yii::$app->getDb()->createCommand()->update(static::tableName(), ['parent_ids' => $parentIds, 'parent_names' => $parentNames], ['id' => $this->id])->execute();
        }
    }

}
