<?php

namespace app\controllers;

use app\models\Constant;
use app\models\User;
use Yii;

/**
 * 数据统计
 */
class StatisticsController extends Controller
{

    public $layout = 'statistics';

    /**
     * 图表
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $personTypes = [
            Constant::TYPE_MING_ZHU_DANG_PAI => '民主党派',
            Constant::TYPE_WU_DANG_PAI => '无党派',
            Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI => '知识分子',
            Constant::TYPE_SHAO_SHU_MING_ZU => '少数民族',
            Constant::TYPE_ZONG_JIAO_JIE => '宗教界',
            Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO => '非公代表',
            Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI => '新阶层',
            Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN => '留学人员',
            Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO => '港澳同胞',
            Constant::TYPE_TAI_BAO_TAI_SHU => '台胞台属',
            Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN => '华侨',
            Constant::TYPE_OTHER => '其他',
        ];
        $data = [];
        foreach ($personTypes as $key => $text) {
            $data[$key] = [
                'name' => $text . '(0人)',
                'value' => 0,
            ];
        }

        $sql = 'SELECT [[type]], COUNT(*) AS [[value]] FROM {{%person}}';
        if ($this->identity->role == User::ROLE_USER) {
            $sql .= ' WHERE [[region_id]] = ' . (int) $this->identity->region_id;
        }
        $sql .= ' GROUP BY [[type]]';

        $rawData = Yii::$app->getDb()->createCommand($sql)->queryAll();
        foreach ($rawData as $d) {
            if (isset($personTypes[$d['type']])) {
                $data[$d['type']]['name'] = $personTypes[$d['type']] . "({$d['value']}人)";
                $data[$d['type']]['value'] = $d['value'];
            }
        }

        return $this->render('index', [
                'data' => array_values($data),
        ]);
    }

    /**
     * 根据地区统计数据
     *
     * @return mixed
     */
    public function actionRegion()
    {
        $db = Yii::$app->getDb();
        $items = [];
        $regions = (new \yii\db\Query())
            ->select(['id', 'name'])
            ->from('{{%region}}')
            ->orderBy(['ordering' => SORT_ASC])
            ->indexBy('id')
            ->all();
        $regions[0] = [
            'id' => 0,
            'name' => '合计',
            'count' => 0
        ];

        $sql = 'SELECT [[region_id]], COUNT(*) AS count FROM {{%person}}';
        $bindValues = [];
        if ($this->identity->role == User::ROLE_USER) {
            $sql .= ' WHERE [[region_id]] = ' . (int) $this->identity->region_id;
        }
        $sql .= ' GROUP BY [[region_id]]';
        $statData = $db->createCommand($sql, $bindValues)->queryAll();
        foreach ($statData as $data) {
            $regions[$data['region_id']]['count'] = $data['count'];
            $regions[0]['count'] += $data['count'];
        }

        return $this->render('region', [
                'items' => $regions,
        ]);
    }

    public function actionPersonType($regionId = null)
    {
        $personTypes = [
            Constant::TYPE_MING_ZHU_DANG_PAI => [
                'name' => '民主党派',
                'count' => 0,
            ],
            Constant::TYPE_WU_DANG_PAI => [
                'name' => '无党派',
                'count' => 0,
            ],
            Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI => [
                'name' => '知识分子',
                'count' => 0,
            ],
            Constant::TYPE_SHAO_SHU_MING_ZU => [
                'name' => '少数民族',
                'count' => 0,
            ],
            Constant::TYPE_ZONG_JIAO_JIE => [
                'name' => '宗教界',
                'count' => 0,
            ],
            Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO => [
                'name' => '非公代表',
                'count' => 0,
            ],
            Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI => [
                'name' => '新阶层',
                'count' => 0,
            ],
            Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN => [
                'name' => '留学人员',
                'count' => 0,
            ],
            Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO => [
                'name' => '港澳同胞',
                'count' => 0,
            ],
            Constant::TYPE_TAI_BAO_TAI_SHU => [
                'name' => '台胞台属',
                'count' => 0,
            ],
            Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN => [
                'name' => '华侨',
                'count' => 0,
            ],
            Constant::TYPE_OTHER => [
                'name' => '其他',
                'count' => 0,
            ],
            0 => [
                'name' => '合计',
                'count' => 0,
            ],
        ];

        $sql = 'SELECT [[type]], COUNT(*) AS count FROM {{%person}}';
        $bindValues = [];
        if ($this->identity->role == User::ROLE_USER) {
            $sql .= ' WHERE [[region_id]] = ' . (int) $this->identity->region_id;
        } else {
            $regionId = (int) $regionId;
            if ($regionId) {
                $sql .= ' WHERE [[region_id]] = :regionId';
                $bindValues = [':regionId' => $regionId];
            }
        }
        $sql .= ' GROUP BY [[type]]';
        $statData = Yii::$app->getDb()->createCommand($sql, $bindValues)->queryAll();
        foreach ($statData as $data) {
            if (isset($personTypes[$data['type']])) {
                $personTypes[$data['type']]['count'] = $data['count'];
                $personTypes[0]['count'] += $data['count'];
            }
        }

        return $this->render('person-type', [
                'items' => $personTypes,
        ]);
    }

}
