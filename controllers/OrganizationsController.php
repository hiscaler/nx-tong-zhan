<?php

namespace app\controllers;

use app\models\Organization;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * 组织
 */
class OrganizationsController extends Controller
{

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('/organizations/view', [
                'model' => $model,
                'labels' => $model->attributeLabels(),
                'referrer' => Yii::$app->getRequest()->getReferrer()
        ]);
    }

    protected function findModel($id)
    {
        $model = Organization::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('请求的页面不存在。');
        }
    }

}
