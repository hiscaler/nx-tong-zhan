<?php

namespace app\controllers;

use app\models\Option;
use app\models\User;
use Yii;
use yii\db\Query;

class SearchController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($type = null, $regionId = null, $keyword = null)
    {
        if ($type !== null || !empty($regionId) || !empty($keyword)) {
            $where = [];

            // 系统管理员角色的可以看到所有数据，其他角色的只能看到本区域的数据
            $identity = Yii::$app->getUser()->getIdentity();
            if ($identity->getRole() == User::ROLE_USER) {
                $where['region_id'] = $identity->region_id;
            } elseif ($regionId) {
                $where['region_id'] = (int) $regionId;
            }

            if ($type !== null && $type) {
                $where['type'] = (int) $type;
            }
            if (!empty($keyword)) {
                if (preg_match('/^[0-9]{6,18}$/', $keyword)) {
                    $where = ['AND', $where, ['LIKE', 't.id_card_number', $keyword]];
                } else {
                    $where = ['AND', $where, ['LIKE', 't.username', $keyword]];
                }

                if ($this->identity->role == User::ROLE_USER) {
                    $where = ['AND', $where, ['t.region_id' => $this->identity->region_id]];
                }
            }

            $items = (new Query())
                ->select(['t.id', 't.type', 't.sex', 't.username', 't.type', 't.native_place', 't.photo', 't.id_card_number', 'r.name AS region_name', 't.mobile_phone'])
                ->from('{{%person}} t')
                ->leftJoin('{{%region}} r', '[[t.region_id]] = [[r.id]]')
                ->where($where)
                ->orderBy(['t.id' => SORT_DESC])
                ->all();

            $personTypes = [];
        } else {
            $items = [];
            $personTypes = Option::personTypeOptions();
        }

        return $this->render('index', [
                'keyword' => $keyword,
                'items' => $items,
                'personTypes' => $personTypes,
        ]);
    }

}
