<?php

namespace app\controllers;

use app\models\Constant;
use app\models\HKMacaoPerson;
use app\models\NewSocialStratumPerson;
use app\models\NonpublicEconomicPerson;
use app\models\OverseasChinesePerson;
use app\models\Person;
use app\models\ReligionPerson;
use app\models\StudyAbroadPerson;
use app\models\TaiWanPerson;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * 人员管理
 */
class PeopleController extends Controller
{

    /**
     * 查看人员详情
     *
     */
    public function actionView($id, $type = null)
    {
        $model = $this->findModel($id, $type);
        switch ($model['type']) {
            case Constant::TYPE_ZONG_JIAO_JIE:
                $view = 'religionPerson';
                $profile = $model->religionPersonProfile;
                break;

            case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                $view = 'nonpublicEconomicPerson';
                $profile = $model->nonpublicEconomicPersonProfile;
                break;

            case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                $view = 'studyAbroadPerson';
                $profile = $model->studyAbroadPersonProfile;
                break;

            case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                $view = 'hkMacaoPerson';
                $profile = $model->hkMacaoPersonProfile;
                break;

            case Constant::TYPE_TAI_BAO_TAI_SHU:
                $view = 'taiWanPerson';
                $profile = $model->taiWanPersonProfile;
                break;

            case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                $view = 'overseasChinesePerson';
                $profile = $model->overseasChinesePersonProfile;
                break;

            default:
                $view = 'view';
                $profile = null;
        }

        return $this->render($view, [
                'model' => $model,
                'labels' => $model->attributeLabels(),
                'profile' => $profile,
                'profileLabels' => $profile ? $profile->attributeLabels() : [],
                'referrer' => Yii::$app->getRequest()->getReferrer()
        ]);
    }

    protected function findModel($id, $type = null)
    {
        switch ($type) {
            case Constant::TYPE_ZONG_JIAO_JIE:
                $model = ReligionPerson::findOne($id);
                break;

            case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                $model = NonpublicEconomicPerson::findOne($id);
                break;

            case Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI:
                $model = NewSocialStratumPerson::findOne($id);
                break;

            case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                $model = StudyAbroadPerson::findOne($id);
                break;

            case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                $model = HKMacaoPerson::findOne($id);
                break;

            case Constant::TYPE_TAI_BAO_TAI_SHU:
                $model = TaiWanPerson::findOne($id);
                break;

            case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                $model = OverseasChinesePerson::findOne($id);
                break;

            default:
                $model = Person::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('请求的页面不存在。');
        }
    }

}
