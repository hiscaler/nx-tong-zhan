<?php

namespace app\controllers;

use app\models\ReligionActivityPlace;
use app\models\User;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * 宗教活动场所信息登记表
 */
class ReligionActivityPlacesController extends Controller
{

    public function actionIndex($keyword = null)
    {
        $where = [];
        if ($this->identity->role == User::ROLE_USER) {
            $where['region_id'] = $this->identity->region_id;
        }
        if ($keyword) {
            $where = ['AND', ['LIKE', 't.name', $keyword]];
        }
        $items = (new Query())
            ->select(['t.*', 'r.name AS region_name'])
            ->from('{{%religion_activity_place}} t')
            ->leftJoin('{{%region}} r', '[[t.region_id]] = [[r.id]]')
            ->where($where)
            ->all();

        return $this->render('index', [
                'keyword' => $keyword,
                'items' => $items,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
                'model' => $model,
                'labels' => $model->attributeLabels(),
                'referrer' => Yii::$app->getRequest()->getReferrer()
        ]);
    }

    protected function findModel($id)
    {
        $model = ReligionActivityPlace::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('请求的页面不存在。');
        }
    }

}
