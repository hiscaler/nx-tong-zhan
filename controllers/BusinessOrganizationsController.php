<?php

namespace app\controllers;

use app\models\Organization;
use app\models\User;
use yii\db\Query;

/**
 * 商会组织
 */
class BusinessOrganizationsController extends OrganizationsController
{

    public function actionIndex($keyword = null)
    {
        $where = [
            'type' => Organization::TYPE_BUSINESS
        ];
        if ($this->identity->role == User::ROLE_USER) {
            $where['input_region_id'] = $this->identity->region_id;
        }
        if ($keyword) {
            $where = ['AND', ['LIKE', 't.name', $keyword]];
        }
        $items = (new Query())
            ->select(['t.*'])
            ->from('{{%organization}} t')
            ->where($where)
            ->all();

        return $this->render('/organizations/index', [
                'keyword' => $keyword,
                'items' => $items,
        ]);
    }

}
