<?php

namespace app\controllers;

use app\models\DemonstrationPlot;
use app\models\User;
use yii\db\Query;

/**
 * 十三五
 */
class DemonstrationPlots1035Controller extends DemonstrationPlotsController
{

    public function actionIndex($keyword = null)
    {
        $where = [
            'type' => DemonstrationPlot::TYPE_1035
        ];
        if ($this->identity->role == User::ROLE_USER) {
            $where['region_id'] = $this->identity->region_id;
        }
        if ($keyword) {
            $where = ['AND', ['LIKE', 't.name', $keyword]];
        }
        $items = (new Query())
            ->select(['t.*', 'r.name AS region_name'])
            ->from('{{%demonstration_plot}} t')
            ->leftJoin('{{%region}} r', '[[t.region_id]] = [[r.id]]')
            ->where($where)
            ->all();

        return $this->render('/demonstration-plots/index', [
                'keyword' => $keyword,
                'items' => $items,
        ]);
    }

}
