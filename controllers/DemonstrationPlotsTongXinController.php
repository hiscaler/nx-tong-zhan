<?php

namespace app\controllers;

use app\models\DemonstrationPlot;
use app\models\User;
use yii\db\Query;

/**
 * 同心
 */
class DemonstrationPlotsTongXinController extends DemonstrationPlotsController
{

    public function actionIndex($category = null, $keyword = null)
    {
        $where = [
            'type' => DemonstrationPlot::TYPE_TONG_XIN
        ];
        $category = (int) $category;
        if ($category) {
            $where = ['AND', $where, ['IN', 't.id', (new Query())->select('demonstration_plot_id')->from('{{%demonstration_plot_category}}')->where(['category_id' => $category])]];
        }
        if ($this->identity->role == User::ROLE_USER) {
            $where['region_id'] = $this->identity->region_id;
        }
        if ($keyword) {
            $where = ['AND', ['LIKE', 't.name', $keyword]];
        }
        $items = (new Query())
            ->select(['t.*', 'r.name AS region_name'])
            ->from('{{%demonstration_plot}} t')
            ->leftJoin('{{%region}} r', '[[t.region_id]] = [[r.id]]')
            ->where($where)
            ->all();

        return $this->render('/demonstration-plots/index', [
                'keyword' => $keyword,
                'items' => $items,
        ]);
    }

}
