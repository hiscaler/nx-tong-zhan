<?php

namespace app\controllers;

use Yii;

/**
 * 接口
 */
class ApiController extends \yii\rest\Controller
{

    public function actionSms()
    {
        $request = Yii::$app->getRequest();
        $success = false;
        $errorMessage = null;
        if ($request->isPost) {
            $username = $request->post('username');
            $password = $request->post('password');
            if (empty($username) || empty($password)) {
                $errorMessage = '请输入用户名和密码.';
            } else {
                $user = \app\models\User::findByUsername($username);
                if ($user) {
                    if ($user->validatePassword($password)) {
                        if (!empty($user->mobile_phone)) {
                            $verifyCode = rand(100000, 999999);
                            $response = \app\extensions\SMSHelper::send($user->mobile_phone, '您本次登录验证码为：' . $verifyCode, '，请在十分钟内输入。');
                            if ($response['success']) {
                                Yii::$app->getCache()->set($username . $verifyCode, $verifyCode, 600);
                                $success = true;
                            } else {
                                $errorMessage = $response['error']['message'];
                            }
                        } else {
                            $errorMessage = '未设置手机号码。';
                        }
                    } else {
                        $errorMessage = '密码不正确。';
                    }
                } else {
                    $errorMessage = '账号不存在。';
                }
            }
        } else {
            $errorMessage = '错误的请求.';
        }

        $responseBody = ['success' => $success];
        if ($success) {
            $responseBody['code'] = $verifyCode;
        } else {
            $responseBody['error']['message'] = $errorMessage;
        }

        return $responseBody;
    }

}
