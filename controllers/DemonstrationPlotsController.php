<?php

namespace app\controllers;

use app\models\DemonstrationPlot;
use Yii;
use yii\web\NotFoundHttpException;

class DemonstrationPlotsController extends Controller
{

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('/demonstration-plots/view', [
                'model' => $model,
                'labels' => $model->attributeLabels(),
                'referrer' => Yii::$app->getRequest()->getReferrer()
        ]);
    }

    protected function findModel($id)
    {
        $model = DemonstrationPlot::findOne($id);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('请求的页面不存在。');
        }
    }

}
