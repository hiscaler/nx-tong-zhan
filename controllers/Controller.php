<?php

namespace app\controllers;

use Yii;

class Controller extends \yii\web\Controller
{

    protected $identity;

    public function init()
    {
        parent::init();
        $user = Yii::$app->getUser();
        if (!$user->isGuest) {
            $this->identity = $user->getIdentity();
        }
    }

}
