<?php

namespace app\controllers;

use yii\web\Controller;

/**
 * 统战阵地
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class PositionsController extends Controller
{

    public function actionIndex($by = null, $value = null)
    {
        if (!empty($by)) {
            $by = trim($by);
            if (!in_array($by, ['area', 'category']) || empty($value)) {
                $by = 'area';
            }
            if ($by == 'area') {
                return $this->render('area', [
                        'items' => [
                            'province' => '省级四同创建点',
                            'city' => '市级四同创建点',
                            'county' => '县级四同创建点',
                        ],
                ]);
            } else {
                $value = trim($value);
                switch ($value) {
                    case 'province':
                        $items = [
                            1 => '同心园区',
                            4 => '同心项目',
                            7 => '同心社区',
                            10 => '同心乡村',
                        ];
                        break;
                    case 'city':
                        $items = [
                            2 => '同心园区',
                            5 => '同心项目',
                            8 => '同心社区',
                            11 => '同心乡村',
                        ];
                        break;

                    default:
                        $items = [
                            3 => '同心园区',
                            6 => '同心项目',
                            9 => '同心社区',
                            12 => '同心乡村',
                        ];
                }

                return $this->render('category', [
                        'items' => $items,
                ]);
            }
        }

        return $this->render('index');
    }

}
