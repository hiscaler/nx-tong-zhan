<?php

namespace app\modules\admin\extensions;

use app\models\Option;
use app\models\User;
use Yii;
use yii\helpers\Html;

class Formatter extends \yii\i18n\Formatter
{

    public $nullDisplay = '';

    // Common
    public function asBoolean($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $imageUrl = Yii::$app->getRequest()->getBaseUrl() . '/admin/images/';
        $booleanFormat = [
            $imageUrl . 'yes.png',
            $imageUrl . 'no.png'
        ];

        return Html::img($value ? $booleanFormat[0] : $booleanFormat[1]);
    }

    public function asSex($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::sexOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 图片展示
     * @param string $value
     * @param array $options
     * @return string
     */
    public function asImage($value, $options = [])
    {
        return empty($value) ? $this->nullDisplay : Html::img(Yii::$app->getRequest()->getBaseUrl() . $value, $options);
    }

    public function asLong2ip($value)
    {
        if (empty($value)) {
            return $this->nullDisplay;
        } else {
            return long2ip($value);
        }
    }

    // User
    public function asUserType($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $options = User::typeOptions();
        return isset($options[$value]) ? $options[$value] : $this->nullDisplay;
    }

    public function asUserStatus($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $options = User::statusOptions();
        return isset($options[$value]) ? $options[$value] : $this->nullDisplay;
    }

    public function asUserRole($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $options = User::roleOptions();
        return isset($options[$value]) ? $options[$value] : $this->nullDisplay;
    }

    /**
     * 人员类型
     * @param integer $value
     * @return string|null
     */
    public function asPersonType($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::personTypeOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 人员类别
     * @param integer $value
     * @return string|null
     */
    public function asPersonCategory($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = \app\models\Person::categoryOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 民族
     * @param integer $value
     * @return string|null
     */
    public function asNation($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::nationOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 学历
     * @param integer $value
     * @return string|null
     */
    public function asEducationLevel($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::educationLevelOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 国籍
     * @param integer $value
     * @return string|null
     */
    public function asNationality($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::nationalityOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 党派
     * @param integer $value
     * @return string|null
     */
    public function asParty($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::partyOptions(true);
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 宗教
     * @param integer $value
     * @return string|null
     */
    public function asReligion($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = Option::religionOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    /**
     * 出国和归国留学人员留学类别
     *
     * @param integer $value
     * @return string|null
     */
    public function asStudyAbroadPersonType($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = \app\models\StudyAbroadPersonProfile::typeOptions();
        return isset($options[$value]) ? $options[$value] : null;
    }

    public function asRegion($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $options = \app\models\Region::getMap();
        return isset($options[$value]) ? $options[$value] : null;
    }

}
