<?php

namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;

/**
 * 统战阵地控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class OrganizationControlPanel extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $type = Yii::$app->getRequest()->get('type');
        $items = [
            [
                'label' => '民主党派',
                'url' => ['democratic-party-organizations/index'],
                'active' => $controllerId == 'democratic-party-organizations',
            ],
            [
                'label' => '商会组织',
                'url' => ['business-organizations/index'],
                'active' => $controllerId == 'business-organizations',
            ],
            [
                'label' => '统战团体组织',
                'url' => ['group-organizations/index'],
                'active' => $controllerId == 'group-organizations',
            ],
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '民主党派和统战团体组织',
                'items' => $this->getItems(),
        ]);
    }

}
