<?php

namespace app\modules\admin\widgets;

use app\models\User;
use Yii;
use yii\base\Widget;

/**
 * 首页图标
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class HomepageIcons extends Widget
{

    public function getItems()
    {
        $userRole = Yii::$app->getUser()->getIdentity()->role;
        $items = [
            [
                'label' => '系统管理人员',
                'icon' => 'icon-users.png',
                'url' => ['users/index'],
            ],
            [
                'label' => '区域管理',
                'icon' => 'icon-regions.png',
                'url' => ['regions/index'],
            ],
            [
                'label' => '统战人物',
                'icon' => 'icon-people.png',
                'url' => ['people/index'],
            ],
            [
                'label' => '统战阵地',
                'icon' => 'icon-position.png',
                'url' => ['demonstration-plots-tong-xin/index'],
            ],
            [
                'label' => '民主党派和统战团体组织',
                'icon' => 'icon-organizations.png',
                'url' => ['business-organizations/index'],
            ],
        ];

        if ($userRole !== User::ROLE_ADMINISTRATOR) {
            $items[0]['url'] = null;
            $items[1]['url'] = null;
        }


        return $items;
    }

    public function run()
    {
        return $this->render('HomepageIcons', [
                'items' => $this->getItems(),
        ]);
    }

}
