<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;

/**
 * 人员管理控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class PeopleControlPanel extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $items = [
            [
                'label' => '所有人员',
                'url' => ['people/index'],
                'active' => $controllerId == 'people',
            ],
            [
                'label' => '民主党派成员',
                'url' => ['democratic-party-people/index'],
                'active' => $controllerId == 'democratic-party-people',
            ],
            [
                'label' => '无党派人士',
                'url' => ['nonparty-people/index'],
                'active' => $controllerId == 'nonparty-people',
            ],
            [
                'label' => '党外知识分子',
                'url' => ['nonparty-intellectual-people/index'],
                'active' => $controllerId == 'nonparty-intellectual-people',
            ],
            [
                'label' => '少数民族',
                'url' => ['minority-people/index'],
                'active' => $controllerId == 'minority-people',
            ],
            [
                'label' => '宗教界代表',
                'url' => ['religion-people/index'],
                'active' => $controllerId == 'religion-people',
            ],
            [
                'label' => '非公有制经济代表',
                'url' => ['nonpublic-economic-people/index'],
                'active' => $controllerId == 'nonpublic-economic-people',
            ],
            [
                'label' => '新的社会阶层人士',
                'url' => ['new-social-stratum-people/index'],
                'active' => $controllerId == 'new-social-stratum-people',
            ],
            [
                'label' => '出国和归国留学人员',
                'url' => ['study-abroad-people/index'],
                'active' => $controllerId == 'study-abroad-people',
            ],
            [
                'label' => '香港同胞、澳门同胞',
                'url' => ['hk-macao-people/index'],
                'active' => $controllerId == 'hk-macao-people',
            ],
            [
                'label' => '台胞台属登记表',
                'url' => ['tai-wan-people/index'],
                'active' => $controllerId == 'tai-wan-people',
            ],
            [
                'label' => '华侨、归侨、侨眷',
                'url' => ['overseas-chinese-people/index'],
                'active' => $controllerId == 'overseas-chinese-people',
            ],
            [
                'label' => '其他人员',
                'url' => ['other-people/index'],
                'active' => $controllerId == 'other-people',
            ],
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '统战人物',
                'items' => $this->getItems(),
        ]);
    }

}
