<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;

/**
 * 区域管理控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class RegionControlPanel extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $actionId = $this->view->context->action->id;
        $items = [
            [
                'label' => '区域添加',
                'url' => ['regions/create'],
                'active' => $controllerId == 'regions' && $actionId == 'create',
            ],
            [
                'label' => '区域列表',
                'url' => ['regions/index'],
                'active' => $controllerId == 'regions' && $actionId == 'index',
            ],
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '系统管理人员',
                'items' => $this->getItems(),
        ]);
    }

}
