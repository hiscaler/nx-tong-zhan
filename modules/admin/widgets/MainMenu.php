<?php

namespace app\modules\admin\widgets;

use app\models\User;
use Yii;
use yii\base\Widget;

/**
 * 顶部菜单
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class MainMenu extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $role = Yii::$app->getUser()->getIdentity()->role;
        $items = [
            [
                'label' => '系统管理人员',
                'url' => ['users/index'],
                'options' => ['class' => 'i icon-user'],
                'active' => in_array($controllerId, ['users']),
                'visible' => $role == User::ROLE_ADMINISTRATOR,
            ],
            [
                'label' => '区域管理',
                'url' => ['regions/index'],
                'options' => ['class' => 'i icon-region'],
                'active' => in_array($controllerId, ['regions']),
                'visible' => $role == User::ROLE_ADMINISTRATOR,
            ],
            [
                'label' => '统战人物',
                'url' => ['people/index'],
                'options' => ['class' => 'i icon-people'],
                'active' => $controllerId == 'people',
            ],
            [
                'label' => '统战阵地',
                'url' => ['demonstration-plots-tong-xin/index'],
                'options' => ['class' => 'i icon-people'],
                'active' => in_array($controllerId, ['demonstration-plots-tong-xin', 'demonstration-plots-1035', 'religion-activity-places',]),
            ],
            [
                'label' => '民主党派和统战团体组织',
                'url' => ['business-organizations/index'],
                'options' => ['class' => 'i icon-people'],
                'active' => in_array($controllerId, ['business-organizations', 'group-organizations', 'democratic-party-organizations']),
            ],
        ];


        return $items;
    }

    public function run()
    {
        return $this->render('MainMenu', [
                'items' => $this->getItems(),
        ]);
    }

}
