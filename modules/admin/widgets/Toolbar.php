<?php

namespace app\modules\admin\widgets;

use app\models\Constant;
use app\models\User;
use app\models\Yad;
use Yii;
use yii\base\Widget;

class Toolbar extends Widget
{

    public function getItems()
    {
        $items = [];
        $user = Yii::$app->getUser();
        if (!$user->isGuest) {
            $identity = $user->getIdentity();
            if ($identity->region_id) {
                $label = '[ ' . $user->getIdentity()->region->name . ' ] ';
            } else {
                $label = '';
            }
            $label .= '欢迎您，' . $identity->nickname;
            $items[] = [
                'label' => $label,
                'url' => ['default/profile'],
            ];

            $items[] = [
                'label' => '退出',
                'url' => ['default/logout'],
                'template' => '<a id="logout" href="{url}">{label}</a>'
            ];
        }

        return $items;
    }

    public function run()
    {
        return $this->render('Toolbar', [
                'items' => $this->getItems(),
        ]);
    }

}
