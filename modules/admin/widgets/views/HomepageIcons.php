<div id="homepage-icons">
    <ul>
        <?php
        $baseUrl = Yii::$app->getRequest()->getBaseUrl() . '/admin';
        foreach ($items as $item):
            ?>
            <li>
                <a href="<?= $item['url'] ? yii\helpers\Url::toRoute($item['url']) : '#' ?>">
                    <img src="<?= $baseUrl . '/images/homepage/' . $item['icon'] ?>" />
                    <div class="name"><?= $item['label'] ?></div>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
