<?php

namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;

/**
 * 统战阵地控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class PositionControlPanel extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $type = Yii::$app->getRequest()->get('type');
        $items = [
            [
                'label' => '四同创建点',
                'url' => ['demonstration-plots-tong-xin/index'],
                'active' => $controllerId == 'demonstration-plots-tong-xin',
            ],
            [
                'label' => '凝心聚力十三五行动示范点',
                'url' => ['demonstration-plots-1035/index'],
                'active' => $controllerId == 'demonstration-plots-1035',
            ],
            [
                'label' => '宗教活动场所',
                'url' => ['religion-activity-places/index'],
                'active' => $controllerId == 'religion-activity-places',
            ],
            [
                'label' => '其他',
                'url' => ['demonstration-plots-other/index'],
                'active' => $controllerId == 'demonstration-plots-other',
            ],
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '统战阵地',
                'items' => $this->getItems(),
        ]);
    }

}
