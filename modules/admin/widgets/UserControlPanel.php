<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;

/**
 * 系统人员管理控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class UserControlPanel extends Widget
{

    public function getItems()
    {
        $controllerId = $this->view->context->id;
        $actionId = $this->view->context->action->id;
        $items = [
            [
                'label' => '搜索',
                'url' => ['users/index'],
                'active' => $controllerId == 'users' && $actionId == 'index',
            ],
            [
                'label' => '人员添加',
                'url' => ['users/create'],
                'active' => $controllerId == 'users' && $actionId == 'create',
            ],
            [
                'label' => '人员列表',
                'url' => ['users/index'],
                'active' => $controllerId == 'users' && $actionId == 'index',
            ],
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '系统管理人员',
                'items' => $this->getItems(),
        ]);
    }

}
