<?php

namespace app\modules\admin\widgets;

use app\models\User;
use Yii;
use yii\base\Widget;

/**
 * 全局管理控制面板
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class ControlPanel extends Widget
{

    public function getItems()
    {
        $controller = $this->view->context;
        $controllerId = $controller->id;
        $actionId = $controller->action->id;
        $items = [
            [
                'label' => '系统管理人员',
                'url' => ['users/index'],
                'active' => in_array($controllerId, ['users']),
                'visible' => Yii::$app->getUser()->getIdentity()->role == User::ROLE_ADMINISTRATOR,
            ],
            [
                'label' => '区域管理',
                'url' => ['regions/index'],
                'active' => in_array($controllerId, ['regions']),
                'visible' => Yii::$app->getUser()->getIdentity()->role == User::ROLE_ADMINISTRATOR,
            ],
            [
                'label' => '个人资料',
                'url' => ['default/profile'],
                'active' => $controllerId == 'default' && $actionId == 'profile',
            ]
        ];

        return $items;
    }

    public function run()
    {
        return $this->render('ControlPanel', [
                'title' => '系统管理',
                'items' => $this->getItems(),
        ]);
    }

}
