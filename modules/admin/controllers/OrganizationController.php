<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;

/**
 * 民主党派和统战团体组织
 */
class OrganizationController extends Controller
{

    public $layout = 'organization';

}
