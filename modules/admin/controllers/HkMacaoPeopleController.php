<?php

namespace app\modules\admin\controllers;

use app\models\Constant;
use app\models\HKMacaoPerson;
use app\models\HkMacaoPersonProfile;
use app\models\PersonSearch;
use app\modules\admin\components\QueryCondition;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * 香港同胞、澳门同胞登记表
 */
class HkMacaoPeopleController extends PeopleController
{

    public function init()
    {
        parent::init();
        $this->type = Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO;
    }

    /**
     * Lists all DemocraticPartyPerson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['PersonSearch']['type'] = $this->type;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new DemocraticPartyPerson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $person = new HKMacaoPerson();
        $person->party = Constant::PARTY_UNKNOWN;
        $model = new HkMacaoPersonProfile();

        if ($model->load(Yii::$app->request->post()) && $person->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $person->validate() && $isValid;
            if ($isValid) {
                $person->save(false);
                $model->person_id = $person->id;
                $model->save(false);

                return $this->redirect(['view', 'id' => $person->id]);
            }
        }

        return $this->render('create', [
                'model' => $person,
                'profile' => $model,
        ]);
    }

    /**
     * Updates an existing DemocraticPartyPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $person = $this->findModel($id);
        $model = HkMacaoPersonProfile::findOne(['person_id' => $person->id]);
        if (!$model) {
            $model = new HkMacaoPersonProfile();
        }

        if ($model->load(Yii::$app->request->post()) && $person->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $person->validate() && $isValid;
            if ($isValid) {
                $person->save(false);
                $model->person_id = $person->id;
                $model->save(false);

                return $this->redirect(['view', 'id' => $person->id]);
            }
        }

        return $this->render('update', [
                'model' => $person,
                'profile' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
                'model' => $model,
                'profile' => $model['hkMacaoPersonProfile']
        ]);
    }

    /**
     * 导出为 Excel 文件
     */
    public function actionToExcel()
    {
        $query = QueryCondition::get('PERSON');
        if ($query) {
            $items = $query->all();
        } else {
            $items = HKMacaoPerson::find()->all();
        }
        $formatter = Yii::$app->getFormatter();
        $phpExcel = new PHPExcel();

        $phpExcel->getProperties()->setCreator("Microsoft")
            ->setLastModifiedBy("Microsoft")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Person");

        $phpExcel->setActiveSheetIndex(0);
        $activeSheet = $phpExcel->getActiveSheet();
        $phpExcel->getDefaultStyle()
            ->getFont()->setSize(14);

        $activeSheet->getDefaultRowDimension()->setRowHeight(25);

        $cols = ['A' => 4, 'B' => 7, 'C' => 5, 'D' => 10, 'E' => 16, 'F' => 12, 'G' => 12, 'H' => 20, 'I' => 16, 'J' => 12, 'K' => 12, 'L' => 12, 'M' => 12, 'N' => 12];
        foreach ($cols as $col => $width) {
            $activeSheet->getColumnDimension($col)->setWidth($width);
        }

        $activeSheet->setCellValue('A1', '香港同胞、澳门同胞汇总登记表')->mergeCells('A1:L1')->getStyle()->applyFromArray(array(
            'font' => array(
                'bold' => true,
                'size' => 16,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        ));

        $activeSheet->setCellValue("A2", '序号')
            ->setCellValue("B2", '港澳同胞姓名')
            ->setCellValue("C2", '性别')
            ->setCellValue("D2", '民族')
            ->setCellValue("E2", '出生年月')
            ->setCellValue("F2", '工作单位及职务')
            ->setCellValue("G2", '现居住地址')
            ->setCellValue("H2", '办公/住宅电话')
            ->setCellValue("I2", '县内亲属姓名')
            ->setCellValue("J2", '与港澳同胞关系')
            ->setCellValue("K2", '政治安排')
            ->setCellValue("L2", '手机号码')
            ->setCellValue("M2", '录入单位')
            ->setCellValue("N2", '备注');

        $row = 3;
        foreach ($items as $i => $item) {
            $users = [];
            $titles = [];
            foreach ($item->familySocialRelations as $d) {
                $users[] = $d['username'];
                $titles[] = $d['title'];
            }
            $activeSheet->setCellValue("A{$row}", $i + 1)
                ->setCellValue("B{$row}", $item['username'])
                ->setCellValue("C{$row}", $formatter->asSex($item['sex']))
                ->setCellValue("D{$row}", $formatter->asNation($item['nation']))
                ->setCellValue("E{$row}", date('Y/m/d', $item['birthday']))
                ->setCellValue("F{$row}", $item['work_information'])
                ->setCellValue("G{$row}", $item['hkMacaoPersonProfile']['main_live_address'])
                ->setCellValue("H{$row}", $item['hkMacaoPersonProfile']['tel'])
                ->setCellValue("I{$row}", implode(PHP_EOL, $users))
                ->setCellValue("J{$row}", implode(PHP_EOL, $titles))
                ->setCellValue("K{$row}", $item['political_arrangements'])
                ->setCellValue("L{$row}", $item['mobile_phone'])
                ->setCellValue("M{$row}", $item['region']['name'])
                ->setCellValue("N{$row}", $item['remark']);
            $row++;
        }

        $phpExcel->getActiveSheet()->setTitle('汇总表');
        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->getStyle("I3:I{$row}")->getAlignment()->setWrapText(true);
        $phpExcel->getActiveSheet()->getStyle("J3:J{$row}")->getAlignment()->setWrapText(true);
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $filename = '香港同胞、澳门同胞.xlsx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . urlencode($filename);
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, $filename, ['mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);
    }

    /**
     * 导出为 Word 文件
     * @param integer $id
     */
    public function actionToWord($id)
    {
        $model = $this->findModel($id);
        $formatter = Yii::$app->getFormatter();
        $phpWord = new PhpWord();
        $phpWord->setDefaultFontName('宋体'); // 全局字体
        $phpWord->setDefaultFontSize(9);     // 全局字号为3号

        $header = array('size' => 16, 'bold' => true);
        $cellRowContinue = array('vMerge' => 'continue');
        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => JcTable::CENTER);
        $fancyTableCellStyle = array('valign' => 'center', 'borderRightSize' => 0, 'fontSize' => 30);
        $fancyTableFontStyle = array('align' => 'center');
        $phpWord->addParagraphStyle('pStyle', array('align' => 'center'));
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);
        $section = $phpWord->addSection();
        $section->addText('香港同胞、澳门同胞登记表', $header, 'pStyle');
        $section->addTextBreak(1);
        $table = $section->addTable($fancyTableStyleName);

        $col1 = 1400;
        $col2 = 1400;
        $col3 = 900;
        $col4 = 1400;
        $col5 = 1400;
        $col6 = 1600;
        $col7 = 1500;

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('姓名', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model['username'], $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('性别', $fancyTableFontStyle);
        $table->addCell($col4, $fancyTableCellStyle)->addText($formatter->asSex($model['sex']), $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('出生年月', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText(date('Y.m.d', $model['birthday']), $fancyTableFontStyle);
        $c = $table->addCell($col7, array('vMerge' => 'restart', 'valign' => 'center'));
        if (!empty($model['photo'])) {
            $c->createTextRun()->addImage(Yii::getAlias('@webroot') . $model['photo'], array('width' => 110, 'height' => 140, 'marginTop' => -1, 'marginLeft' => -1));
        } else {
            $c->addTextRun(array('alignment' => Jc::CENTER))->addText('');
        }

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('国籍', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($formatter->asNationality($model['nationality']), $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('民族', $fancyTableFontStyle);
        $table->addCell($col4, $fancyTableCellStyle)->addText($formatter->asNation($model['nation']), $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('出生地点', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText($model['birthplace'], $fancyTableFontStyle);
        $table->addCell(null, $cellRowContinue);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('籍贯', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4, array('gridSpan' => 3))->addText($model->native_place, $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('现主要居住地', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText($model->hkMacaoPersonProfile->main_live_address, $fancyTableFontStyle);
        $table->addCell(null, $cellRowContinue);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('移动电话', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4, array('gridSpan' => 3))->addText($model['mobile_phone'], $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('学历学位', $fancyTableFontStyle);
        $table->addCell($col6 + $col7, array('gridSpan' => 2, 'valign' => 'center'))->addText($formatter->asEducationLevel($model['full_time_education_level']), $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('固定电话', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4, array('gridSpan' => 3))->addText($model->hkMacaoPersonProfile->tel, $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('工作单位及职务', $fancyTableFontStyle);
        $table->addCell($col6 + $col7, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['work_information'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('主要社会职务', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, array('gridSpan' => 6, 'valign' => 'center'))->addText($model->social_title, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('类别', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, ['gridSpan' => 6])->addText($formatter->asPersonCategory($model->category), $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('政治安排', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, ['gridSpan' => 6])->addText($model->political_arrangements, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('熟悉专长或专攻方向', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, ['gridSpan' => 6])->addText($model->hkMacaoPersonProfile->speciality, $fancyTableFontStyle);

        $table->addRow(1500);
        $table->addCell($col1, $fancyTableCellStyle)->addText('个人简介（是企业负责人的含企业情况简介', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, array('gridSpan' => 6, 'valign' => 'center'))->addText($model->intro, $fancyTableFontStyle);

        $familySocialRelations = $model['familySocialRelations'];
        array_unshift($familySocialRelations, [
            'title' => '称谓',
            'username' => '姓名',
            'birthday' => '出生年月',
            'party' => '党派',
            'work_information' => '工作单位及职务',
        ]);
        foreach ($familySocialRelations as $i => $relation) {
            $table->addRow(200);
            if ($i == 0) {
                $table->addCell($col1, array('vMerge' => 'restart', 'valign' => 'center'))->addText('宁乡县内主要亲属', $fancyTableFontStyle);
                $birthday = $relation['birthday'];
                $party = $relation['party'];
            } else {
                $table->addCell($col1, $cellRowContinue);
                $birthday = date('Y.m.d', $relation['birthday']);
                $party = $formatter->asParty($relation['party']);
            }
            $table->addCell($col2, $fancyTableCellStyle)->addText($relation['title'], $fancyTableFontStyle);
            $table->addCell($col3, $fancyTableCellStyle)->addText($relation['username'], $fancyTableFontStyle);

            $table->addCell($col4, array('valign' => 'center'))->addText($birthday, $fancyTableFontStyle);
            $table->addCell($col5, $fancyTableCellStyle)->addText($party, $fancyTableFontStyle);
            $table->addCell($col6 + $col7, array('gridSpan' => 2, 'valign' => 'center'))->addText($relation['work_information'], $fancyTableFontStyle);
        }
        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('备注', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7, array('gridSpan' => 6, 'valign' => 'center'))->addText($model->remark, $fancyTableFontStyle);

        $footer = $section->addFooter();
        $footer->addText('录入单位：' . $model['region']['name'], null, array('alignment' => Jc::RIGHT));

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $filename = $model['id'] . '.docx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filename;
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, "{$model['username']}.docx", ['mimeType' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
    }

    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HKMacaoPerson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
