<?php

namespace app\modules\admin\controllers;

use app\models\Constant;
use app\models\Person;
use app\models\PersonSearch;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PeopleController implements the CRUD actions for Person model.
 */
class PeopleController extends Controller
{

    /**
     * 要查看的人员类型
     *
     * @var integer
     */
    protected $type;

    /**
     * 布局文件
     *
     * @var string
     */
    public $layout = 'people';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete', 'to-word', 'to-excel'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id == 'create' && !Yii::$app->getUser()->getIdentity()->region_id) {
                throw new \yii\web\BadRequestHttpException('您没有归属到具体的区域，禁止添加人员数据。');
            }

            return true;
        }

        return false;
    }

    /**
     * Lists all Person models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Person model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $person = $this->findModel($id);
        return $this->render('/people/view', [
//                'person' => $person,
                'model' => $person,
        ]);
        if ($this->id == 'people') {
            switch ($person->type) {
//                case Constant::TYPE_MING_ZHU_DANG_PAI:
//                    $this->redirect(['democratic-party-people/view', 'id' => $person->id]);
//                    break;

                case Constant::TYPE_WU_DANG_PAI:
                    $this->redirect(['nonparty-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI:
                    $this->redirect(['nonparty-intellectual-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_SHAO_SHU_MING_ZU:
                    $this->redirect(['minority-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_ZONG_JIAO_JIE:
                    $this->redirect(['religion-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                    $this->redirect(['nonpublic-economic-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI:
                    $this->redirect(['new-social-stratum-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                    $this->redirect(['study-abroad-people/update', 'id' => $person->id]);
                    break;

                case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                    $this->redirect(['hk-macao-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_TAI_BAO_TAI_SHU:
                    $this->redirect(['tai-wan-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                    $this->redirect(['overseas-chinese-people/view', 'id' => $person->id]);
                    break;

                case Constant::TYPE_OTHER:
                    $this->redirect(['other-people/view', 'id' => $person->id]);
                    break;
            }
        }

        switch ($person->type) {
            case Constant::TYPE_MING_ZHU_DANG_PAI:
                $model = $person->democraticPartyPerson;
                break;

            case Constant::TYPE_WU_DANG_PAI:
                $model = $person->nonpartyPerson;
                break;

            case Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI:
                $model = $person->nonpartyIntellectualPerson;
                break;

            case Constant::TYPE_SHAO_SHU_MING_ZU:
                $model = $person->minorityPerson;
                break;

            case Constant::TYPE_ZONG_JIAO_JIE:
                $model = $person->religionPerson;
                break;

            case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                $model = $person->nonpublicEconomicPerson;
                break;

            case Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI:
                $model = $person->newSocialStratumPerson;
                break;

            case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                $model = $person->studyAbroadPerson;
                break;

            case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                $model = $person->hKMacaoPerson;
                break;

            case Constant::TYPE_TAI_BAO_TAI_SHU:
                $model = $person->taiWanPerson;
                break;

            case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                $model = $person->overseasChinesePerson;
                break;

            case Constant::TYPE_OTHER:
                $model = $person->otherPerson;
                break;
        }

        return $this->render('view', [
                'person' => $person,
                'model' => $model,
        ]);
    }

    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Person();
        $model->type = $this->type;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('/people/create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Person model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('/people/update', [
                    'model' => $model,
            ]);
        }


        switch ($model->type) {
            case Constant::TYPE_MING_ZHU_DANG_PAI:
                $this->redirect(['democratic-party-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_WU_DANG_PAI:
                $this->redirect(['nonparty-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI:
                $this->redirect(['nonparty-intellectual-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_SHAO_SHU_MING_ZU:
                $this->redirect(['minority-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_ZONG_JIAO_JIE:
                $this->redirect(['religion-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                $this->redirect(['nonpublic-economic-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI:
                $this->redirect(['new-social-stratum-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                $this->redirect(['study-abroad-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                $this->redirect(['hk-macao-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_TAI_BAO_TAI_SHU:
                $this->redirect(['tai-wan-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                $this->redirect(['overseas-chinese-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_OTHER:
                $this->redirect(['other-people/update', 'id' => $model->id]);
                break;
        }
    }

    /**
     * Deletes an existing Person model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionToWord($id)
    {
        $model = $this->findModel($id);
        switch ($model->type) {
            case Constant::TYPE_MING_ZHU_DANG_PAI:
                $title = '民主党派成员登记表';
                break;

            case Constant::TYPE_WU_DANG_PAI:
                $this->redirect(['nonparty-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_DANG_WAI_ZHI_SHI_FEN_ZI:
                $title = '党外知识分子登记表';
                break;

            case Constant::TYPE_SHAO_SHU_MING_ZU:
                $title = '少数民族人士登记表';
                break;

            case Constant::TYPE_ZONG_JIAO_JIE:
                $this->redirect(['religion-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_FEI_SI_YOU_ZHI_JING_JI_DAI_BIAO:
                $this->redirect(['nonpublic-economic-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_XING_DE_SHE_HUI_JIE_CENG_REN_SHI:
                $title = '新的社会阶层人士登记表';
                break;

            case Constant::TYPE_CHU_GUO_GUI_GUO_LIU_XUE_REN_YUAN:
                $this->redirect(['study-abroad-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_XIANG_GANG_AO_MENG_TONG_BAO:
                $this->redirect(['hk-macao-people/update', 'id' => $model->id]);
                break;

            case Constant::TYPE_TAI_BAO_TAI_SHU:
                $this->redirect(['tai-wan-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_HUA_JIAO_GUI_JIAO_QIAO_JUAN:
                $this->redirect(['overseas-chinese-people/to-word', 'id' => $model->id]);
                break;

            case Constant::TYPE_OTHER:
                $title = '其他需要联系和团结的人员登记表';
                break;

            default :
                throw new \yii\web\BadRequestHttpException('导出 Word 功能正在升级中...');
        }

        $formatter = Yii::$app->getFormatter();
        $phpWord = new PhpWord();
        $phpWord->setDefaultFontName('宋体'); // 全局字体
        $phpWord->setDefaultFontSize(9);     // 全局字号为3号

        $header = array('size' => 16, 'bold' => true);
        $cellRowContinue = array('vMerge' => 'continue');
        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => JcTable::CENTER);
        $fancyTableCellStyle = array('valign' => 'center', 'borderRightSize' => 0, 'fontSize' => 30);
        $fancyTableFontStyle = array('align' => 'center');
        $phpWord->addParagraphStyle('pStyle', array('align' => 'center'));
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);
        $section = $phpWord->addSection();
        $section->addText($title, $header, 'pStyle');
        $section->addTextBreak(1);
        $table = $section->addTable($fancyTableStyleName);

        $col1 = 700;
        $col2 = 1400;
        $col3 = 900;
        $col4 = 700;
        $col5 = 800;
        $col6 = 1200;
        $col7 = 1150;
        $col8 = 1600;
        $col9 = 1500;

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('姓名', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model['username'], $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('性别', $fancyTableFontStyle);
        $table->addCell($col4, $fancyTableCellStyle)->addText($formatter->asSex($model['sex']), $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('民族', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText($formatter->asNation($model['nation']), $fancyTableFontStyle);
        $table->addCell($col7, $fancyTableCellStyle)->addText('出生年月', $fancyTableFontStyle);
        $table->addCell($col8, $fancyTableCellStyle)->addText(date('Y.m.d', $model['birthday']), $fancyTableFontStyle);
        $c = $table->addCell($col9, array('vMerge' => 'restart', 'valign' => 'center'));
        if (!empty($model['photo'])) {
            $c->createTextRun()->addImage(Yii::getAlias('@webroot') . $model['photo'], array('width' => 110, 'height' => 140, 'marginTop' => -1, 'marginLeft' => -1));
        } else {
            $c->addTextRun(array('alignment' => Jc::CENTER))->addText('');
        }

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('国籍', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($formatter->asNationality($model['nationality']), $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('籍贯', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['native_place'], $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText('出生地点', $fancyTableFontStyle);
        $table->addCell($col7 + $col8, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['birthplace'], $fancyTableFontStyle);
        $table->addCell($col9, $cellRowContinue);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('党派', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($formatter->asParty($model['party']), $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('加入党派时间', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['join_party_date'] ? $formatter->asDate($model['join_party_date']) : '', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText('户籍所在地', $fancyTableFontStyle);
        $table->addCell($col7 + $col8, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['registered_residence_address'], $fancyTableFontStyle);
        $table->addCell($col9, $cellRowContinue);


        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('参加工作时间', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model['join_job_date'] ? $formatter->asDate($model['join_job_date']) : '', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('移动电话', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['mobile_phone'], $fancyTableFontStyle);
        $table->addCell($col6, array('valign' => 'center'))->addText('身份证号码', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['id_card_number'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, array('vMerge' => 'restart', 'valign' => 'center'))->addText('学历学位', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText('全日制教育', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText($formatter->asEducationLevel($model['full_time_education_level']), $fancyTableFontStyle);
        $table->addCell($col4 + $col5 + $col6, array('gridSpan' => 3, 'valign' => 'center'))->addText('毕业院校系及专业', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['full_time_education_school'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell(null, $cellRowContinue);
        $table->addCell($col2, $fancyTableCellStyle)->addText('在职教育', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText($formatter->asEducationLevel($model['in_service_education_level']), $fancyTableFontStyle);
        $table->addCell($col4 + $col5 + $col6, array('gridSpan' => 3, 'valign' => 'center'))->addText('毕业院校系及专业', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['in_service_education_school'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('职称（职级）', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model->professional_title, $fancyTableFontStyle);
        $table->addCell($col3 + $col4 + $col5, array('gridSpan' => 3, 'valign' => 'center'))->addText('工作单位及职务', $fancyTableFontStyle);
        $table->addCell($col6 + $col7 + $col8 + $col9, array('gridSpan' => 4, 'valign' => 'center'))->addText($model->work_information, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('类别', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, ['gridSpan' => 8])->addText($formatter->asPersonCategory($model->category), $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1 + $col2, array('gridSpan' => 2, 'valign' => 'center'))->addText('主要社会职务', $fancyTableFontStyle);
        $table->addCell($col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 7, 'valign' => 'center'))->addText($model->social_title, $fancyTableFontStyle);

        $table->addRow(2000);
        $table->addCell($col1, $fancyTableCellStyle)->addText('个人简历及获奖情况', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->intro, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('政治安排', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->political_arrangements, $fancyTableFontStyle);

        $familySocialRelations = $model['familySocialRelations'];
        array_unshift($familySocialRelations, [
            'title' => '称谓',
            'username' => '姓名',
            'birthday' => '出生年月',
            'party' => '党派',
            'work_information' => '工作单位及职务',
        ]);
        foreach ($familySocialRelations as $i => $relation) {
            $table->addRow(200);
            if ($i == 0) {
                $table->addCell($col1, array('vMerge' => 'restart', 'valign' => 'center'))->addText('家庭主要成员及社会关系', $fancyTableFontStyle);
                $birthday = $relation['birthday'];
                $party = $relation['party'];
            } else {
                $table->addCell($col1, $cellRowContinue);
                $birthday = date('Y.m.d', $relation['birthday']);
                $party = $formatter->asParty($relation['party']);
            }
            $table->addCell($col2, $fancyTableCellStyle)->addText($relation['title'], $fancyTableFontStyle);
            $table->addCell($col3, $fancyTableCellStyle)->addText($relation['username'], $fancyTableFontStyle);

            $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($birthday, $fancyTableFontStyle);
            $table->addCell($col6, $fancyTableCellStyle)->addText($party, $fancyTableFontStyle);
            $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($relation['work_information'], $fancyTableFontStyle);
        }
        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('备注', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->remark, $fancyTableFontStyle);

        $footer = $section->addFooter();
        $footer->addText('录入单位：' . $model['region']['name'], null, array('alignment' => Jc::RIGHT));

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $filename = $model['id'] . '.docx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filename;
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, "{$model['username']}.docx", ['mimeType' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
    }

    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
