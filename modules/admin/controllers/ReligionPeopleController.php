<?php

namespace app\modules\admin\controllers;

use app\models\Constant;
use app\models\Person;
use app\models\PersonSearch;
use app\models\ReligionPerson;
use app\models\ReligionPersonProfile;
use app\modules\admin\components\QueryCondition;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use PhpOffice\PhpWord\SimpleType\JcTable;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * 宗教界代表人士登记表
 */
class ReligionPeopleController extends PeopleController
{

    public function init()
    {
        parent::init();
        $this->type = Constant::TYPE_ZONG_JIAO_JIE;
    }

    /**
     * Lists all DemocraticPartyPerson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['PersonSearch']['type'] = $this->type;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new DemocraticPartyPerson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $person = new ReligionPerson();
        $person->type = $this->type;
        $model = new ReligionPersonProfile();

        if ($model->load(Yii::$app->request->post()) && $person->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $person->validate() && $isValid;
            if ($isValid) {
                $person->save(false);
                $model->person_id = $person->id;
                $model->save(false);

                return $this->redirect(['view', 'id' => $person->id]);
            }
        }

        return $this->render('create', [
                'model' => $person,
                'profile' => $model,
        ]);
    }

    /**
     * Updates an existing DemocraticPartyPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $person = $this->findModel($id);
        $model = ReligionPersonProfile::findOne(['person_id' => $person->id]);
        if (!$model) {
            $model = new ReligionPersonProfile();
        }

        if ($model->load(Yii::$app->request->post()) && $person->load(Yii::$app->request->post())) {
            $isValid = $model->validate();
            $isValid = $person->validate() && $isValid;
            if ($isValid) {
                $person->save(false);
                $model->person_id = $person->id;
                $model->save(false);

                return $this->redirect(['view', 'id' => $person->id]);
            }
        }

        return $this->render('update', [
                'model' => $person,
                'profile' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
                'model' => $model,
        ]);
    }

    /**
     * 导出为 Excel 文件
     */
    public function actionToExcel()
    {
        $query = QueryCondition::get('PERSON');
        if ($query) {
            $items = $query->all();
        } else {
            $items = ReligionPerson::find()->all();
        }
        $formatter = Yii::$app->getFormatter();
        $phpExcel = new PHPExcel();

        $phpExcel->getProperties()->setCreator("Microsoft")
            ->setLastModifiedBy("Microsoft")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Person");

        $phpExcel->setActiveSheetIndex(0);
        $activeSheet = $phpExcel->getActiveSheet();
        $phpExcel->getDefaultStyle()
            ->getFont()->setSize(14);

        $activeSheet->getDefaultRowDimension()->setRowHeight(25);

        $cols = ['A' => 4, 'B' => 7, 'C' => 5, 'D' => 10, 'E' => 16, 'F' => 12, 'G' => 12, 'H' => 20, 'I' => 16, 'J' => 12, 'K' => 12, 'L' => 12];
        foreach ($cols as $col => $width) {
            $activeSheet->getColumnDimension($col)->setWidth($width);
        }

        $activeSheet->setCellValue('A1', '宗教界代表人士登记汇总表')->mergeCells('A1:L1')->getStyle()->applyFromArray(array(
            'font' => array(
                'bold' => true,
                'size' => 16,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        ));

        $activeSheet->setCellValue("A2", '序号')
            ->setCellValue("B2", '姓名')
            ->setCellValue("C2", '性别')
            ->setCellValue("D2", '民族')
            ->setCellValue("E2", '出生年月')
            ->setCellValue("F2", '信仰宗教')
            ->setCellValue("G2", '教职')
            ->setCellValue("H2", '主要活动场所')
            ->setCellValue("I2", '政治安排')
            ->setCellValue("J2", '手机号码')
            ->setCellValue("K2", '录入单位')
            ->setCellValue("L2", '备注');

        $row = 3;
        foreach ($items as $i => $item) {
            $activeSheet->setCellValue("A{$row}", $i + 1)
                ->setCellValue("B{$row}", $item['username'])
                ->setCellValue("C{$row}", $formatter->asSex($item['sex']))
                ->setCellValue("D{$row}", $formatter->asNation($item['nation']))
                ->setCellValue("e{$row}", date('Y/m/d', $item['birthday']))
                ->setCellValue("F{$row}", $formatter->asReligion($item['party']))
                ->setCellValue("G{$row}", $item['professional_title'])
                ->setCellValue("H{$row}", $item['work_information'])
                ->setCellValue("I{$row}", $item['political_arrangements'])
                ->setCellValue("J{$row}", $item['mobile_phone'])
                ->setCellValue("K{$row}", $item['region']['name'])
                ->setCellValue("L{$row}", $item['remark']);
            $row++;
        }

        $phpExcel->getActiveSheet()->setTitle('汇总表');
        $phpExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $filename = '宗教界代表.xlsx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . urlencode($filename);
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, $filename, ['mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);
    }

    /**
     * 导出为 Word 文件
     * @param integer $id
     */
    public function actionToWord($id)
    {
        $model = $this->findModel($id);
        $formatter = Yii::$app->getFormatter();
        $phpWord = new PhpWord();
        $phpWord->setDefaultFontName('宋体'); // 全局字体
        $phpWord->setDefaultFontSize(9);

        $header = array('size' => 16, 'bold' => true);
        $cellRowContinue = array('vMerge' => 'continue');
        $fancyTableStyleName = 'Fancy Table';
        $fancyTableStyle = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => JcTable::CENTER);
        $fancyTableCellStyle = array('valign' => 'center', 'borderRightSize' => 0, 'fontSize' => 30);
        $fancyTableFontStyle = array('align' => 'center');
        $phpWord->addParagraphStyle('pStyle', array('align' => 'center'));
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle);
        $section = $phpWord->addSection();
        $section->addText('宗教界人士登记表', $header, 'pStyle');
        $section->addTextBreak(1);
        $table = $section->addTable($fancyTableStyleName);

        $col1 = 700;
        $col2 = 1400;
        $col3 = 900;
        $col4 = 700;
        $col5 = 800;
        $col6 = 1200;
        $col7 = 1150;
        $col8 = 1600;
        $col9 = 1500;

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('姓名', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model['username'], $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('性别', $fancyTableFontStyle);
        $table->addCell($col4, $fancyTableCellStyle)->addText($formatter->asSex($model['sex']), $fancyTableFontStyle);
        $table->addCell($col5, $fancyTableCellStyle)->addText('民族', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText($formatter->asNation($model['nation']), $fancyTableFontStyle);
        $table->addCell($col7, $fancyTableCellStyle)->addText('出生年月', $fancyTableFontStyle);
        $table->addCell($col8, $fancyTableCellStyle)->addText(date('Y.m.d', $model['birthday']), $fancyTableFontStyle);
        $c = $table->addCell($col9, array('vMerge' => 'restart', 'valign' => 'center'));
        if (!empty($model['photo'])) {
            $c->createTextRun()->addImage(Yii::getAlias('@webroot') . $model['photo'], array('width' => 110, 'height' => 140, 'marginTop' => -1, 'marginLeft' => -1));
        } else {
            $c->addTextRun(array('alignment' => Jc::CENTER))->addText('');
        }

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('国籍', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($formatter->asNationality($model['nationality']), $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('籍贯', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['native_place'], $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText('出生地点', $fancyTableFontStyle);
        $table->addCell($col7 + $col8, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['birthplace'], $fancyTableFontStyle);
        $table->addCell($col9, $cellRowContinue);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('信仰宗教类别', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($formatter->asParty($model['party']), $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('入教年月', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['join_party_date'] ? $formatter->asDate($model['join_party_date']) : '', $fancyTableFontStyle);
        $table->addCell($col6, $fancyTableCellStyle)->addText('户籍所在地', $fancyTableFontStyle);
        $table->addCell($col7 + $col8, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['registered_residence_address'], $fancyTableFontStyle);
        $table->addCell($col9, $cellRowContinue);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('参加工作时间', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model['join_job_date'] ? $formatter->asDate($model['join_job_date']) : '', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText('移动电话', $fancyTableFontStyle);
        $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($model['mobile_phone'], $fancyTableFontStyle);
        $table->addCell($col6, array('valign' => 'center'))->addText('身份证号码', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['id_card_number'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, array('vMerge' => 'restart', 'valign' => 'center'))->addText('学历学位', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText('全日制教育', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText($formatter->asEducationLevel($model['full_time_education_level']), $fancyTableFontStyle);
        $table->addCell($col4 + $col5 + $col6, array('gridSpan' => 3, 'valign' => 'center'))->addText('毕业院校系及专业', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['full_time_education_school'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $cellRowContinue);
        $table->addCell($col2, $fancyTableCellStyle)->addText('在职教育', $fancyTableFontStyle);
        $table->addCell($col3, $fancyTableCellStyle)->addText($formatter->asEducationLevel($model['in_service_education_level']), $fancyTableFontStyle);
        $table->addCell($col4 + $col5 + $col6, array('gridSpan' => 3, 'valign' => 'center'))->addText('毕业院校系及专业', $fancyTableFontStyle);
        $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($model['in_service_education_school'], $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('职称（职级）', $fancyTableFontStyle);
        $table->addCell($col2, $fancyTableCellStyle)->addText($model->professional_title, $fancyTableFontStyle);
        $table->addCell($col3 + $col4 + $col5, array('gridSpan' => 3, 'valign' => 'center'))->addText('工作单位及职务', $fancyTableFontStyle);
        $table->addCell($col6 + $col7 + $col8 + $col9, array('gridSpan' => 4, 'valign' => 'center'))->addText($model->work_information, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('类别', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, ['gridSpan' => 8])->addText($formatter->asPersonCategory($model->category), $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1 + $col2, array('gridSpan' => 2, 'valign' => 'center'))->addText('主要社会职务', $fancyTableFontStyle);
        $table->addCell($col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 7, 'valign' => 'center'))->addText($model->social_title, $fancyTableFontStyle);

        $table->addRow(2000);
        $table->addCell($col1, $fancyTableCellStyle)->addText('个人简历及获奖情况', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->intro, $fancyTableFontStyle);

        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('政治安排', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->political_arrangements, $fancyTableFontStyle);

        $familySocialRelations = $model['familySocialRelations'];
        array_unshift($familySocialRelations, [
            'title' => '称谓',
            'username' => '姓名',
            'birthday' => '出生年月',
            'party' => '党派',
            'work_information' => '工作单位及职务',
        ]);
        foreach ($familySocialRelations as $i => $relation) {
            $table->addRow(200);
            if ($i == 0) {
                $table->addCell($col1, array('vMerge' => 'restart', 'valign' => 'center'))->addText('家庭主要成员及社会关系', $fancyTableFontStyle);
                $birthday = $relation['birthday'];
                $party = $relation['party'];
            } else {
                $table->addCell($col1, $cellRowContinue);
                $birthday = date('Y.m.d', $relation['birthday']);
                $party = $formatter->asParty($relation['party']);
            }
            $table->addCell($col2, $fancyTableCellStyle)->addText($relation['title'], $fancyTableFontStyle);
            $table->addCell($col3, $fancyTableCellStyle)->addText($relation['username'], $fancyTableFontStyle);

            $table->addCell($col4 + $col5, array('gridSpan' => 2, 'valign' => 'center'))->addText($birthday, $fancyTableFontStyle);
            $table->addCell($col6, $fancyTableCellStyle)->addText($party, $fancyTableFontStyle);
            $table->addCell($col7 + $col8 + $col9, array('gridSpan' => 3, 'valign' => 'center'))->addText($relation['work_information'], $fancyTableFontStyle);
        }
        $table->addRow(200);
        $table->addCell($col1, $fancyTableCellStyle)->addText('备注', $fancyTableFontStyle);
        $table->addCell($col2 + $col3 + $col4 + $col5 + $col6 + $col7 + $col8 + $col9, array('gridSpan' => 8, 'valign' => 'center'))->addText($model->remark, $fancyTableFontStyle);

        $footer = $section->addFooter();
        $footer->addText('录入单位：' . $model['region']['name'], null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT));

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $filename = $model['id'] . '.docx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filename;
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, "{$model['username']}.docx", ['mimeType' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']);
    }

    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReligionPerson::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
