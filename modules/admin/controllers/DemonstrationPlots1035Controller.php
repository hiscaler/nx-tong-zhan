<?php

namespace app\modules\admin\controllers;

use app\models\DemonstrationPlot;
use app\models\DemonstrationPlot1035;
use app\models\DemonstrationPlot1035Search;
use app\modules\admin\components\QueryCondition;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * 凝心聚力“十三五”行动示范点信息登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class DemonstrationPlots1035Controller extends PositionController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DemonstrationPlot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DemonstrationPlot1035Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DemonstrationPlot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DemonstrationPlot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DemonstrationPlot1035();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DemonstrationPlot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DemonstrationPlot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * 导出为 Excel 文件
     */
    public function actionToExcel()
    {
        $query = QueryCondition::get('DemonstrationPlot');
        if ($query) {
            $items = $query->all();
        } else {
            $items = DemonstrationPlot::find()->all();
        }
        $formatter = Yii::$app->getFormatter();
        $phpExcel = new PHPExcel();

        $phpExcel->getProperties()->setCreator("Microsoft")
            ->setLastModifiedBy("Microsoft")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Person");

        $phpExcel->setActiveSheetIndex(0);
        $activeSheet = $phpExcel->getActiveSheet();
        $phpExcel->getDefaultStyle()
            ->getFont()->setSize(14);

        $activeSheet->getDefaultRowDimension()->setRowHeight(25);

        $cols = ['A' => 4, 'B' => 7, 'C' => 5, 'D' => 10, 'E' => 16, 'F' => 12, 'G' => 12, 'H' => 20, 'I' => 16, 'J' => 12, 'K' => 12, 'L' => 12];
        foreach ($cols as $col => $width) {
            $activeSheet->getColumnDimension($col)->setWidth($width);
        }

        $activeSheet->setCellValue('A1', '宁乡县凝心聚力“十三五”行动示范点情况汇总表')->mergeCells('A1:K1')->getStyle()->applyFromArray(array(
            'font' => array(
                'bold' => true,
                'size' => 16,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        ));

        $activeSheet->setCellValue("A2", '序号')
            ->setCellValue("B2", '创建点名称')
            ->setCellValue("C2", '类别')
            ->setCellValue("D2", '统战元素')
            ->setCellValue("E2", '详细地址')
            ->setCellValue("F2", '负责人姓名及职务')
            ->setCellValue("G2", '联系方式')
            ->setCellValue("H2", '县级授牌时间')
            ->setCellValue("I2", '市级授牌时间')
            ->setCellValue("J2", '省级授牌时间')
            ->setCellValue("K2", '录入单位');

        $row = 3;
        foreach ($items as $i => $item) {
            $activeSheet->setCellValue("A{$row}", $i + 1)
                ->setCellValue("B{$row}", $item['name'])
                ->setCellValue("C{$row}", $item->getCategoryNames())
                ->setCellValue("D{$row}", $item->elements)
                ->setCellValue("E{$row}", $item->address)
                ->setCellValue("F{$row}", $item['superintendent_informations'])
                ->setCellValue("G{$row}", $item->superintendent_mobile_phone)
                ->setCellValue("H{$row}", $item->county_level_awarding_datetime ? $formatter->asDate($item->county_level_awarding_datetime) : '')
                ->setCellValue("I{$row}", $item->city_level_awarding_datetime ? $formatter->asDate($item->city_level_awarding_datetime) : '')
                ->setCellValue("J{$row}", $item->province_level_awarding_datetime ? $formatter->asDate($item->province_level_awarding_datetime) : '')
                ->setCellValue("K{$row}", $item['inputRegion']['name']);
            $row++;
        }

        $phpExcel->getActiveSheet()->setTitle('汇总表');
        $phpExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $filename = date('Ymd') . '.xlsx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filename;
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, $filename, ['mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);
    }

    /**
     * Finds the DemonstrationPlot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DemonstrationPlot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DemonstrationPlot1035::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
