<?php

namespace app\modules\admin\controllers;

use app\models\Constant;
use app\models\OtherPerson;
use app\models\PersonSearch;
use app\modules\admin\components\QueryCondition;
use Yii;

/**
 * 其他需要联系和团结的人员登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class OtherPeopleController extends PeopleController
{

    public function init()
    {
        parent::init();
        $this->type = Constant::TYPE_OTHER;
    }

    /**
     * Lists all DemocraticPartyPerson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['PersonSearch']['type'] = $this->type;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OtherPerson();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('/people/create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * 导出为 Excel 文件
     */
    public function actionToExcel()
    {
        $query = QueryCondition::get('PERSON');
        if ($query) {
            $items = $query->all();
        } else {
            $items = OtherPerson::find()->all();
        }
        $formatter = Yii::$app->getFormatter();
        $phpExcel = new \PHPExcel();

        $phpExcel->getProperties()->setCreator("Microsoft")
            ->setLastModifiedBy("Microsoft")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Person");

        $phpExcel->setActiveSheetIndex(0);
        $activeSheet = $phpExcel->getActiveSheet();
        $phpExcel->getDefaultStyle()
            ->getFont()->setSize(14);

        $activeSheet->getDefaultRowDimension()->setRowHeight(25);

        $cols = ['A' => 4, 'B' => 7, 'C' => 5, 'D' => 10, 'E' => 16, 'F' => 12, 'G' => 12, 'H' => 20, 'I' => 16, 'J' => 12, 'K' => 12, 'L' => 12];
        foreach ($cols as $col => $width) {
            $activeSheet->getColumnDimension($col)->setWidth($width);
        }

        $activeSheet->setCellValue('A1', '其他需要联系和团结的党外代表人士登记汇总表')->mergeCells('A1:L1')->getStyle()->applyFromArray(array(
            'font' => array(
                'bold' => true,
                'size' => 16,
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        ));

        $activeSheet->setCellValue("A2", '序号')
            ->setCellValue("B2", '姓名')
            ->setCellValue("C2", '性别')
            ->setCellValue("D2", '民族')
            ->setCellValue("E2", '出生年月')
            ->setCellValue("F2", '党派')
            ->setCellValue("G2", '学历学位')
            ->setCellValue("H2", '工作单位及职务')
            ->setCellValue("I2", '政治安排')
            ->setCellValue("J2", '手机号码')
            ->setCellValue("K2", '录入单位')
            ->setCellValue("L2", '备注');

        $row = 3;
        foreach ($items as $i => $item) {
            $activeSheet->setCellValue("A{$row}", $i + 1)
                ->setCellValue("B{$row}", $item['username'])
                ->setCellValue("C{$row}", $formatter->asSex($item['sex']))
                ->setCellValue("D{$row}", $formatter->asNation($item['nation']))
                ->setCellValue("E{$row}", date('Y/m/d', $item['birthday']))
                ->setCellValue("F{$row}", $formatter->asParty($item['party']))
                ->setCellValue("G{$row}", $formatter->asEducationLevel($item['full_time_education_level']))
                ->setCellValue("H{$row}", $item['work_information'])
                ->setCellValue("I{$row}", $item['political_arrangements'])
                ->setCellValue("J{$row}", $item['mobile_phone'])
                ->setCellValue("K{$row}", $item['region']['name'])
                ->setCellValue("L{$row}", $item['remark']);
            $row++;
        }

        $phpExcel->getActiveSheet()->setTitle('汇总表');
        $phpExcel->setActiveSheetIndex(0);
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $filename = '其他人员.xlsx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . urlencode($filename);
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, $filename, ['mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);
    }

}
