<?php

namespace app\modules\admin\controllers;

use app\models\DemocraticPartyOrganization;
use app\models\DemocraticPartyOrganizationSearch;
use app\modules\admin\components\QueryCondition;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * 民主党派组织数据管理
 *
 */
class DemocraticPartyOrganizationsController extends OrganizationController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DemocraticPartyOrganization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DemocraticPartyOrganizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DemocraticPartyOrganization model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DemocraticPartyOrganization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DemocraticPartyOrganization();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DemocraticPartyOrganization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DemocraticPartyOrganization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * 导出为 Excel 文件
     */
    public function actionToExcel()
    {
        $query = QueryCondition::get('DemocraticPartyOrganization');
        if ($query) {
            $items = $query->all();
        } else {
            $items = DemocraticPartyOrganization::find()->all();
        }
        $formatter = Yii::$app->getFormatter();
        $phpExcel = new PHPExcel();

        $phpExcel->getProperties()->setCreator("Microsoft")
            ->setLastModifiedBy("Microsoft")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Person");

        $phpExcel->setActiveSheetIndex(0);
        $activeSheet = $phpExcel->getActiveSheet();
        $phpExcel->getDefaultStyle()
            ->getFont()->setSize(14);

        $activeSheet->getDefaultRowDimension()->setRowHeight(25);

        $cols = ['A' => 5, 'B' => 15, 'C' => 12, 'D' => 10, 'E' => 16, 'F' => 12, 'G' => 30, 'H' => 16, 'I' => 16, 'J' => 12, 'K' => 12, 'L' => 12];
        foreach ($cols as $col => $width) {
            $activeSheet->getColumnDimension($col)->setWidth($width);
        }

        $activeSheet->setCellValue('A1', '民主党派汇总表')->mergeCells('A1:J1')->getStyle()->applyFromArray(array(
            'font' => array(
                'bold' => true,
                'size' => 16,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        ));

        $activeSheet->setCellValue("A2", '序号')
            ->setCellValue("B2", '名称')
            ->setCellValue("C2", '成立时间')
            ->setCellValue("D2", '办公（活动）场所')
            ->setCellValue("E2", '党（会）员人数')
            ->setCellValue("F2", '负责人姓名')
            ->setCellValue("G2", '负责人工作单位及职务')
            ->setCellValue("H2", '政治安排')
            ->setCellValue("I2", '联系方式')
            ->setCellValue("J2", '录入单位');

        $row = 3;
        foreach ($items as $i => $item) {
            $activeSheet->setCellValue("A{$row}", $i + 1)
                ->setCellValue("B{$row}", $item['name'])
                ->setCellValue("C{$row}", $item->found_datetime ? $formatter->asDate($item->found_datetime) : '')
                ->setCellValue("D{$row}", $item->address)
                ->setCellValue("E{$row}", $item->people_count)
                ->setCellValue("F{$row}", $item->superintendent_username)
                ->setCellValue("G{$row}", $item->superintendent_mobile_phone)
                ->setCellValue("H{$row}", $item->superintendent_work_information)
                ->setCellValue("I{$row}", $item->political_arrangements)
                ->setCellValue("K{$row}", $item['inputRegion']['name']);
            $row++;
        }

        $phpExcel->getActiveSheet()->setTitle('汇总表');
        $phpExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $filename = date('Ymd') . '.xlsx';
        $file = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $filename;
        $objWriter->save($file);

        Yii::$app->getResponse()->sendFile($file, $filename, ['mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']);
    }

    /**
     * Finds the DemocraticPartyOrganization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DemocraticPartyOrganization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DemocraticPartyOrganization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
