<?php

use app\models\Option;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="form-nonpublic-economic-people" class="table-form table-form-comon">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'username') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'username', $options)->textInput(['maxlength' => true, 'class' => 'username'])->label(false) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'nation') ?>
                        </th>
                        <td class="col6789" colspan="4">
                            <?= $form->field($model, 'nation', $options)->dropDownList(Option::nationOptions())->label(false) ?>
                        </td>
                        <td class="col10 last" rowspan="3">
                            <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            <?= $form->field($model, 'photo', $options)->fileInput()->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'nationality') ?>
                        </th>
                        <td class="col2"><?= $form->field($model, 'nationality', $options)->dropDownList(Option::nationalityOptions())->label(false) ?></td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'native_place') ?>
                        </th>
                        <td class="col45" colspan="2"><?= $form->field($model, 'native_place', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col67" colspan="2">
                            <?= Html::activeLabel($model, 'birthplace') ?>
                        </th>
                        <td class="col89" colspan="2"><?= $form->field($model, 'birthplace', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'party') ?></th>
                        <td class="col2"><?= $form->field($model, 'party', $options)->dropDownList(Option::partyOptions())->label(false) ?></td>
                        <th class="col3"><?= Html::activeLabel($model, 'join_party_date') ?></th>
                        <td class="col45" colspan="2">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'join_party_date',
                                'pickerType' => 'date',
                                'htmlOptions' => [
                                    'label' => false,
                                ]
                            ]);
                            ?>
                        </td>
                        <th class="col67" colspan="2"><?= Html::activeLabel($model, 'registered_residence_address') ?></th>
                        <td class="col89" colspan="2"><?= $form->field($model, 'registered_residence_address', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'join_job_date') ?></th>
                        <td class="col2">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'join_job_date',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                        <th class="col3"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                        <td class="col45" colspan="2"><?= $form->field($model, 'mobile_phone', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col67" colspan="2"><?= Html::activeLabel($model, 'id_card_number') ?></th>
                        <td class="col8910" colspan="3"><?= $form->field($model, 'id_card_number', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th rowspan="2" class="col1"><label for="">学历学位</label></th>
                        <th class="col2"><?= Html::activeLabel($model, 'full_time_education_level') ?></th>
                        <td class="col3"><?= $form->field($model, 'full_time_education_level')->dropDownList(Option::educationLevelOptions())->label(false) ?></td>
                        <th class="col4567" colspan="4"><?= Html::activeLabel($model, 'full_time_education_school') ?></th>
                        <td class="col8910 last" colspan="3"><?= $form->field($model, 'full_time_education_school')->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col2"><?= Html::activeLabel($model, 'in_service_education_level') ?></th>
                        <td class="col3"><?= $form->field($model, 'in_service_education_level')->dropDownList(Option::educationLevelOptions())->label(false) ?></td>
                        <th class="col4567" colspan="4"><?= Html::activeLabel($model, 'in_service_education_school') ?></th>
                        <td class="col8910 last" colspan="3"><?= $form->field($model, 'in_service_education_school')->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col12">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col345678910 last" colspan="9"><?= $form->field($model, 'category', $options)->dropDownList(\app\models\Person::categoryOptions(), ['prompt' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'work_information') ?></th>
                        <td class="col234" colspan="3"><?= $form->field($model, 'work_information', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col56" colspan="2">
                            <?= Html::activeLabel($model, 'professional_title') ?>
                        </th>
                        <td class="col78910" colspan="4"><?= $form->field($model, 'professional_title', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'social_title') ?>
                        </th>
                        <td class="col23" colspan="2"><?= $form->field($model, 'social_title', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col34" colspan="2">
                            <?= Html::activeLabel($profile, 'evaluation_date') ?>
                        </th>
                        <td class="col56" colspan="2"><?= $form->field($profile, 'evaluation_date', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col78" colspan="2">
                            <?= Html::activeLabel($profile, 'evaluation_level') ?>
                        </th>
                        <td class="col910" colspan="2"><?= $form->field($profile, 'evaluation_level', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($profile, 'enterprise_intro') ?></th>
                        <td class="col2345678910 last" colspan="9"><?= $form->field($profile, 'enterprise_intro')->textarea(['rows' => 3, 'placeholder' => '注册时间、地点、总资产、年利润、年纳税额等。'])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                        <td class="col2345678910 last" colspan="9"><?= $form->field($model, 'intro')->textarea(['rows' => 6, 'placeholder' => '（控制150字以内）'])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                        <td class="col2345678910 last" colspan="9"><?= $form->field($model, 'political_arrangements')->textarea(['rows' => 6, 'placeholder' => '政治安排主要指导担任代表、委员情况；
奖励荣誉主要指获县级以上的。'])->label(false) ?></td>
                    </tr>

                    <?=
                    $this->render('/people/_familySocialRelationsForm', [
                        'form' => $form,
                        'model' => $model,
                    ])
                    ?>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'remark') ?>
                        </th>
                        <td class="col2345678910 last" colspan="9"><?= $form->field($model, 'remark', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>

<?php app\modules\admin\components\JsBlock::begin() ?>
<script type="text/javascript">
    $(function () {
        var partyId = '<?= \yii\helpers\Html::getInputId($model, 'party') ?>';
        var joinPartyDateId = '<?= \yii\helpers\Html::getInputId($model, 'join_party_date') ?>';
        $('#' + partyId).change(function () {
            if ($(this).val() == 10) {
                $('.field-' + joinPartyDateId).hide();
            } else {
                $('.field-' + joinPartyDateId).show();
            }
        });
    });
</script>
<?php app\modules\admin\components\JsBlock::end() ?>
