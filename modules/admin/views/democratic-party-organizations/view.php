<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DemocraticPartyOrganization */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '民主党派', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="democratic-party-organization-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'found_datetime:date',
            'address',
            'people_count',
            'honour_reward',
            'superintendent_username',
            'superintendent_mobile_phone',
            'superintendent_work_information',
            'political_arrangements',
            'intro:ntext',
            'input_region_id',
            'created_at:date',
            'updated_at:date',
        ],
    ])
    ?>

</div>
