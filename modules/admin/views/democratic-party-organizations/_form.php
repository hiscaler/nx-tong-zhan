<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DemocraticPartyOrganization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-democratic-party-organization">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'name') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'found_datetime') ?>
                        </th>
                        <td class="col4">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'found_datetime',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'address') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'address', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'people_count') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'people_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'honour_reward') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'honour_reward', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'superintendent_username') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'superintendent_username', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'superintendent_mobile_phone') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'superintendent_mobile_phone', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'superintendent_work_information') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'superintendent_work_information', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'political_arrangements') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'political_arrangements', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'intro') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'intro', $options)->textarea() ?>
                        </td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>