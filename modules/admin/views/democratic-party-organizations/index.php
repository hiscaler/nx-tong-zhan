<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DemocraticPartyOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '民主党派';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="democratic-party-organization-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            'name',
            [
                'attribute' => 'found_datetime',
                'format' => 'date',
                'contentOptions' => ['class' => 'date'],
            ],
            'address',
            [
                'attribute' => 'people_count',
                'contentOptions' => ['class' => 'number'],
            ],
            [
                'attribute' => 'superintendent_username',
                'contentOptions' => ['class' => 'username'],
            ],
            'superintendent_work_information',
            'political_arrangements',
            [
                'attribute' => 'superintendent_mobile_phone',
                'contentOptions' => ['class' => 'mobile-phone'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => array('class' => 'buttons-3 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
