<?php
/* @var $this yii\web\View */
/* @var $model app\models\DemocraticPartyOrganization */

$this->title = '更新: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '民主党派', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';
?>
<div class="democratic-party-organization-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
