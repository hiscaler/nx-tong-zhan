<?php
/* @var $this yii\web\View */
/* @var $model app\models\DemocraticPartyOrganization */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '民主党派', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="democratic-party-organization-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
