<?php
/* @var $this yii\web\View */
/* @var $model app\models\Organization */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '商会组织信息登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="organization-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
