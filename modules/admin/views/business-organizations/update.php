<?php
/* @var $this yii\web\View */
/* @var $model app\models\Organization */

$this->title = '更新: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '商会组织信息登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '详情', 'url' => ['view', 'id' => $model->id]],
];
?>
<div class="organization-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
