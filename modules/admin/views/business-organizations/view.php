<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Organization */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '商会组织信息登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];
?>
<div class="organization-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'english_name',
            'english_short_name',
            'address',
            'company_members_count',
            'person_members_count',
            'director_count',
            'executive_director_count',
            'legal_person_username',
            'legal_person_mobile_phone',
            'legal_person_work_information',
            'certificate_number',
            'certificate_issue_datetime:datetime',
            'intro:ntext',
            'input_region_id',
            'created_at',
            'updated_at',
        ],
    ])
    ?>

</div>
