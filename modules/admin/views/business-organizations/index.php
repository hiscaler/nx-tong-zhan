<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '商会组织信息登记表';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="organization-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            'name',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return $model->getCategoryNameString();
                }
            ],
            'address',
            [
                'attribute' => 'company_members_count',
                'contentOptions' => ['class' => 'number'],
            ],
            [
                'attribute' => 'person_members_count',
                'contentOptions' => ['class' => 'number'],
            ],
            'certificate_number',
            [
                'attribute' => 'legal_person_username',
                'contentOptions' => ['class' => 'username'],
            ],
            'legal_person_work_information',
            [
                'attribute' => 'legal_person_mobile_phone',
                'contentOptions' => ['class' => 'mobile-phone'],
            ],
            [
                'attribute' => 'inputRegion.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => array('class' => 'buttons-3 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
