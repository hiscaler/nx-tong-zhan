<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '香港同胞、澳门同胞登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];

$formatter = Yii::$app->getFormatter();
?>
<div class="person-view">

    <div class="table-form table-form-comon">
        <div class="outer">
            <div class="inner">

                <table>
                    <tbody>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'username') ?>
                            </th>
                            <td class="col2">
                                <?= $model['username'] ?>
                            </td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'sex') ?>
                            </th>
                            <td class="col4">
                                <?= $formatter->asSex($model['sex']) ?>
                            </td>
                            <th class="col5">
                                <?= Html::activeLabel($model, 'birthday') ?>
                            </th>
                            <td class="col6">
                                <?= date('Y年n月', $model['birthday']) ?>
                            </td>
                            <td class="col7 last" rowspan="3">
                                <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'nationality') ?>
                            </th>
                            <td class="col2"><?= $formatter->asNationality($model['nationality']) ?></td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'nation') ?>
                            </th>
                            <td class="col4">
                                <?= $formatter->asNation($model['nation']) ?>
                            </td>
                            <th class="col5">
                                <?= Html::activeLabel($model, 'birthplace') ?>
                            </th>
                            <td class="col6"><?= $model['birthplace'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'native_place') ?></th>
                            <td class="col234" colspan="3"><?= $model['native_place'] ?></td>
                            <th class="col5"><?= Html::activeLabel($profile, 'main_live_address') ?></th>
                            <td class="col6">
                                <?= $profile['main_live_address'] ?>
                            </td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                            <td class="col234" colspan="3"><?= $model['mobile_phone'] ?></td>
                            <th class="col5"><?= Html::activeLabel($model, 'full_time_education_level') ?></th>
                            <td class="col67" colspan="2">
                                <?= $formatter->asEducationLevel($model['full_time_education_level']) ?>
                            </td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($profile, 'tel') ?></th>
                            <td class="col234" colspan="3"><?= $profile['tel'] ?></td>
                            <th class="col5"><?= Html::activeLabel($model, 'work_information') ?></th>
                            <td class="col67" colspan="2">
                                <?= $model['work_information'] ?>
                            </td>
                        </tr>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'social_title') ?>
                            </th>
                            <td class="col234567 last" colspan="6"><?= $model['social_title'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                            <td class="col234567 last" colspan="6"><?= $model['political_arrangements'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                            <td class="col234567 last" colspan="6"><?= $model['intro'] ?></td>
                        </tr>

                        <?=
                        $this->render('/people/_familySocialRelations', [
                            'model' => $model,
                        ])
                        ?>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'remark') ?>
                            </th>
                            <td class="col234567 last" colspan="6"><?= $model['remark'] ?></td>
                        </tr>
                    </tbody>

                </table>

            </div>

        </div>
    </div>

</div>
