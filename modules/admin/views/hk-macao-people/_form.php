<?php

use app\models\Option;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-form-comon">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'username') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'username', $options)->textInput(['maxlength' => true, 'class' => 'username'])->label(false) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'sex') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'sex', $options)->dropDownList(Option::sexOptions())->label(false) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'birthday') ?>
                        </th>
                        <td class="col6">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'birthday',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                        <td class="col7 last" rowspan="3">
                            <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            <?= $form->field($model, 'photo', $options)->fileInput()->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'nationality') ?>
                        </th>
                        <td class="col2"><?= $form->field($model, 'nationality', $options)->dropDownList(Option::nationalityOptions())->label(false) ?></td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'nation') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'nation', $options)->dropDownList(Option::nationOptions())->label(false) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'birthplace') ?>
                        </th>
                        <td class="col6"><?= $form->field($model, 'birthplace', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'native_place') ?></th>
                        <td class="col234" colspan="3"><?= $form->field($model, 'native_place', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col5"><?= Html::activeLabel($profile, 'main_live_address') ?></th>
                        <td class="col6">
                            <?= $form->field($profile, 'main_live_address', $options)->textInput(['maxlength' => true])->label(false) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                        <td class="col234" colspan="3"><?= $form->field($model, 'mobile_phone', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col5"><?= Html::activeLabel($model, 'full_time_education_level') ?></th>
                        <td class="col67" colspan="2">
                            <?= $form->field($model, 'full_time_education_level', $options)->dropDownList(Option::educationLevelOptions())->label(false) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($profile, 'tel') ?></th>
                        <td class="col234" colspan="3"><?= $form->field($profile, 'tel', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col5"><?= Html::activeLabel($model, 'work_information') ?></th>
                        <td class="col67" colspan="2">
                            <?= $form->field($model, 'work_information', $options)->textInput(['maxlength' => true])->label(false) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'social_title') ?>
                        </th>
                        <td class="col234567 last" colspan="6"><?= $form->field($model, 'social_title', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col34567 last" colspan="6"><?= $form->field($model, 'category', $options)->dropDownList(\app\models\Person::categoryOptions(), ['prompt' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                        <td class="col234567 last" colspan="6"><?= $form->field($model, 'political_arrangements')->textarea(['rows' => 6, 'placeholder' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                        <td class="col234567 last" colspan="6"><?= $form->field($model, 'intro')->textarea(['rows' => 6, 'placeholder' => ''])->label(false) ?></td>
                    </tr>

                    <?=
                    $this->render('/people/_familySocialRelationsForm', [
                        'form' => $form,
                        'model' => $model,
                        'spanCols' => 6,
                    ])
                    ?>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'remark') ?>
                        </th>
                        <td class="col234567 last" colspan="6"><?= $form->field($model, 'remark', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="7">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
