<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-position">
    <div class="outer">
        <div class="inner">
            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'name') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'region_id') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'region_id', $options)->dropDownList(\app\models\Region::getTree(), ['prompt' => '请选择区域']) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col234 categories" colspan="3">
                            <?= $form->field($model, 'category', $options)->checkboxList(app\models\DemonstrationPlot1035::categoryOptions()) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'address') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'address', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'elements') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'elements', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'county_level_awarding_datetime') ?>
                        </th>
                        <td class="col4">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'county_level_awarding_datetime',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'city_level_awarding_datetime') ?>
                        </th>
                        <td class="col2">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'city_level_awarding_datetime',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'province_level_awarding_datetime') ?>
                        </th>
                        <td class="col4">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'province_level_awarding_datetime',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'superintendent_informations') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'superintendent_informations', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'superintendent_mobile_phone') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'superintendent_mobile_phone', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'honors_awards') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'honors_awards', $options)->textarea(['row' => 6]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'intro') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'intro', $options)->textarea(['row' => 6]) ?>
                        </td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>