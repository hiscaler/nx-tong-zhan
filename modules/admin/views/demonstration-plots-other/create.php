<?php
/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlot */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '其他', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="demonstration-plot-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
