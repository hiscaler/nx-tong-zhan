<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DemonstrationPlotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '其他';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '阵地列表', 'url' => ['index']],
];
?>
<div class="demonstration-plot-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            [
                'attribute' => 'region.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            'name',
            'address',
            // 'elements',
            [
                'attribute' => 'county_level_awarding_datetime',
                'format' => 'date',
                'contentOptions' => ['class' => 'date'],
            ],
            [
                'attribute' => 'city_level_awarding_datetime',
                'format' => 'date',
                'contentOptions' => ['class' => 'date'],
            ],
            [
                'attribute' => 'province_level_awarding_datetime',
                'format' => 'date',
                'contentOptions' => ['class' => 'date'],
            ],
            'superintendent_informations',
            'superintendent_mobile_phone',
            [
                'attribute' => 'inputRegion.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'contentOptions' => ['class' => 'datetime'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => array('class' => 'buttons-3 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
