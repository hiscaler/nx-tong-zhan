<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrganizationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-outside form-search form-layout-column">
    <div class="organization-search form">

        <?php
        $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
        ]);
        ?>

        <div class="entry">
            <div class="column">
                <?= $form->field($model, 'name') ?>
            </div>

            <div class="column">
                <?= $form->field($model, 'category')->dropDownList(\app\models\GroupOrganization::categoryOptions(), ['prompt' => '']) ?>
            </div>

        </div>

        <?php if (Yii::$app->getUser()->getIdentity()->getRole() == \app\models\User::ROLE_ADMINISTRATOR): ?>
            <div class="entry">
                <div class="column">
                    <?= $form->field($model, 'input_region_id')->dropDownList(\app\models\Region::getTree(), ['prompt' => '']) ?>
                </div>
            </div>
        <?php endif ?>

        <?php // echo $form->field($model, 'address') ?>

        <?php // echo $form->field($model, 'company_members_count') ?>

        <?php // echo $form->field($model, 'person_members_count') ?>

        <?php // echo $form->field($model, 'director_count') ?>

        <?php // echo $form->field($model, 'executive_director_count') ?>

        <?php // echo $form->field($model, 'legal_person_username') ?>

        <?php // echo $form->field($model, 'legal_person_mobile_phone') ?>

        <?php // echo $form->field($model, 'legal_person_work_information') ?>

        <?php // echo $form->field($model, 'certificate_number') ?>

        <?php // echo $form->field($model, 'certificate_issue_datetime') ?>

        <?php // echo $form->field($model, 'intro') ?>

        <?php // echo $form->field($model, 'input_region_id') ?>

        <?php // echo $form->field($model, 'enabled') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'updated_by')  ?>

        <div class="form-group buttons">
            <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
