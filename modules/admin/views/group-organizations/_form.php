<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Organization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-position-organization">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'name') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'ordering') ?>
                        </th>
                        <td class="col378" colspan="3">
                            <?= $form->field($model, 'ordering', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'english_name') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'english_name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'english_short_name') ?>
                        </th>
                        <td class="col378" colspan="3">
                            <?= $form->field($model, 'english_short_name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col2345678 categories" colspan="7">
                            <?= $form->field($model, 'category', $options)->checkboxList(app\models\GroupOrganization::categoryOptions()) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'address') ?>
                        </th>
                        <td class="col2345678" colspan="7">
                            <?= $form->field($model, 'address', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'company_members_count') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'company_members_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'person_members_count') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'person_members_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'director_count') ?>
                        </th>
                        <td class="col6">
                            <?= $form->field($model, 'director_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col7">
                            <?= Html::activeLabel($model, 'executive_director_count') ?>
                        </th>
                        <td class="col8">
                            <?= $form->field($model, 'executive_director_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'legal_person_username') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'legal_person_username', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'legal_person_mobile_phone') ?>
                        </th>
                        <td class="col378" colspan="3">
                            <?= $form->field($model, 'legal_person_mobile_phone', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'legal_person_work_information') ?>
                        </th>
                        <td class="col2345678" colspan="7">
                            <?= $form->field($model, 'legal_person_work_information', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'certificate_number') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'certificate_number', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'certificate_issue_datetime') ?>
                        </th>
                        <td class="col378" colspan="3">
                            <?= $form->field($model, 'certificate_issue_datetime', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'intro') ?>
                        </th>
                        <td class="col2345678" colspan="7">
                            <?= $form->field($model, 'intro', $options)->textarea(['row' => 6]) ?>
                        </td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
