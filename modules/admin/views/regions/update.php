<?php

/* @var $this yii\web\View */
/* @var $model app\models\Region */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Region',
    ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => '区域管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name];
$this->params['breadcrumbs'][] = '更新';

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '新增', 'url' => ['create']],
];

echo $this->render('_form', [
    'model' => $model,
]);

