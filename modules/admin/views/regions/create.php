<?php
/* @var $this yii\web\View */
/* @var $model app\models\Region */

$this->title = '新增';
$this->params['breadcrumbs'][] = ['label' => '区域管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="category-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
