<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '区域管理';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '添加', 'url' => ['create']],
];
?>
<div class="ibox float-e-margins">

    <div class="ibox-content">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return [
                    'data-tt-id' => $model['id'],
                    'data-tt-parent-id' => $model['parent_id'],
                    'style' => $model['parent_id'] ? 'display: none' : '',
                ];
            },
            'columns' => [
                [
                    'attribute' => 'name',
                    'header' => '区域名称',
                ],
                [
                    'attribute' => 'ordering',
                    'header' => '排序',
                    'contentOptions' => ['class' => 'number'],
                ],
                [
                    'attribute' => 'status',
                    'header' => '状态',
                    'format' => 'boolean',
                    'contentOptions' => ['class' => 'boolean pointer'],
                ],
                [
                    'attribute' => 'created_at',
                    'header' => '添加时间',
                    'format' => 'date',
                    'contentOptions' => ['class' => 'date']
                ],
                [
                    'attribute' => 'updated_at',
                    'header' => '更新时间',
                    'format' => 'date',
                    'contentOptions' => ['class' => 'date']
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'headerOptions' => array('class' => 'button-1 last'),
                ],
            ],
        ]);
        ?>

    </div>
</div>

<?php
$baseUrl = Yii::$app->getRequest()->getBaseUrl() . '/admin/jquery-treetable-3.2.0';
$this->registerCssFile($baseUrl . '/css/jquery.treetable.css');
$this->registerCssFile($baseUrl . '/css/jquery.treetable.theme.default.css');
$js = '$(".table").treetable({expandable: true, initialState: "expand"});';
$this->registerJs($js);
