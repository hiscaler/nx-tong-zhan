<?php
/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlot */

$this->title = '添加创建点';
$this->params['breadcrumbs'][] = ['label' => '四同创建点', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="demonstration-plot-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
