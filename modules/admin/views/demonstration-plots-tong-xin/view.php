<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlot */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '四同创建点', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];
?>
<div class="demonstration-plot-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'region.name',
            'name',
            'address',
            'elements',
            'county_level_awarding_datetime:date',
            'city_level_awarding_datetime:date',
            'province_level_awarding_datetime:date',
            'superintendent_informations',
            'superintendent_mobile_phone',
            'honors_awards:ntext',
            'intro:ntext',
            'inputRegion.name',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>

</div>
