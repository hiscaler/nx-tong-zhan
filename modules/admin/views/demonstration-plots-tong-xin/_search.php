<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlotSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-outside form-search form-layout-column">
    <div class="demonstration-plot-search form">

        <?php
        $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
        ]);
        ?>

        <div class="entry">
            <div class="column">
                <?= $form->field($model, 'region_id')->dropDownList(\app\models\Region::getTree(), ['prompt' => '']) ?>
            </div>

            <div class="column">
                <?= $form->field($model, 'name') ?>
            </div>

        </div>

        <div class="entry">
            <div class="column">
                <?= $form->field($model, 'category')->dropDownList(\app\models\DemonstrationPlotTongXin::categoryOptions(), ['prompt' => '']) ?>
            </div>
        </div>

        <?php // echo $form->field($model, 'elements') ?>

        <?php // echo $form->field($model, 'county_level_awarding_datetime') ?>

        <?php // echo $form->field($model, 'city_level_awarding_datetime') ?>

        <?php // echo $form->field($model, 'province_level_awarding_datetime') ?>

        <?php // echo $form->field($model, 'superintendent_informations') ?>

        <?php // echo $form->field($model, 'superintendent_mobile_phone') ?>

        <?php // echo $form->field($model, 'honors_awards') ?>

        <?php // echo $form->field($model, 'intro') ?>

        <?php // echo $form->field($model, 'input_region_id') ?>

        <?php // echo $form->field($model, 'enabled') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'updated_by')  ?>

        <div class="form-group buttons">
            <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
