<?php

use app\models\Option;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-form-comon">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'username') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'username', $options)->textInput(['maxlength' => true, 'class' => 'username'])->label(false) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'sex') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'sex', $options)->dropDownList(Option::sexOptions())->label(false) ?>
                        </td>
                        <th class="col5">
                            <?= Html::activeLabel($model, 'nation') ?>
                        </th>
                        <td class="col6">
                            <?= $form->field($model, 'nation', $options)->dropDownList(Option::nationOptions())->label(false) ?>
                        </td>
                        <th class="col7">
                            <?= Html::activeLabel($model, 'birthday') ?>
                        </th>
                        <td class="col8">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'birthday',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                        <td class="col9 last" rowspan="3">
                            <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            <?= $form->field($model, 'photo', $options)->fileInput()->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'nationality') ?>
                        </th>
                        <td class="col2"><?= $form->field($model, 'nationality', $options)->dropDownList(Option::nationalityOptions())->label(false) ?></td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'native_place') ?>
                        </th>
                        <td class="col45" colspan="2"><?= $form->field($model, 'native_place', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col6">
                            <?= Html::activeLabel($model, 'birthplace') ?>
                        </th>
                        <td class="col78" colspan="2"><?= $form->field($model, 'birthplace', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'party') ?></th>
                        <td class="col2"><?= $form->field($model, 'party', $options)->dropDownList(Option::partyOptions())->label(false) ?></td>
                        <th class="col3"><?= Html::activeLabel($model, 'join_party_date') ?></th>
                        <td class="col45" colspan="2">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'join_party_date',
                                'pickerType' => 'date',
                                'htmlOptions' => [
                                    'label' => false,
                                ]
                            ]);
                            ?>
                        </td>
                        <th class="col6"><?= Html::activeLabel($model, 'registered_residence_address') ?></th>
                        <td class="col78" colspan="2"><?= $form->field($model, 'registered_residence_address', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'join_job_date') ?></th>
                        <td class="col2">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'join_job_date',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                        <th class="col3"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                        <td class="col45" colspan="2"><?= $form->field($model, 'mobile_phone', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col6"><?= Html::activeLabel($model, 'id_card_number') ?></th>
                        <td class="col789" colspan="3"><?= $form->field($model, 'id_card_number', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($profile, 'leave_date') ?></th>
                        <td class="col2"><?= $form->field($profile, 'leave_date', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col3"><?= Html::activeLabel($profile, 'return_date') ?></th>
                        <td class="col45" colspan="2"><?= $form->field($profile, 'return_date', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col6"><?= Html::activeLabel($profile, 'living_place') ?></th>
                        <td class="col789" colspan="3"><?= $form->field($profile, 'living_place', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($profile, 'tel') ?></th>
                        <td class="col2345" colspan="4"><?= $form->field($profile, 'tel', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col6">
                            <?= Html::activeLabel($profile, 'live_address') ?>
                        </th>
                        <td class="col789" colspan="3"><?= $form->field($profile, 'live_address', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'work_information') ?>
                        </th>
                        <td class="col2345" colspan="4"><?= $form->field($model, 'work_information', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                        <th class="col16">
                            <?= Html::activeLabel($model, 'social_title') ?>
                        </th>
                        <td class="col789 last" colspan="3"><?= $form->field($model, 'social_title', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col3456789 last" colspan="9"><?= $form->field($model, 'category', $options)->dropDownList(\app\models\Person::categoryOptions(), ['prompt' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                        <td class="col23456789 last" colspan="8"><?= $form->field($model, 'political_arrangements')->textarea(['rows' => 6, 'placeholder' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($profile, 'speciality') ?></th>
                        <td class="col23456789 last" colspan="8"><?= $form->field($profile, 'speciality')->textarea(['rows' => 2, 'placeholder' => ''])->label(false) ?></td>
                    </tr>

                    <tr>
                        <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                        <td class="col2345678910 last" colspan="8"><?= $form->field($model, 'intro')->textarea(['rows' => 6, 'placeholder' => '（控制150字以内）'])->label(false) ?></td>
                    </tr>

                    <?=
                    $this->render('/people/_familySocialRelationsForm', [
                        'form' => $form,
                        'model' => $model,
                        'type' => \app\models\Constant::FAMILY_SOCIAL_RELATIONS_PRIMARY,
                        'text' => '华侨、归侨本人主要家庭成员及社会关系'
                    ])
                    ?>

                    <?=
                    $this->render('/people/_familySocialRelationsForm', [
                        'form' => $form,
                        'model' => $model,
                        'type' => \app\models\Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY,
                        'text' => '宁乡县内主要眷属'
                    ])
                    ?>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'remark') ?>
                        </th>
                        <td class="col2345678910 last" colspan="8"><?= $form->field($model, 'remark', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="9">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>

<?php app\modules\admin\components\JsBlock::begin() ?>
<script type="text/javascript">
    $(function () {
        var partyId = '<?= \yii\helpers\Html::getInputId($model, 'party') ?>';
        var joinPartyDateId = '<?= \yii\helpers\Html::getInputId($model, 'join_party_date') ?>';
        $('#' + partyId).change(function () {
            if ($(this).val() == 10) {
                $('.field-' + joinPartyDateId).hide();
            } else {
                $('.field-' + joinPartyDateId).show();
            }
        });
    });
</script>
<?php app\modules\admin\components\JsBlock::end() ?>