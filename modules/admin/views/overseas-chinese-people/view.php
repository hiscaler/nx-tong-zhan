<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '华侨（归侨、侨眷）登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];

$formatter = Yii::$app->getFormatter();
?>
<div class="person-view">

    <div class="table-form table-form-comon">
        <div class="outer">
            <div class="inner">

                <table>
                    <tbody>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'username') ?>
                            </th>
                            <td class="col2">
                                <?= $model['username'] ?>
                            </td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'sex') ?>
                            </th>
                            <td class="col4">
                                <?= $formatter->asSex($model['sex']) ?>
                            </td>
                            <th class="col5">
                                <?= Html::activeLabel($model, 'nation') ?>
                            </th>
                            <td class="col6">
                                <?= $formatter->asNation($model['nation']) ?>
                            </td>
                            <th class="col7" colspan="2">
                                <?= Html::activeLabel($model, 'birthday') ?>
                            </th>
                            <td class="col8">
                                <?= date('Y年n月', $model['birthday']) ?>
                            </td>
                            <td class="col10 last" rowspan="3">
                                <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'nationality') ?>
                            </th>
                            <td class="col2"><?= $formatter->asNationality($model['nationality']) ?></td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'native_place') ?>
                            </th>
                            <td class="col45" colspan="2"><?= $model['native_place'] ?></td>
                            <th class="col67" colspan="2">
                                <?= Html::activeLabel($model, 'birthplace') ?>
                            </th>
                            <td class="col89" colspan="2"><?= $model['birthplace'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'party') ?></th>
                            <td class="col2"><?= $formatter->asParty($model['party']) ?></td>
                            <th class="col3"><?= Html::activeLabel($model, 'join_party_date') ?></th>
                            <td class="col45" colspan="2"><?= $model['join_party_date'] ? date('Y.m.d', $model['join_party_date']) : '' ?></td>
                            <th class="col67" colspan="2"><?= Html::activeLabel($model, 'registered_residence_address') ?></th>
                            <td class="col89" colspan="2"><?= $model['registered_residence_address'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'join_job_date') ?></th>
                            <td class="col2"><?= $model['join_job_date'] ? $formatter->asDate($model['join_job_date']) : null ?></td>
                            <th class="col3"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                            <td class="col45" colspan="2"><?= $model['mobile_phone'] ?></td>
                            <th class="col67" colspan="2"><?= Html::activeLabel($model, 'id_card_number') ?></th>
                            <td class="col8910" colspan="3"><?= $model['id_card_number'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($profile, 'leave_date') ?></th>
                            <td class="col2"><?= $profile['leave_date'] ?></td>
                            <th class="col3"><?= Html::activeLabel($profile, 'return_date') ?></th>
                            <td class="col45" colspan="2"><?= $profile['return_date'] ?></td>
                            <th class="col6"><?= Html::activeLabel($profile, 'living_place') ?></th>
                            <td class="col789" colspan="3"><?= $profile['living_place'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($profile, 'tel') ?></th>
                            <td class="col2345" colspan="4"><?= $profile['tel'] ?></td>
                            <th class="col6">
                                <?= Html::activeLabel($profile, 'live_address') ?>
                            </th>
                            <td class="col789" colspan="3"><?= $profile['live_address'] ?></td>
                        </tr>


                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'work_information') ?></th>
                            <td class="col2" colspan="3"><?= $model['work_information'] ?></td>
                            <th class="col3"><?= Html::activeLabel($model, 'social_title') ?></th>
                            <td class="col45" colspan="3"><?= $model['social_title'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                            <td class="col2345678910 last" colspan="9"><?= $model['political_arrangements'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($profile, 'speciality') ?></th>
                            <td class="col2345678910 last" colspan="9"><?= $profile['speciality'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                            <td class="col2345678910 last" colspan="9"><?= $model['intro'] ?></td>
                        </tr>

                        <?=
                        $this->render('/people/_familySocialRelations', [
                            'model' => $model,
                            'type' => \app\models\Constant::FAMILY_SOCIAL_RELATIONS_PRIMARY,
                            'text' => '华侨、归侨本人主要家庭成员及社会关系',
                        ])
                        ?>

                        <?=
                        $this->render('/people/_familySocialRelations', [
                            'model' => $model,
                            'type' => \app\models\Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY,
                            'text' => '宁乡县内主要眷属',
                        ])
                        ?>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'remark') ?>
                            </th>
                            <td class="col2345678910 last" colspan="9"><?= $model['remark'] ?></td>
                        </tr>
                    </tbody>

                </table>

            </div>

        </div>
    </div>

</div>
