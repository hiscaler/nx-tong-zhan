<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '华侨（归侨、侨眷）登记表';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="person-index">

    <?= $this->render('/people/_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            [
                'attribute' => 'username',
                'contentOptions' => ['class' => 'username'],
            ],
            [
                'attribute' => 'sex',
                'format' => 'sex',
                'contentOptions' => ['class' => 'sex'],
            ],
            [
                'attribute' => 'birthday',
                'value' => function ($model) {
                    return date('Y年n月', $model['birthday']);
                },
                'contentOptions' => ['class' => 'date'],
            ],
            'native_place',
            [
                'attribute' => 'full_time_education_level',
                'format' => 'educationLevel',
                'contentOptions' => ['class' => 'education-level'],
            ],
            [
                'attribute' => 'id_card_number',
                'contentOptions' => ['class' => 'id-card-number'],
            ],
            [
                'attribute' => 'party',
                'format' => 'religion',
            ],
            [
                'attribute' => 'join_party_date',
                'contentOptions' => ['class' => 'date'],
            ],
            'religionPersonProfile.position_certificate_number',
            [
                'attribute' => 'work_information',
            ],
            [
                'attribute' => 'mobile_phone',
                'contentOptions' => ['class' => 'mobile-phone'],
            ],
            [
                'attribute' => 'political_arrangements',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncate($model['political_arrangements'], 20);
                }
            ],
            [
                'attribute' => 'region.name',
                'header' => '录入单位',
                'contentOptions' => ['class' => 'region-name'],
            ],
            'remark',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {to-word} {delete}',
                'buttons' => [
                    'to-word' => function ($url, $model, $key) {
                        return Html::a(Html::img(Yii::$app->getRequest()->getBaseUrl() . '/admin/images/word.png'), $url, ['data-pjax' => 0, 'title' => '导出为 Word 文件']);
                    }
                ],
                'headerOptions' => array('class' => 'buttons-4 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
