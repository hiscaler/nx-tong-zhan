<?php
/* @var $this yii\web\View */
/* @var $model app\models\Slide */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => '后台人员管理',
    ]) . ' ' . $model->username;

$this->params['breadcrumbs'][] = ['label' => '后台人员管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';

$this->params['menus'] = [
    ['label' => '后台人员管理', 'url' => ['index']],
    ['label' => '添加', 'url' => ['create']],
];
?>
<div class="slide-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
