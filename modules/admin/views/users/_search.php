<?php

use app\models\Option;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-outside form-search form-layout-column">
    <div class="user-search form">

        <?php
        $form = ActiveForm::begin([
                'id' => 'form-user-search',
                'action' => ['index'],
                'method' => 'get',
        ]);
        ?>

        <div class="entry">
            <?= $form->field($model, 'username') ?>

            <?php echo $form->field($model, 'status')->dropDownList(\app\models\User::statusOptions(), ['prompt' => '']) ?>
        </div>

        <div class="entry">
            <?php echo $form->field($model, 'role')->dropDownList(\app\models\User::roleOptions(), ['prompt' => '']) ?>
            <?php echo $form->field($model, 'region_id')->dropDownList(\app\models\Region::getTree(), ['prompt' => '']) ?>
        </div>


        <div class="form-group buttons">
            <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
