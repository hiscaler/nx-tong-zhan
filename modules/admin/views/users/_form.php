<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-outside">
    <div class="form user-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'region_id')->dropDownList(app\models\Region::getTree(), ['prompt' => '']) ?>

        <?= $form->field($model, 'role')->dropDownList(User::roleOptions()) ?>

        <?php if ($model->isNewRecord): ?>
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'g-text']) ?>

            <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true, 'class' => 'g-text']) ?>
        <?php endif; ?>

        <?= $form->field($model, 'nickname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(User::statusOptions(), ['prompt' => '']) ?>

        <div class="form-group buttons">
            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
