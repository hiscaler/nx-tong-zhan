<?php
/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title = Yii::t('app', 'Create {modelClass}', [
        'modelClass' => '后台人员',
    ]);
$this->params['breadcrumbs'][] = ['label' => '后台人员管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = '添加';

$this->params['menus'] = [
    ['label' => '后台人员管理', 'url' => ['index']],
];
?>

<div class="user-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
