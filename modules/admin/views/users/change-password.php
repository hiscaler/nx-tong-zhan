<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->params['breadcrumbs'][] = ['label' => '后台人员管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改密码';

$this->params['menus'] = [
    ['label' => '后台人员管理', 'url' => ['index']],
];
?>

<div class="user-create">

    <?=
    $this->render('_changePasswordForm', [
        'user' => $user,
        'model' => $model,
    ]);
    ?>

</div>
