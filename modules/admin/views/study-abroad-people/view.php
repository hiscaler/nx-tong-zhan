<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => '出国和归国留学人员', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];

$formatter = Yii::$app->getFormatter();
?>
<div class="person-view">

    <div class="table-form table-form-comon">
        <div class="outer">
            <div class="inner">

                <table>
                    <tbody>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'username') ?>
                            </th>
                            <td class="col2">
                                <?= $model['username'] ?>
                            </td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'sex') ?>
                            </th>
                            <td class="col4">
                                <?= $formatter->asSex($model['sex']) ?>
                            </td>
                            <th class="col5">
                                <?= Html::activeLabel($model, 'nation') ?>
                            </th>
                            <td class="col6">
                                <?= $formatter->asNation($model['nation']) ?>
                            </td>
                            <th class="col7" colspan="2">
                                <?= Html::activeLabel($model, 'birthday') ?>
                            </th>
                            <td class="col8">
                                <?= date('Y年n月', $model['birthday']) ?>
                            </td>
                            <td class="col10 last" rowspan="3">
                                <?= $model['photo'] ? Html::img($model->photo) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'nationality') ?>
                            </th>
                            <td class="col2"><?= $formatter->asNationality($model['nationality']) ?></td>
                            <th class="col3">
                                <?= Html::activeLabel($model, 'native_place') ?>
                            </th>
                            <td class="col45" colspan="2"><?= $model['native_place'] ?></td>
                            <th class="col67" colspan="2">
                                <?= Html::activeLabel($model, 'birthplace') ?>
                            </th>
                            <td class="col89" colspan="2"><?= $model['birthplace'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'party') ?></th>
                            <td class="col2"><?= $formatter->asParty($model['party']) ?></td>
                            <th class="col3"><?= Html::activeLabel($model, 'join_party_date') ?></th>
                            <td class="col45" colspan="2"><?= $model['join_party_date'] ?></td>
                            <th class="col67" colspan="2"><?= Html::activeLabel($model, 'registered_residence_address') ?></th>
                            <td class="col89" colspan="2"><?= $model['registered_residence_address'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'join_job_date') ?></th>
                            <td class="col2"><?= $model['join_job_date'] ? $formatter->asDate($model['join_job_date']) : null ?></td>
                            <th class="col3"><?= Html::activeLabel($model, 'mobile_phone') ?></th>
                            <td class="col45" colspan="2"><?= $model['mobile_phone'] ?></td>
                            <th class="col67" colspan="2"><?= Html::activeLabel($model, 'id_card_number') ?></th>
                            <td class="col8910" colspan="3"><?= $model['id_card_number'] ?></td>
                        </tr>

                        <tr>
                            <th rowspan="2" class="col1"><label for="">学历学位</label></th>
                            <th class="col2"><?= Html::activeLabel($model, 'full_time_education_level') ?></th>
                            <td class="col3"><?= $formatter->asEducationLevel($model['full_time_education_level']) ?></td>
                            <th class="col4567" colspan="4"><?= Html::activeLabel($model, 'in_service_education_school') ?></th>
                            <td class="col8910 last" colspan="3"><?= $model['in_service_education_school'] ?></td>
                        </tr>

                        <tr>
                            <th class="col2"><?= Html::activeLabel($model, 'in_service_education_level') ?></th>
                            <td class="col3"><?= $formatter->asEducationLevel($model['in_service_education_level']) ?></td>
                            <th class="col4567" colspan="4"><?= Html::activeLabel($model, 'full_time_education_school') ?></th>
                            <td class="col8910 last" colspan="3"><?= $model['full_time_education_school'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($profile, 'country') ?>
                            </th>
                            <td class="col2"><?= $profile['country'] ?></td>
                            <th class="col3">
                                <?= Html::activeLabel($profile, 'type') ?>
                            </th>
                            <td class="col45" colspan="2"><?= $formatter->asStudyAbroadPersonType($profile['type']) ?></td>
                            <th class="col67" colspan="2">
                                <?= Html::activeLabel($profile, 'school') ?>
                            </th>
                            <td class="col8910" colspan="3"><?= $profile['school'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'professional_title') ?>
                            </th>
                            <td class="col2"><?= $model['professional_title'] ?></td>
                            <th class="col345" colspan="3">
                                <?= Html::activeLabel($model, 'work_information') ?>
                            </th>
                            <td class="col678910" colspan="5"><?= $model['work_information'] ?></td>
                        </tr>

                        <tr>
                            <th class="col12" colspan="2">
                                <?= Html::activeLabel($model, 'social_title') ?>
                            </th>
                            <td class="col345678910 last" colspan="8"><?= $model['social_title'] ?></td>
                        </tr>

                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'intro') ?></th>
                            <td class="col2345678910 last" colspan="9"><?= $model['intro'] ?></td>
                        </tr>
                        <tr>
                            <th class="col1"><?= Html::activeLabel($model, 'political_arrangements') ?></th>
                            <td class="col2345678910 last" colspan="9"><?= $model['political_arrangements'] ?></td>
                        </tr>

                        <?=
                        $this->render('/people/_familySocialRelations', [
                            'model' => $model,
                        ])
                        ?>

                        <tr>
                            <th class="col1">
                                <?= Html::activeLabel($model, 'remark') ?>
                            </th>
                            <td class="col2345678910 last" colspan="9"><?= $model['remark'] ?></td>
                        </tr>
                    </tbody>

                </table>

            </div>

        </div>
    </div>

</div>
