<?php
/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = '添加人员';
$this->params['breadcrumbs'][] = ['label' => '出国和归国留学人员', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="person-create">

    <?=
    $this->render('_form', [
        'model' => $model,
        'profile' => $profile
    ])
    ?>

</div>
