<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '出国和归国留学人员';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="person-index">


    <?= $this->render('/people/_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            [
                'attribute' => 'username',
                'contentOptions' => ['class' => 'username'],
            ],
            [
                'attribute' => 'sex',
                'format' => 'sex',
                'contentOptions' => ['class' => 'sex'],
            ],
            [
                'attribute' => 'nation',
                'format' => 'nation',
                'contentOptions' => ['class' => 'nation'],
            ],
            [
                'attribute' => 'birthday',
                'value' => function ($model) {
                    return date('Y年n月', $model['birthday']);
                },
                'contentOptions' => ['class' => 'date'],
            ],
            [
                'attribute' => 'party',
                'format' => 'party',
                'contentOptions' => ['class' => 'party'],
            ],
            [
                'attribute' => 'full_time_education_level',
                'header' => '学历学位',
                'format' => 'educationLevel',
                'contentOptions' => ['class' => 'education-level'],
            ],
            [
                'attribute' => 'studyAbroadPersonProfile.country',
            ],
            'work_information',
            [
                'attribute' => 'political_arrangements',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncate($model['political_arrangements'], 20);
                }
            ],
            [
                'attribute' => 'mobile_phone',
                'contentOptions' => ['class' => 'mobile-phone'],
            ],
            [
                'attribute' => 'region.name',
                'header' => '录入单位',
                'contentOptions' => ['class' => 'region-name'],
            ],
            'remark',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {to-word} {delete}',
                'buttons' => [
                    'to-word' => function ($url, $model, $key) {
                        return Html::a(Html::img(Yii::$app->getRequest()->getBaseUrl() . '/admin/images/word.png'), $url, ['data-pjax' => 0, 'title' => '导出为 Word 文件']);
                    }
                ],
                'headerOptions' => array('class' => 'buttons-4 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
