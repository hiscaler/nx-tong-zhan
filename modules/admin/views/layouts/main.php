<?php

use yii\widgets\Breadcrumbs;

$this->beginContent('@app/modules/admin/views/layouts/base.php');
?>

<?php
if (isset($this->params['breadcrumbs']) && $this->params['breadcrumbs']) {
    echo '<div class="breadcrumbs clearfix">';
    echo Breadcrumbs::widget(
        [
            'itemTemplate' => "<li>{link}<i>&raquo;</i></li>",
            'homeLink' => [
                'label' => '首页',
                'url' => ['/admin/default/index'],
            ],
            'links' => $this->params['breadcrumbs'],
    ]);
    echo '</div>';
}
if (isset($this->params['menus']) && $this->params['menus']) {
    echo \app\modules\admin\widgets\MenuButtons::widget([
        'items' => $this->params['menus'],
    ]);
}
?>
<div class="container">
    <div class="inner">
        <?= $content ?>
    </div>
</div>

<?php $this->endContent(); ?>