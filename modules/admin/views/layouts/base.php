<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\admin\widgets\MainMenu;
use yii\helpers\Html;

app\modules\admin\assets\AppAsset::register($this);

$baseUrl = Yii::$app->getRequest()->getBaseUrl() . '/admin';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?> - <?= Yii::$app->name ?></title>
        <?php $this->head() ?>
    </head>
    <body id="mts-app">
        <?php $this->beginBody() ?>

        <div id="page-hd">
            <div id="page">
                <!-- Header -->
                <div id="header">
                    <div id="logo"><?php echo Html::a(Html::img($baseUrl . '/images/logo.png'), ['default/index']); ?></div>
                    <div id="main-menu">
                        <?= MainMenu::widget() ?>
                    </div>
                    <div id="header-account-manage">
                        <?= app\modules\admin\widgets\Toolbar::widget(); ?>
                    </div>
                </div>
                <!-- // Header -->
            </div>
        </div>
        <div id="page-bd">
            <div class="container">
                <?= $content ?>
            </div>
        </div>
        <div id="page-ft">
            <div id="footer">
                版权所有 &copy; 2011-<?= date('Y') ?> 中共宁乡县委统战部电话 0731-88980610　湖南回车键信息技术有限公司提供技术支持 QQ 512315888 电话 0731-88980238
            </div>
        </div>

        <?php $this->endBody() ?>
        <script type="text/javascript">
            yadjet.icons.boolean = ['<?= $baseUrl ?>/images/no.png', '<?= $baseUrl ?>/images/yes.png'];
        </script>
    </body>
</html>
<?php $this->endPage() ?>