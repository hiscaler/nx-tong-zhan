<?php

use app\models\Constant;
?>
<tr>
    <th class="col1">
        <?= isset($text) ? $text : '家庭主要成员及社会关系' ?>
    </th>
    <td class="col2345678910" colspan="9">
        <div id="grid-person-family-social-relations" class="grid-view">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="title">称谓</th>
                        <th class="username">姓名</th>
                        <th class="birthday">出生年月</th>
                        <th class="party">党派</th>
                        <th class="last">工作单位及职务</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $formatter = Yii::$app->getFormatter();
                    if (isset($type) && $type == Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY) {
                        $relations = $model->familySocialRelationsSecondary;
                    } else {
                        $relations = $model->familySocialRelations;
                    }
                    foreach ($relations as $key => $relation):
                        ?>
                        <tr id="row-<?= $key ?>">
                            <td class="title">
                                <?= $relation['title'] ?>
                            </td>
                            <td class="username"><?= $relation['username'] ?></td>
                            <td class="birthday"><?= $relation['birthday'] ? date('Y.m.d', $relation['birthday']) : '' ?></td>
                            <td class="party"><?= $formatter->asParty($relation['party']) ?></td>
                            <td><?= $relation['work_information'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </td>
</tr>