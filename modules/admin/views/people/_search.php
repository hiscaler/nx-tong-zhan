<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-outside form-search form-layout-column">
    <div class="person-search form">

        <?php
        $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
        ]);
        ?>

        <div class="entry">
            <?php if (Yii::$app->getUser()->getIdentity()->getRole() == \app\models\User::ROLE_ADMINISTRATOR): ?>
                <?= $form->field($model, 'region_id')->dropDownList(\app\models\Region::getTree(), ['prompt' => '']) ?>
            <?php endif ?>

            <?= $form->field($model, 'username') ?>
        </div>

        <div class="entry">
            <?= $form->field($model, 'id_card_number') ?>
            <?= $form->field($model, 'nation')->dropDownList(app\models\Option::nationOptions(), ['prompt' => '']) ?>
        </div>

        <div class="form-group buttons">
            <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
