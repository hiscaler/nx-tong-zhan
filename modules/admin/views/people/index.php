<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '人员管理';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="person-index">


    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            [
                'attribute' => 'region.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            [
                'attribute' => 'username',
                'contentOptions' => ['class' => 'username'],
            ],
            [
                'attribute' => 'sex',
                'format' => 'sex',
                'contentOptions' => ['class' => 'sex'],
            ],
            [
                'attribute' => 'birthday',
                'value' => function ($model) {
                    return date('Y年n月', $model['birthday']);
                },
                'contentOptions' => ['class' => 'date'],
            ],
            [
                'attribute' => 'join_job_date',
                'format' => 'date',
                'contentOptions' => ['class' => 'date'],
            ],
            [
                'attribute' => 'full_time_education_level',
                'format' => 'educationLevel',
                'contentOptions' => ['class' => 'education-level'],
            ],
            'full_time_education_school',
            [
                'attribute' => 'mobile_phone',
                'contentOptions' => ['class' => 'mobile-phone'],
            ],
            [
                'attribute' => 'region.name',
                'header' => '录入单位',
                'contentOptions' => ['class' => 'region-name'],
            ],
//            'full_time_education_level_text',
//            'full_time_education_school',
//            'in_service_education_level_text',
//            'in_service_education_school',
//            'social_title',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'headerOptions' => array('class' => 'button-1 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
