<?php

use app\models\Constant;
use yii\helpers\Html;

if (isset($type) && $type == Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY) {
    $type = Constant::FAMILY_SOCIAL_RELATIONS_SECONDARY;
    $relations = $model->familySocialRelationsSecondary;
} else {
    $type = Constant::FAMILY_SOCIAL_RELATIONS_PRIMARY;
    $relations = $model->familySocialRelations;
}
?>

<tr>
    <th>
        <p><a class="btn-add-new-person-family-social-relation-row btn-circle" href="javascript:;" data-type="<?= $type ?>">+</a></p>
        <?= isset($text) ? $text : '家庭主要成员及社会关系' ?>
        <?= Html::hiddenInput('index-counter', count($relations), ['id' => 'index-counter']); ?>
    </th>
    <td colspan="<?= isset($spanCols) ? $spanCols : 9 ?>">
        <div class="grid-view">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="title">称谓</th>
                        <th class="username">姓名</th>
                        <th class="birthday">出生年月</th>
                        <th class="party">党派</th>
                        <th>工作单位及职务</th>
                        <th class="buttons button-1 last"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $options = [
                        'template' => '{input}',
                        'options' => ['class' => null]
                    ];
                    if ($model->isNewRecord || empty($relations)):
                        ?>
                        <tr id="row-0-<?= $type ?>">
                            <td>
                                <?= $form->field($model, "relations[0][id]", $options)->hiddenInput()->label(false) ?>
                                <?= $form->field($model, "relations[0][type]", $options)->hiddenInput(['value' => $type])->label(false) ?>
                                <?= $form->field($model, 'relations[0][title]', $options)->textInput(['maxlength' => true])->label(false) ?>
                            </td>
                            <td><?= $form->field($model, 'relations[0][username]', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                            <td class="birthday">
                                <?=
                                \yadjet\datePicker\my97\DatePicker::widget([
                                    'form' => $form,
                                    'model' => $model,
                                    'name' => basename($model::className()) . "[relations][0][birthday]",
                                    'pickerType' => 'date',
                                ]);
                                ?>
                            </td>
                            <td><?= $form->field($model, 'relations[0][party]', $options)->dropDownList(app\models\Option::partyOptions(true), ['prompt' => ''])->label(false) ?></td>
                            <td><?= $form->field($model, 'relations[0][work_information]', $options)->textInput(['maxlength' => true])->label(false) ?></td>
                            <td class="btns last"></td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($relations as $key => $relation): ?>
                            <tr id="row-<?= $key ?>-<?= $type ?>">
                                <td class="title">
                                    <?= $form->field($model, "relations[$key][id]", $options)->hiddenInput(['value' => $relation['id']])->label(false) ?>
                                    <?= $form->field($model, "relations[$key][type]", $options)->hiddenInput(['value' => $relation['type']])->label(false) ?>
                                    <?= $form->field($model, "relations[$key][title]", $options)->textInput(['maxlength' => true, 'value' => $relation['title']])->label(false) ?>
                                </td>
                                <td class="username"><?= $form->field($model, "relations[$key][username]", $options)->textInput(['maxlength' => true, 'value' => $relation['username']])->label(false) ?></td>
                                <td class="birthday">
                                    <?=
                                    \yadjet\datePicker\my97\DatePicker::widget([
                                        'form' => $form,
                                        'model' => $model,
                                        'name' => basename($model::className()) . "[relations][$key][birthday]",
                                        'value' => $relation['birthday'] ? date('Y-m-d', $relation['birthday']) : null,
                                        'pickerType' => 'date',
                                    ]);
                                    ?>
                                </td>
                                <td class="party"><?= $form->field($model, "relations[$key][party]", $options)->dropDownList(app\models\Option::partyOptions(true), ['prompt' => '', 'value' => $relation['party']])->label(false) ?></td>
                                <td><?= $form->field($model, "relations[$key][work_information]", $options)->textInput(['maxlength' => true, 'value' => $relation['work_information']])->label(false) ?></td>
                                <td class="btns last">
                                    <a href="javascript:;" onclick="removeCloneRow(this); return false;" title="删除" aria-label="删除" class="btn-remove-dynamic-row"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

    </td>
</tr>