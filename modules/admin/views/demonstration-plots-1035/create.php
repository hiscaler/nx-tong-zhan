<?php
/* @var $this yii\web\View */
/* @var $model app\models\DemonstrationPlot */

$this->title = '添加示范点';
$this->params['breadcrumbs'][] = ['label' => '凝心聚力十三五行动示范点', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
];
?>
<div class="demonstration-plot-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
