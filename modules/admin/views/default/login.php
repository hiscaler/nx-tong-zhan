<?php

use yii\widgets\ActiveForm;

$baseUrl = Yii::$app->getRequest()->getBaseUrl() . '/admin';
?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="不超过150个字符"/>
        <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
        <link href="<?= $baseUrl ?>/css/login.css" rel="stylesheet" type="text/css">
        <title>登录</title>
    </head>
    <body>
        <section>
            <div class="logo"><img src="<?= $baseUrl ?>/images/login-logo.png" /></div>
            <div class="log-form">
                <div class="logo-z"><img src="<?= $baseUrl ?>/images/logo-z.png" ></div>
                <?php
                $fieldConfigs = [
                    'options' => ['class' => 'lf-row', 'tag' => 'div'],
                    'template' => '<ul><li class="f-tit">{label}：</li><li class="f-ipt">{input}{error}</li></a>',
                ];
                $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableAjaxValidation' => false,
                ]);
                ?>
                <?= $form->field($model, 'username', $fieldConfigs)->textInput(['tabindex' => 1]); ?>
                <?= $form->field($model, 'password', $fieldConfigs)->passwordInput(['tabindex' => 2]); ?>

                <?php
                $fieldConfigs = [
                    'options' => ['class' => 'lf-row', 'tag' => 'div',],
                    'template' => '<ul><li class="f-tit">{label}：</li><li class="f-ipt f-yzm">{input}{error}</li></a>',
                ];
                echo $form->field($model, 'verifyCode', $fieldConfigs)->widget(\yii\captcha\Captcha::className(), [
                    'template' => '{input}<span>{image}</span>',
                    'captchaAction' => 'default/captcha',
                ]);
                ?>

                <div class="lf-ctl">
                    <?=
                    \yii\helpers\Html::activeCheckbox($model, 'rememberMe', ['labelOptions' => [
                            'class' => 'check'
                        ], 'label' => '<i></i><span>记住密码</span>'])
                    ?>

                    <input type="submit" name="bt_login" id="bt_login" value="登录" class="button" />
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </section>
        <footer>
            版权所有 &copy; 2011-<?= date('Y') ?> 中共宁乡县委统战部电话 0731-88980610　湖南回车键信息技术有限公司提供技术支持 QQ 512315888 电话 0731-88980238
        </footer>
    </body>
</html>