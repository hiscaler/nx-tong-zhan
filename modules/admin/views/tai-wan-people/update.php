<?php
/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = '更新人员: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => '台胞（台属）登记表', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';

$this->params['menus'] = [
    ['label' => '人员列表', 'url' => ['index']],
    ['label' => '添加人员', 'url' => ['create']],
    ['label' => '详情', 'url' => ['view', 'id' => $model->id]],
];
?>
<div class="person-update">

    <?=
    $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
    ])
    ?>

</div>
