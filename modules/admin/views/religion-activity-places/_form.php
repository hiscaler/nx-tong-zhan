<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReligionActivityPlace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form table-position table-position-religion-activity-place">
    <div class="outer">
        <div class="inner">

            <?php
            $form = ActiveForm::begin();
            $options = [
                'template' => '{input}',
            ];
            echo $form->errorSummary($model);
            ?>

            <table>
                <tbody>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'name') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'name', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'region_id') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'region_id', $options)->dropDownList(\app\models\Region::getTree(), ['prompt' => '请选择区域']) ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'category') ?>
                        </th>
                        <td class="col234 categories" colspan="3">
                            <?= $form->field($model, 'category', $options)->checkboxList(app\models\ReligionActivityPlace::categoryOptions()) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'address') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'address', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'staff_people_count') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'staff_people_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'other_people_count') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'other_people_count', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'superintendent_informations') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'superintendent_informations', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'superintendent_mobile_phone') ?>
                        </th>
                        <td class="col4">
                            <?= $form->field($model, 'superintendent_mobile_phone', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'certificate_number') ?>
                        </th>
                        <td class="col2">
                            <?= $form->field($model, 'certificate_number', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                        <th class="col3">
                            <?= Html::activeLabel($model, 'certificate_issue_datetime') ?>
                        </th>
                        <td class="col4">
                            <?=
                            \yadjet\datePicker\my97\DatePicker::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'certificate_issue_datetime',
                                'pickerType' => 'date',
                            ]);
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'historical_relic_protected_level') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'historical_relic_protected_level', $options)->textInput(['maxlength' => true]) ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="col1">
                            <?= Html::activeLabel($model, 'intro') ?>
                        </th>
                        <td class="col234" colspan="3">
                            <?= $form->field($model, 'intro', $options)->textarea(['row' => 6]) ?>
                        </td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </td>
                    </tr>
                </tfoot>

            </table>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>