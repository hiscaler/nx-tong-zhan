<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReligionActivityPlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '宗教活动场所';
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '搜索', 'url' => '#'],
    ['label' => '添加', 'url' => ['create']],
    ['label' => '导出数据', 'url' => ['to-excel']],
    ['label' => '阵地列表', 'url' => ['index']],
];
?>
<div class="religion-activity-place-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'serial-number'],
            ],
            [
                'attribute' => 'region.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            'name',
            'address',
            'staff_people_count',
            'other_people_count',
            'superintendent_informations',
            'superintendent_mobile_phone',
            'certificate_number',
            'certificate_issue_datetime:datetime',
            'historical_relic_protected_level',
            [
                'attribute' => 'inputRegion.name',
                'contentOptions' => ['class' => 'region-name'],
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'contentOptions' => ['class' => 'datetime'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'headerOptions' => array('class' => 'buttons-3 last'),
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
