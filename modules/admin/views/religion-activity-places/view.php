<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReligionActivityPlace */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '宗教活动场所', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
    ['label' => '更新', 'url' => ['update', 'id' => $model->id]],
];
?>
<div class="religion-activity-place-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'region.name',
            'name',
            'address',
            'staff_people_count',
            'other_people_count',
            'superintendent_informations',
            'superintendent_mobile_phone',
            'certificate_number',
            'certificate_issue_datetime:datetime',
            'historical_relic_protected_level',
            'intro:ntext',
            'inputRegion.name',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>

</div>
