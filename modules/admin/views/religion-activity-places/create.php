<?php
/* @var $this yii\web\View */
/* @var $model app\models\ReligionActivityPlace */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => '宗教活动场所', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['menus'] = [
    ['label' => '列表', 'url' => ['index']],
];
?>
<div class="religion-activity-place-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
