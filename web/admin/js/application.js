Date.prototype.fromUnixTimestamp = function (value) {
    return new Date(parseFloat(value) * 1000);
};
Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, //month 
        "d+": this.getDate(), //day 
        "h+": this.getHours(), //hour 
        "m+": this.getMinutes(), //minute 
        "s+": this.getSeconds(), //second 
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter 
        "S": this.getMilliseconds() //millisecond 
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
};
Number.prototype.toFixed = function (d) {
    var s = this + "";
    if (!d) {
        d = 0;
    }
    if (s.indexOf(".") == -1) {
        s += ".";
    }
    s += new Array(d + 1).join("0");
    if (new RegExp("^(-|\\+)?(\\d+(\\.\\d{0," + (d + 1) + "})?)\\d*$").test(s)) {
        var s = "0" + RegExp.$2, pm = RegExp.$1, a = RegExp.$3.length, b = true;
        if (a == d + 2) {
            a = s.match(/\d/g);
            if (parseInt(a[a.length - 1]) > 4) {
                for (var i = a.length - 2; i >= 0; i--) {
                    a[i] = parseInt(a[i]) + 1;
                    if (a[i] == 10) {
                        a[i] = 0;
                        b = i != 1;
                    } else {
                        break;
                    }
                }
            }
            s = a.join("").replace(new RegExp("(\\d+)(\\d{" + d + "})\\d$"), "$1.$2");
        }
        if (b) {
            s = s.substr(1);
        }
        return (pm + s).replace(/\.$/, "");
    }
    return this + "";
};
$.browser = $.browser || {};
$.browser.mozilla = /firefox/.test(navigator.userAgent.toLowerCase());
$.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
$.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
$.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
/**
 * Lock UI
 */
(function ($) {
    $.fn.lock = function () {
        return this.unlock().each(function () {
            if ($.css(this, 'position') === 'static') {
                this.style.position = 'relative';
            }
            if ($.browser.msie) {
                this.style.zoom = 1;
            }
            $(this).append('<div id="widget-lock-ui" class="lock-ui" style="position:absolute;width:100%;height:100%;top:0;left:0;z-index:1000;background-color:#000;cursor:wait;opacity:.7;filter: alpha(opacity=70);"><div>');
        });
    };
    $.fn.unlock = function () {
        return this.each(function () {
            $('#widget-lock-ui', this).remove();
        });
    };
})(jQuery);

$(function () {
    $('#header-account-manage li.change-tenant a:first').on('click', function () {
        $(this).parent().find('ul').show();
    });
    $(document).on('pjax:error', function (xhr, textStatus, error, options) {
        console.log(xhr);
        console.log(textStatus);
        console.log(error);
        console.log(options);
        layer.alert(textStatus.responseText);
    });
    $('.ajax').on('click', function () {
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: $this.attr('href'),
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $this.remove();
                } else {
                    layer.alert(response.error.message);
                }
            }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                layer.alert('[ ' + XMLHttpRequest.status + ' ] ' + XMLHttpRequest.responseText);
            }
        });
        
        return false;
    });
});

function clone(myObj) {
    if (typeof (myObj) != 'object' || myObj == null)
        return myObj;
    var newObj = new Object();
    for (var i in myObj) {
        newObj[i] = clone(myObj[i]);
    }

    return newObj;
}

var yadjet = yadjet || {};
yadjet.icons = yadjet.icon || {};
yadjet.icons.boolean = [
    '/images/no.png',
    '/images/yes.png'
];
yadjet.utils = yadjet.utils || {
    getCsrfParam: function () {
        return $('meta[name=csrf-param]').attr('content');
    },
    getCsrfToken: function () {
        return $('meta[name=csrf-token]').attr('content');
    }
};
yadjet.actions = yadjet.actions || {
    toggle: function (selector, url) {
        var dataExt = arguments[2] ? arguments[2] : {};
        var trData = arguments[3] ? arguments[3] : [];
        $(selector).on('click', function (event) {
            event.stopPropagation();
            var $this = $(this);
            var $tr = $this.parent().parent();
            var data = {
                id: $tr.attr('data-key'),
                _csrf: yadjet.utils.getCsrfToken()
            };
            for (var key in dataExt) {
                data[key] = dataExt[key];
            }
            console.info(trData);
            for (var key in trData) {
                // `data-key` To `dataKey`
                var t = trData[key].toLowerCase();
                t = t.replace(/\b\w+\b/g, function (word) {
                    return word.substring(0, 1).toUpperCase() + word.substring(1);
                });
                t = t.replace('-', '');
                t = t.substring(0, 1).toLowerCase() + t.substring(1);
                data[t] = $tr.attr('data-' + trData[key]);
            }
            console.info(data);
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                beforeSend: function (xhr) {
                    $this.hide().parent().addClass('running-c-c');
                }, success: function (response) {
                    if (response.success) {
                        var data = response.data;
                        $this.attr('src', yadjet.icons.boolean[data.value ? 1 : 0]);
                        if (data.updatedAt) {
                            $tr.find('td.rb-updated-at').html(data.updatedAt);
                        }
                        if (data.updatedBy) {
                            $tr.find('td.rb-updated-by').html(data.updatedBy);
                        }
                        if (data.onOffDatetime) {
                            $tr.find('td.rb-on-off-datetime').html(data.onOffDatetime);
                        }
                    } else {
                        layer.alert(response.error.message);
                    }
                    $this.show().parent().removeClass('running-c-c');
                }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert('[ ' + XMLHttpRequest.status + ' ] ' + XMLHttpRequest.responseText);
                    $this.show().parent().removeClass('running-c-c');
                }
            });

            return false;
        });
    },
    gridColumnConfig: function () {
        jQuery(document).on('click', '#menu-buttons li a.grid-column-config', function () {
            var $this = $(this);
            $.ajax({
                type: 'GET',
                url: $this.attr('href'),
                beforeSend: function (xhr) {
                    $.fn.lock();
                }, success: function (response) {
                    layer.open({
                        title: '表格栏位设定',
                        content: response,
                        lock: true,
                        padding: '10px',
                        yes: function () {
                            $.pjax.reload({container: '#' + $this.attr('data-reload-object')});
                        }
                    });
                    $.fn.unlock();
                }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert('[ ' + XMLHttpRequest.status + ' ] ' + XMLHttpRequest.responseText);
                    $.fn.unlock();
                }
            });

            return false;
        });
    }
};

yadjet.actions.gridColumnConfig();

$(function () {
    $('.tabs-common li a').on('click', function () {
        var $t = $(this),
            $widget = $t.parent().parent().parent().parent();
        $t.parent().siblings().removeClass('active');
        $t.parent().addClass('active');
        $widget.find('.tab-pane').hide();
        $widget.find('#' + $t.attr('data-toggle')).show();
        return false;
    });
});
$(document).on('click', '.search-button a', function () {
    var $t = $(this);
    if ($t.attr('data-toggle') === 'show') {
        $t.attr('data-toggle', 'hide');
        $('.form-search').hide();
    } else {
        $t.attr('data-toggle', 'show');
        $('.form-search').show();
    }

    return false;
});

function IEVersion() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if (isIE) {
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if (fIEVersion == 7) {
            return 7;
        } else if (fIEVersion == 8) {
            return 8;
        } else if (fIEVersion == 9) {
            return 9;
        } else if (fIEVersion == 10) {
            return 10;
        } else {
            return 6;//IE版本<=7
        }
    } else if (isEdge) {
        return 'edge';//edge
    } else if (isIE11) {
        return 11; //IE11  
    } else {
        return -1;//不是ie浏览器
    }
}
function setElementName(elems, name) {
    var ieVersion = IEVersion();
    if ($.browser.msie === true && ieVersion < 9 && ieVersion !== 'edge') {
        $(elems).each(function () {
            this.mergeAttributes(document.createElement("<input name='" + name + "'/>"), false);
        });
    } else {
        $(elems).attr('name', name);
    }
}
$('.btn-add-new-person-family-social-relation-row').on('click', function() {
    var $t = $(this),
        typeValue = $t.attr('data-type'),
        $cloneRow = $('#row-0-' + typeValue).clone(false),
        indexCounter = parseInt($('#index-counter').val()),
        id, name, elements, element, attrs;
        if (indexCounter) {
            indexCounter += 1;
        }
        
    elements = $cloneRow.find('input,select,textarea');
    for (var i = 0, l = elements.length; i < l; i++) {
        element = $(elements[i]);
        if (element.attr('id') === 'person-relations-0-title') {
            element.val(0).attr({
                id: 'person-relations-' + indexCounter + '-title',
                name: 'Person[relations][' + indexCounter + '][title]',
                value: ''
            });
        } else {
            var elementType = $(elements[i]).attr('type'),
                isTextarea = false;
            attrs = {};
            if (elementType === 'text') {
                attrs.value = '';
            } else if (elements[i].tagName.toLowerCase() == 'select') {
                // @todo
                // $(elements[i])[0].selectedIndex = 5;
            } else if (elementType === 'checkbox') {
                attrs.checked = 'checked';
            } else if (elementType === 'hidden') {
                // nothing
            } else {
                isTextarea = true;
            }
            console.info($(elements[i]).attr('name'));
            console.info($(elements[i]).attr('name').indexOf('type'));
            if (elementType === 'hidden' && $(elements[i]).attr('name').indexOf('type') === -1) {
                attrs.value = '';
            }            
            id = element.attr('id');
            if (typeof id !== typeof undefined && id !== false) {
                attrs.id = id.replace('0', indexCounter);
            }
            setElementName(element, element.attr('name').replace('0', indexCounter));
            element.attr(attrs);
            if (isTextarea) {
                if ($.browser.msie) {
                    element.val("1、\r2、\r3、\r4、\r5、\r6、");
                } else {
                    element.text("1、\r2、\r3、\r4、\r5、\r6、");
                }
            }
        }
    }
    $cloneRow.find('.btns').html('').append('<a href="javascript:;" onclick="removeCloneRow(this);" title="删除" aria-label="删除" class="btn-remove-dynamic-row"><span class="glyphicon glyphicon-trash"></span></a>');
//    $('#grid-person-family-social-relations table tbody').append('<tr id="row-' + indexCounter + '">' + $cloneRow.html() + '</tr>');
    $t.parent().parent().parent().find('.grid-view table tbody').append('<tr id="row-' + indexCounter + '">' + $cloneRow.html() + '</tr>');
    $('#index-counter').val(indexCounter);
});

function removeCloneRow(t) {
    if (confirm('您确定删除该记录？')) {
        $(t).parent().parent().remove();
        $('#index-counter').val(parseInt($('#index-counter').val()) - 1);

        // 重新设置数组下标
        var $jobs = $('.job');
        $.each($jobs, function (index) {
            var elements = $(this).find('input,select,textarea');
            for (var i = 0, l = elements.length; i < l; i++) {
                element = $(elements[i]);
                id = element.attr('id');
                var num = id.match(/\d+?/g);
                if (num !== null) {
                    element.attr('id', element.attr('id').replace(num, index));
                    element.attr('name', element.attr('name').replace(num, index));
                }
            }
        });
    }

    return false;
}