<?php

namespace app\extensions;

use Yii;

/**
 * 短信发送
 */
class SMSHelper
{

    /**
     *
     * @param url $url
     * @return integer
     */
    public static function send($mobilePhones, $text)
    {
        if (is_string($mobilePhones)) {
            $tels = trim($mobilePhones);
        } else {
            $tels = array();
            foreach ($mobilePhones as $mobilePhone) {
                $mobilePhone = strtr(trim($mobilePhone), array(' ' => '', '　' => ''));
                if (!empty($mobilePhone)) {
                    $tels[] = $mobilePhone;
                }
            }
            $tels = implode(',', array_unique($tels));
        }
        $smsConfigs = isset(Yii::$app->params['sms']) ? Yii::$app->params['sms'] : array();
        $params = array(
            'Uid' => isset($smsConfigs['uid']) ? $smsConfigs['uid'] : '',
            'Key' => isset($smsConfigs['key']) ? $smsConfigs['key'] : '',
            'smsMob' => $tels,
            'smsText' => strtr(strip_tags($text), array(' ' => '', '　' => '')),
        );
        $url = (isset($smsConfigs['url']) ? $smsConfigs['url'] : '') . http_build_query($params);
        $curl = curl_init();
        $timeout = 5;
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $content = curl_exec($curl);
        $status = curl_getinfo($curl);
        curl_close($curl);

        // Parse response result
        if ($content === false) {
            $response = array(
                'success' => false,
                'error' => array(
                    'code' => 0,
                    'message' => '连接短信发送服务器出错。',
                ),
            );
        } else if (is_array($content)) {
            $response = array(
                'success' => false,
                'error' => array(
                    'code' => $status['http_code'],
                    'message' => $status,
                ),
            );
        } else {
            $errors = self::errors();
            if (isset($errors[$content])) {
                $response = array(
                    'success' => false,
                    'error' => array(
                        'code' => $content,
                        'message' => $errors[$content],
                    ),
                );
            } else {
                $response = array(
                    'success' => true,
                    'count' => (int) $content,
                    'message' => "成功发送 {$content} 条短信。",
                );
            }
        }

        return $response;
    }

    private static function errors()
    {
        return array(
            '-1' => '没有该用户账户',
            '-2' => '接口密钥不正确',
            '-21' => 'MD5接口密钥加密不正确',
            '-3' => '短信数量不足',
            '-11' => '该用户被禁用',
            '-14' => '短信内容出现非法字符',
            '-4' => '手机号格式不正确',
            '-41' => '手机号码为空',
            '-42' => '短信内容为空',
            '-51' => '短信签名格式不正确',
            '-6' => 'IP限制',
        );
    }

}
