<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$baseUrl = Yii::$app->getRequest()->getBaseUrl();
$actionId = $this->context->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body ontouchstart>
        <?php $this->beginBody() ?>
        <div class="container" id="container">
            <div class="page__hd">
                宁乡统战信息管理系统
            </div>
            <div class="page__bd">
                <div class="weui-tab">
                    <div class="weui-navbar">
                        <div class="weui-navbar__item<?= $actionId == 'index' ? ' weui-bar__item_on' : '' ?>">
                            <a href="<?= \yii\helpers\Url::toRoute(['statistics/index']) ?>">图表</a>
                        </div>
                        <div class="weui-navbar__item<?= $actionId == 'region' ? ' weui-bar__item_on' : '' ?>">
                            <a href="<?= \yii\helpers\Url::toRoute(['statistics/region']) ?>">地区</a>
                        </div>
                        <div class="weui-navbar__item<?= $actionId == 'person-type' ? ' weui-bar__item_on' : '' ?>">
                            <a href="<?= \yii\helpers\Url::toRoute(['statistics/person-type']) ?>">人员类型</a>
                        </div>
                    </div>
                    <div class="weui-tab__panel">
                        <?= $content ?>
                    </div>
                </div>

            </div>
            <div class="page__ft">
                <?= app\widgets\Tabbar::widget() ?>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
