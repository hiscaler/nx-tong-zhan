<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$baseUrl = Yii::$app->getRequest()->getBaseUrl();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body ontouchstart>
        <?php $this->beginBody() ?>
        <div class="container" id="container">
            <div class="page__hd">
                宁乡统战信息管理系统
            </div>
            <div class="page__bd">
                <?= $content ?>
            </div>
            <div class="page__ft">
                
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
