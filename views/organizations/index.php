<?php
/* @var $this yii\web\View */

$this->title = '商会组织';

$formatter = Yii::$app->getFormatter();
$baseUrl = Yii::$app->getRequest()->getBaseUrl();
?>

<div class="weui-search-bar" id="searchBar">
    <form action="" class="weui-search-bar__form">
        <div class="weui-search-bar__box">
            <i class="weui-icon-search"></i>
            <input type="search" class="weui-search-bar__input" id="searchInput" name="keyword" placeholder="请输入您要搜索的关键词" required="" value="<?= $keyword ?>">
            <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>
        </div>
    </form>
    <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>
</div>

<div class="page__bd_spacing page__bd_vertical_spacing">
    <?php if ($items): ?>
        <div class="people-weui-cell">
            <?php foreach ($items as $item): ?>
                <div class="weui-cell">
                    <div class="weui-cell__hd" style="position: relative;margin-right: 10px;">

                    </div>
                    <div class="weui-cell__bd">
                        <p>
                            <a href="<?= \yii\helpers\Url::toRoute(['organizations/view', 'id' => $item['id'], 'type' => $item['type']]) ?>"><?= $item['name'] ?></a>
                        </p>
                        <p style="font-size: 13px;color: #888888;">

                        </p>
                    </div>

                </div>
                <div class="weui-form-preview">
                    <div class="weui-form-preview__bd">
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">详细地址</label>
                            <span class="weui-form-preview__value"><?= $item['address'] ?></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">法人姓名</label>
                            <span class="weui-form-preview__value"><?= $item['legal_person_username'] ?></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">法人工作单位及职务</label>
                            <span class="weui-form-preview__value"><?= $item['legal_person_work_information'] ?></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">联系方式</label>
                            <span class="weui-form-preview__value"><a href="tel:<?= $item['legal_person_mobile_phone'] ?>"><?= $item['legal_person_mobile_phone'] ?></a></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">登记证书号</label>
                            <span class="weui-form-preview__value"><?= $item['certificate_number'] ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <div class="weui-msg">
            <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
            <div class="weui-msg__text-area">
                <h2 class="weui-msg__title">提示消息</h2>
                <p class="weui-msg__desc">
                    <?php
                    if (empty($username)) {
                        echo '请输入您要搜索的关键词。';
                    } else {
                        echo '未找到关键词为“' . $username . '”的数据！';
                    }
                    ?>
                </p>
            </div>
        </div>
    <?php endif; ?>

</div>