<?php
/* @var $this yii\web\View */

$this->title = $model['name'];

$formatter = Yii::$app->getFormatter();
?>

<div class="page__bd_spacing page__bd_vertical_spacing">

    <div class="weui-form-preview">
        <div class="weui-form-preview__hd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"></label>
                <em class="weui-form-preview__value"><?= $model['name'] ?></em>
            </div>
        </div>

        <div class="weui-form-preview__bd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['english_name'] ?></label>
                <span class="weui-form-preview__value"><?= $model['english_name'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['english_short_name'] ?></label>
                <span class="weui-form-preview__value"><?= $model['english_short_name'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['address'] ?></label>
                <span class="weui-form-preview__value"><?= $model['address'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['company_members_count'] ?></label>
                <span class="weui-form-preview__value"><?= $model['company_members_count'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['person_members_count'] ?></label>
                <span class="weui-form-preview__value"><?= $model['person_members_count'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['director_count'] ?></label>
                <span class="weui-form-preview__value"><?= $model['director_count'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['executive_director_count'] ?></label>
                <span class="weui-form-preview__value"><?= $model['executive_director_count'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['legal_person_username'] ?></label>
                <span class="weui-form-preview__value"><?= $model['legal_person_username'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['legal_person_mobile_phone'] ?></label>
                <span class="weui-form-preview__value"><?= $model['legal_person_mobile_phone'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['legal_person_work_information'] ?></label>
                <span class="weui-form-preview__value"><?= $model['legal_person_work_information'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['certificate_number'] ?></label>
                <span class="weui-form-preview__value"><?= $model['certificate_number'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['certificate_issue_datetime'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asDate($model['certificate_issue_datetime']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <fieldset>
                    <legend><?= $labels['intro'] ?></legend>
                    <article class="weui_article">
                        <section>
                            <div style="text-align: left;">
                                <?php
                                $parser = new \cebe\markdown\Markdown();
                                echo $parser->parse($model['intro']);
                                ?>
                            </div>
                        </section>
                    </article>
                </fieldset>
            </div>

            <?php if (!empty($referrer)): ?>
                <div class="weui-form-preview__ft">
                    <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="<?= $referrer ?>">返回上一页</a>
                </div>
            <?php endif; ?>

        </div>

    </div>

</div>