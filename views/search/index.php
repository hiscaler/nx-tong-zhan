<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = '搜索';

$formatter = Yii::$app->getFormatter();
$request = Yii::$app->getRequest();
$baseUrl = $request->getBaseUrl();

$searchUrl = ['search/index'];
$type = $request->get('type');
if ($type !== null) {
    $searchUrl['type'] = (int) $type;
}
?>

<div class="weui-search-bar" id="searchBar">
    <?php echo yii\helpers\Html::beginForm('', 'GET', ['class' => 'weui-search-bar__form']) ?>
    <div class="weui-search-bar__box">
        <i class="weui-icon-search"></i>
        <input type="search" class="weui-search-bar__input" id="searchInput" name="keyword" placeholder="请输入您要搜索的人员姓名或者身份证号码" required="" value="<?= $keyword ?>">
        <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>
    </div>
    <?php yii\helpers\Html::endForm() ?>
    <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>
</div>

<div class="page__bd_spacing page__bd_vertical_spacing">
    <?php if ($personTypes): ?>
        <div class="weui-grids">
            <?php foreach ($personTypes as $key => $value): ?>
                <a href="<?= Url::toRoute(['search/index', 'type' => $key]) ?>" class="weui-grid">
                    <div class="weui-grid__icon">
                        <img src="<?= $baseUrl ?>/images/icon-tong-xin.png" alt="">
                    </div>
                    <p class="weui-grid__label"><?= $value ?></p>
                </a>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <?php if ($items): ?>
            <div class="people-weui-cell">
                <?php foreach ($items as $item): ?>
                    <div class="weui-cell">
                        <div class="weui-cell__hd" style="position: relative;margin-right: 10px;">
                            <img src="<?= $baseUrl . ($item['photo'] ?: '/images/avatar-' . ($item['sex'] ? 'male' : 'female') . '.png') ?>" style="width:50px;height:50px;display:block;border-radius:50px">
                            <span class="weui-badge" style="position: absolute;top: -.4em;right: -.4em;"><?= $formatter->asSex($item['sex']) ?></span>
                        </div>
                        <div class="weui-cell__bd">
                            <p>
                                <a href="<?= Url::toRoute(['people/view', 'id' => $item['id'], 'type' => $item['type']]) ?>"><?= $item['username'] ?></a>
                            </p>
                            <p style="font-size: 13px;color: #888888;">
                                [ <?= $item['region_name'] ?> ] <?= $formatter->asPersonType($item['type']) ?>
                            </p>
                        </div>

                    </div>
                    <div class="weui-form-preview">
                        <div class="weui-form-preview__bd">
                            <div class="weui-form-preview__item">
                                <label class="weui-form-preview__label">身份证号码</label>
                                <span class="weui-form-preview__value"><?= $item['id_card_number'] ?></span>
                            </div>
                            <div class="weui-form-preview__item">
                                <label class="weui-form-preview__label">籍贯</label>
                                <span class="weui-form-preview__value"><?= $item['native_place'] ?></span>
                            </div>
                            <div class="weui-form-preview__item">
                                <label class="weui-form-preview__label">联系方式</label>
                                <span class="weui-form-preview__value"><a href="tel:<?= $item['mobile_phone'] ?>"><?= $item['mobile_phone'] ?></a></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php else: ?>
            <div class="weui-msg">
                <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
                <div class="weui-msg__text-area">
                    <h2 class="weui-msg__title">提示消息</h2>
                    <p class="weui-msg__desc">
                        <?php
                        if (empty($username)) {
                            echo '请输入您要搜索的人员姓名或者身份证号码（支持模糊搜索）。';
                        } else {
                            echo '未找到关键词为“' . $username . '”的人员数据！';
                        }
                        ?>
                    </p>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

</div>