<?php

use yii\helpers\Url;

$baseUrl = Yii::$app->getRequest()->getBaseUrl()
?>

<div class="page__bd_spacing page__bd_vertical_spacing">

    <div id="positions">
        <div class="weui-grids">
            <?php foreach ($items as $key => $value): ?>
                <a href="<?= Url::toRoute(['positions/index', 'by' => 'category', 'value' => $key]) ?>" class="weui-grid">
                    <div class="weui-grid__icon">
                        <img src="<?= $baseUrl ?>/images/icon-tong-xin-<?= $key ?>.png" alt="">
                    </div>
                    <p class="weui-grid__label"><?= $value ?></p>
                </a>
            <?php endforeach; ?>
        </div>
    </div>

</div>