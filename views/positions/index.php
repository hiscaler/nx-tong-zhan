<?php

use yii\helpers\Url;

$baseUrl = Yii::$app->getRequest()->getBaseUrl()
?>


<div class="page__bd_spacing page__bd_vertical_spacing">

    <div id="positions">
        <div class="weui-grids">
            <a href="<?= Url::toRoute(['positions/index', 'by' => 'area']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-tong-xin.png" alt="">
                </div>
                <p class="weui-grid__label">四同创建点</p>
            </a>
            <a href="<?= Url::toRoute(['demonstration-plots-1035/index']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-1035.png" alt="">
                </div>
                <p class="weui-grid__label">凝心聚力十三五行动示范点</p>
            </a>
            <a href="<?= Url::toRoute(['group-organizations/index']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-group-organization.png" alt="">
                </div>
                <p class="weui-grid__label">统战团体组织</p>
            </a>
            <a href="<?= Url::toRoute(['business-organizations/index']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-business-organization.png" alt="">
                </div>
                <p class="weui-grid__label">商会组织</p>
            </a>
            <a href="<?= Url::toRoute(['religion-activity-places/index']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-religion-activity-place.png" alt="">
                </div>
                <p class="weui-grid__label">宗教活动场所</p>
            </a>
            <a href="<?= Url::toRoute(['demonstration-plots-other/index']) ?>" class="weui-grid">
                <div class="weui-grid__icon">
                    <img src="<?= $baseUrl ?>/images/icon-other.png" alt="">
                </div>
                <p class="weui-grid__label">其他</p>
            </a>
        </div>
    </div>

</div>