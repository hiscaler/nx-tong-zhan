<?php
/* @var $this yii\web\View */

$this->title = $model['name'];

$formatter = Yii::$app->getFormatter();
?>

<div class="page__bd_spacing page__bd_vertical_spacing">

    <div class="weui-form-preview">
        <div class="weui-form-preview__hd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"></label>
                <em class="weui-form-preview__value"><?= $model['name'] ?></em>
            </div>
        </div>

        <div class="weui-form-preview__bd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['address'] ?></label>
                <span class="weui-form-preview__value"><?= $model['address'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['elements'] ?></label>
                <span class="weui-form-preview__value"><?= $model['elements'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['county_level_awarding_datetime'] ?></label>
                <span class="weui-form-preview__value"><?= date('Y/m/d', $model['county_level_awarding_datetime']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['city_level_awarding_datetime'] ?></label>
                <span class="weui-form-preview__value"><?= date('Y/m/d', $model['city_level_awarding_datetime']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['province_level_awarding_datetime'] ?></label>
                <span class="weui-form-preview__value"><?= date('Y/m/d', $model['province_level_awarding_datetime']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['superintendent_informations'] ?></label>
                <span class="weui-form-preview__value"><?= $model['superintendent_informations'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['superintendent_mobile_phone'] ?></label>
                <span class="weui-form-preview__value"><?= $model['superintendent_mobile_phone'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['honors_awards'] ?></label>
                <span class="weui-form-preview__value"><?= $model['honors_awards'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <fieldset>
                    <legend><?= $labels['intro'] ?></legend>
                    <article class="weui_article">
                        <section>
                            <div style="text-align: left;">
                                <?php
                                $parser = new \cebe\markdown\Markdown();
                                echo $parser->parse($model['intro']);
                                ?>
                            </div>
                        </section>
                    </article>
                </fieldset>
            </div>

            <?php if (!empty($referrer)): ?>
                <div class="weui-form-preview__ft">
                    <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="<?= $referrer ?>">返回上一页</a>
                </div>
            <?php endif; ?>

        </div>

    </div>

</div>