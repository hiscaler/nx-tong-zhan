<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="weui-msg">
    <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
    <div class="weui-msg__text-area">
        <h2 class="weui-msg__title"><?= Html::encode($this->title) ?></h2>
        <p class="weui-msg__desc">
            <?= nl2br(Html::encode($message)) ?>
        </p>
    </div>
</div>