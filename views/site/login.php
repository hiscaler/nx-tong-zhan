<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

AppAsset::register($this);

$baseUrl = Yii::$app->getRequest()->getBaseUrl();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
        <?= Html::csrfMetaTags() ?>
        <title>宁乡统战信息管理系统用户登录</title>
        <link href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/css/login.css" rel="stylesheet">
        <?php $this->head() ?>
    </head>
    <body ontouchstart>
        <?php $this->beginBody() ?>
        <div class="container" id="container">
            <div class="page-login">
                <div class="form-login">
                    <div class="hd">
                        成员登录<span>宁乡统战用户登录</span>
                    </div>
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="cells">
                        <div class="cell">
                            <div class="cell__hd"><label class="username"></label></div>
                            <div class="cell__bd">
                                <?php echo $form->field($model, 'username')->textInput(['class' => 'input', 'placeholder' => '账号'])->label(false); ?>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="cell__hd"><label class="password"></label></div>
                            <div class="cell__bd">
                                <?php echo $form->field($model, 'password')->passwordInput(['class' => 'input', 'placeholder' => '密码'])->label(false); ?>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="cell__hd"><label class="verify-code"></label></div>
                            <div class="cell__bd">
                                <?php echo $form->field($model, 'verifyCode', ['template' => '{label}{input}<a href="#" id="btn-send-sms">发送验证码</a>{error}'])->textInput(['class' => 'input', 'placeholder' => '验证码'])->label(false); ?>
                            </div>
                        </div>
                    </div>

                    <div class="buttons">
                        <?= Html::submitButton('立即登录', ['class' => 'btn-login']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <footer id="page-ft">
                    版权所有&copy;中共宁乡市委统战部
                </footer>
            </div>

        </div>
        <?php $this->endBody() ?>
        <script type="text/javascript">

            $(function () {
                $('#btn-send-sms').click(function () {
                    var username = $.trim($('#loginform-username').val()),
                        password = $.trim($('#loginform-password').val()),
                        $this = $(this);

                    if (username && password) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= yii\helpers\Url::toRoute(['api/sms']) ?>',
                            data: {username: username, password: password},
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    var seconds = 60;
                                    var timer = setInterval(function () {
                                        seconds -= 1;
                                        document.getElementById('btn-send-sms').innerHTML = seconds + '秒';
                                        if (seconds <= 0) {
                                            document.getElementById('btn-send-sms').disabled = false;
                                            document.getElementById('btn-send-sms').innerHTML = '发送验证码';
                                            clearTimeout(timer);
                                            seconds = 60;
                                        }
                                    }, 1000);
                                } else {
                                    alert(response.error.message);
                                }
                            }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert('[ ' + XMLHttpRequest.status + ' ] ' + XMLHttpRequest.responseText);
                            }
                        });
                    } else {
                        alert('请输入帐号和密码。');
                    }

                    return false;
                }
                );
            });
        </script>
    </body>
</html>
<?php $this->endPage() ?>
<body>
