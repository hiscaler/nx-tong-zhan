<?php
/* @var $this yii\web\View */

$this->title = $model['username'];

$formatter = Yii::$app->getFormatter();
?>

<div class="page__bd_spacing page__bd_vertical_spacing">

    <div class="weui-form-preview">
        <div class="weui-form-preview__hd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"></label>
                <em class="weui-form-preview__value"><?= $model['username'] ?></em>
            </div>
        </div>

        <div class="weui-form-preview__bd">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['type'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asPersonType($model['type']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['id_card_number'] ?></label>
                <span class="weui-form-preview__value"><?= $model['id_card_number'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['sex'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asSex($model['sex']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['nation'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asNation($model['nation']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['birthday'] ?></label>
                <span class="weui-form-preview__value"><?= date('Y/m/d', $model['birthday']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['nationality'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asNationality($model['nationality']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['native_place'] ?></label>
                <span class="weui-form-preview__value"><?= $model['native_place'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['birthplace'] ?></label>
                <span class="weui-form-preview__value"><?= $model['birthplace'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['party'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asParty($model['party']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['join_party_date'] ?></label>
                <span class="weui-form-preview__value"><?= $model['join_party_date'] ? $formatter->asDate($model['join_party_date']) : '' ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['registered_residence_address'] ?></label>
                <span class="weui-form-preview__value"><?= $model['registered_residence_address'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['join_job_date'] ?></label>
                <span class="weui-form-preview__value"><?= $model['join_job_date'] ? $formatter->asDate($model['join_job_date']) : '' ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['mobile_phone'] ?></label>
                <span class="weui-form-preview__value"><?= $model['mobile_phone'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['full_time_education_level'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asEducationLevel($model['full_time_education_level']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['full_time_education_school'] ?></label>
                <span class="weui-form-preview__value"><?= $model['full_time_education_school'] ?></span>
            </div>
            <?php if ($model['in_service_education_level']): ?>
                <div class="weui-form-preview__item">
                    <label class="weui-form-preview__label"><?= $labels['in_service_education_level'] ?></label>
                    <span class="weui-form-preview__value"><?= $formatter->asEducationLevel($model['in_service_education_level']) ?></span>
                </div>
                <div class="weui-form-preview__item">
                    <label class="weui-form-preview__label"><?= $labels['in_service_education_school'] ?></label>
                    <span class="weui-form-preview__value"><?= $model['in_service_education_school'] ?></span>
                </div>
            <?php endif; ?>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $profileLabels['country'] ?></label>
                <span class="weui-form-preview__value"><?= $profile['country'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $profileLabels['type'] ?></label>
                <span class="weui-form-preview__value"><?= $formatter->asStudyAbroadPersonType($profile['type']) ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $profileLabels['school'] ?></label>
                <span class="weui-form-preview__value"><?= $profile['school'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['professional_title'] ?></label>
                <span class="weui-form-preview__value"><?= $model['professional_title'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['work_information'] ?></label>
                <span class="weui-form-preview__value"><?= $model['work_information'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['social_title'] ?></label>
                <span class="weui-form-preview__value"><?= $model['social_title'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label"><?= $labels['political_arrangements'] ?></label>
                <span class="weui-form-preview__value"><?= $model['political_arrangements'] ?></span>
            </div>
            <div class="weui-form-preview__item">
                <fieldset>
                    <legend><?= $labels['intro'] ?></legend>
                    <article class="weui_article">
                        <section>
                            <div style="text-align: left;">
                                <?php
                                $parser = new \cebe\markdown\Markdown();
                                echo $parser->parse($model['intro']);
                                ?>
                            </div>
                        </section>
                    </article>
                </fieldset>
            </div>

            <?php if ($model['familySocialRelations']): ?>
                <div class="family-social-relations">
                    <fieldset>
                        <legend>家庭主要成员及社会关系</legend>
                        <div class="weui-form-preview">
                            <div class="weui-form-preview__bd">
                                <?php foreach ($model['familySocialRelations'] as $relation): ?>
                                    <div class="weui-form-preview__item">
                                        <label class="weui-form-preview__label">称谓</label>
                                        <span class="weui-form-preview__value"><?= $relation['title'] ?></span>
                                    </div>
                                    <div class="weui-form-preview__item">
                                        <label class="weui-form-preview__label">姓名</label>
                                        <span class="weui-form-preview__value"><?= $relation['username'] ?></span>
                                    </div>
                                    <div class="weui-form-preview__item">
                                        <label class="weui-form-preview__label">出生年月</label>
                                        <span class="weui-form-preview__value"><?= $relation['birthday'] ? date('Y.m.d', $relation['birthday']) : '' ?></span>
                                    </div>
                                    <div class="weui-form-preview__item">
                                        <label class="weui-form-preview__label">党派</label>
                                        <span class="weui-form-preview__value"><?= $formatter->asParty($relation['party']) ?></span>
                                    </div>
                                    <div class="weui-form-preview__item">
                                        <label class="weui-form-preview__label">工作单位及职务</label>
                                        <span class="weui-form-preview__value"><?= $relation['work_information'] ?></span>
                                    </div>
                                    </br>
                                <?php endforeach; ?>

                            </div>

                        </div>
                    </fieldset>

                </div>
            <?php endif; ?>

            <?php if (!empty($referrer)): ?>
                <div class="weui-form-preview__ft">
                    <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="<?= $referrer ?>">返回上一页</a>
                </div>
            <?php endif; ?>

        </div>

    </div>

</div>