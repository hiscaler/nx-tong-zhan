
<div class="page__bd_spacing">
    <div class="weui-cells__title">用户资料</div>
    <div class="weui-cells">
        <div class="weui-cell">
            <div class="weui-cell__bd"><p>用户名</p></div>
            <div class="weui-cell__ft"><?= $identity->username ?></div>
        </div>
        <?php if ($identity->region): ?>
            <div class="weui-cell">
                <div class="weui-cell__bd"><p>所属区域</p></div>
                <div class="weui-cell__ft"><?= $identity->region->name ?></div>
            </div>
        <?php endif ?>
        <div class="weui-cell">
            <div class="weui-cell__bd"><p>注册时间</p></div>
            <div class="weui-cell__ft"><?= date('Y-m-d', $identity->created_at) ?></div>
        </div>
    </div>
    <div class="weui-cells">
        <a class="weui-cell weui-cell_access" href="<?= yii\helpers\Url::toRoute(['change-password']) ?>">
            <div class="weui-cell__bd">
                <p>修改密码</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>
        <a class="weui-cell weui-cell_access" href="<?= yii\helpers\Url::toRoute(['statistics/index']) ?>">
            <div class="weui-cell__bd">
                <p>数据统计</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>

    </div>
    <a class="weui-btn weui-btn_primary" href="<?= yii\helpers\Url::toRoute(['site/logout']) ?>" style="margin-top: 10px;">退出</a>
</div>
