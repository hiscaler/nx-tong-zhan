<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->params['breadcrumbs'][] = Yii::t('app', 'Change Password');
$session = Yii::$app->getSession();
?>
<div class="page__bd_spacing">
    <?php
    if ($session->hasFlash('notice')):
        ?>
        <div class="weui-msg">
            <div class="weui-msg__icon-area"><i class="weui-icon-success weui-icon_msg"></i></div>
            <div class="weui-msg__text-area">
                <h2 class="weui-msg__title">提示消息</h2>
                <p class="weui-msg__desc">
                    <?= $session->getFlash('notice') ?>
                </p>
            </div>
        </div>
    <?php else: ?>
        <?=
        $this->render('_changePasswordForm', [
            'user' => $user,
            'model' => $model,
        ]);
        ?>
    <?php endif; ?>

</div>