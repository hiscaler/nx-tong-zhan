<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="weui-cells weui-cells_form">

    <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">用户名</label></div>
        <div class="weui-cell__bd">
            <?= $form->field($user, 'username')->textInput(['maxlength' => true, 'readonly' => 'readonly', 'class' => 'weui-input'])->label(false) ?>
        </div>
    </div>

    <div class="weui-cell">
        <div class="weui-cell__hd"><label for="" class="weui-label">密码</label></div>
        <div class="weui-cell__bd">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'weui-input'])->label(false) ?>
        </div>
    </div>
    <div class="weui-cell">
        <div class="weui-cell__hd"><label for="" class="weui-label">确认密码</label></div>
        <div class="weui-cell__bd">
            <?= $form->field($model, 'confirmPassword')->passwordInput(['maxlength' => true, 'class' => 'weui-input'])->label(false) ?>
        </div>
    </div>

</div>

<div class="clearfix" style="margin-top: 10px;">
    <?= Html::submitButton('修改密码', ['class' => 'weui-btn weui-btn_primary']) ?>
</div>

<?php ActiveForm::end(); ?>



