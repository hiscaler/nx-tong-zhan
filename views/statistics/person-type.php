<div class="weui-cells" style="margin-bottom: 40px;">
    <?php foreach ($items as $key => $item): ?>
        <a class="weui-cell weui-cell_access" href="<?= \yii\helpers\Url::toRoute(['search/index', 'type' => $key]) ?>">
            <div class="weui-cell__bd">
                <p><?= $item['name'] ?></p>
            </div>
            <div class="weui-cell__ft"><?= isset($item['count']) ? $item['count'] . ' 人' : '无' ?></div>
        </a>
    <?php endforeach; ?>
</div>