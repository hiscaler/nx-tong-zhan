<div id="chart" style="width: 100%;height:400px; margin: 0 auto;"></div>


<?php app\components\JsBlock::begin() ?>
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('chart'));
    var option = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
            {
                name: '访问来源',
                type: 'pie',
                radius: ['20%', '33%'],
                data: <?= yii\helpers\Json::encode($data) ?>
            }
        ]
    };


    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
</script>

<?php app\components\JsBlock::end() ?>