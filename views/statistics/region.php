<div class="weui-cells" style="margin-bottom: 40px;">
    <?php foreach ($items as $item): ?>
        <a class="weui-cell weui-cell_access" href="<?= \yii\helpers\Url::toRoute(['statistics/person-type', 'regionId' => $item['id']]) ?>">
            <div class="weui-cell__bd">
                <p><?= $item['name'] ?></p>
            </div>
            <div class="weui-cell__ft"><?= isset($item['count']) ? $item['count'] . ' 人' : '无' ?></div>
        </a>
    <?php endforeach; ?>
</div>