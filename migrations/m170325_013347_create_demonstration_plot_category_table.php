<?php

use yii\db\Migration;

/**
 * 示范点类型关联数据
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170325_013347_create_demonstration_plot_category_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%demonstration_plot_category}}', [
            'demonstration_plot_id' => $this->integer()->notNull()->comment('示范点编号'),
            'category_id' => $this->integer()->notNull()->comment('类型编号'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%demonstration_plot_category}}');
    }

}
