<?php

use yii\db\Migration;

/**
 * 组织信息登记表类型关联数据
 */
class m170326_085401_create_organization_category_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%organization_category}}', [
            'organization_id' => $this->integer()->notNull()->comment('组织编号'),
            'category_id' => $this->integer()->notNull()->comment('类型编号'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%organization_category}}');
    }

}
