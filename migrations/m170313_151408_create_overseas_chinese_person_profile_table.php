<?php

use yii\db\Migration;

/**
 * 华侨（归侨、侨眷）登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170313_151408_create_overseas_chinese_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%overseas_chinese_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'leave_date' => $this->string(20)->comment('何时出境'),
            'living_place' => $this->string(100)->comment('旅居地'),
            'return_date' => $this->string(20)->comment('归国时间'),
            'live_address' => $this->string(60)->comment('现居住地址'),
            'tel' => $this->string(20)->comment('固定电话'),
            'speciality' => $this->string()->comment('熟悉专长或专攻方向')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%overseas_chinese_person_profile}}');
    }

}
