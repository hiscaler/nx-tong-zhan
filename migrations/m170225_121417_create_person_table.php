<?php

use yii\db\Migration;

/**
 * 人员信息基础表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170225_121417_create_person_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%person}}', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0)->comment('类型'),
            'category' => $this->smallInteger()->notNull()->defaultValue(0)->comment('类别'),
            'username' => $this->string(12)->notNull()->comment('姓名'),
            'sex' => $this->boolean()->notNull()->defaultValue(0)->comment('性别'),
            'nation' => $this->smallInteger()->notNull()->defaultValue(0)->comment('民族'),
            'birthday' => $this->integer()->notNull()->comment('出生年月'),
            'nationality' => $this->smallInteger()->notNull()->defaultValue(0)->comment('国籍'),
            'native_place' => $this->string(20)->notNull()->comment('籍贯'),
            'birthplace' => $this->string(20)->comment('出生地点'),
            'party' => $this->smallInteger()->notNull()->defaultValue(0)->comment('党派'),
            'join_party_date' => $this->integer()->comment('加入党派年月'),
            'registered_residence_address' => $this->string(60)->comment('户籍所在地'),
            'join_job_date' => $this->integer()->comment('参加工作时间'),
            'mobile_phone' => $this->string(50)->comment('移动电话'),
            'id_card_number' => $this->string(18)->comment('身份证号码'),
            'photo' => $this->string(100)->comment('照片'),
            'full_time_education_level' => $this->smallInteger()->notNull()->defaultValue(0)->comment('全日制教育学历学位'),
            'full_time_education_school' => $this->string(100)->comment('毕业院校系及专业'),
            'in_service_education_level' => $this->smallInteger()->notNull()->defaultValue(0)->comment('在职教育学历学位'),
            'in_service_education_school' => $this->string(100)->comment('毕业院校系及专业'),
            'professional_title' => $this->string(255)->comment('职称（职级）'),
            'work_information' => $this->string(255)->comment('工作单位及职务'),
            'social_title' => $this->string(255)->comment('主要社会职务'),
            'intro' => $this->text()->comment('个人简历及获奖情况'),
            'political_arrangements' => $this->text()->comment('政治安排'),
            'remark' => $this->text()->comment('备注'),
            'enabled' => $this->boolean()->notNull()->defaultValue(1)->comment('激活'),
            'region_id' => $this->integer()->notNull()->comment('所属区域'),
            'created_at' => $this->integer()->notNull()->comment('添加时间'),
            'created_by' => $this->integer()->notNull()->comment('添加人'),
            'updated_at' => $this->integer()->notNull()->comment('更新时间'),
            'updated_by' => $this->integer()->notNull()->comment('更新人'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%person}}');
    }

}
