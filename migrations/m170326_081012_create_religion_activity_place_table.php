<?php

use yii\db\Migration;

/**
 * 宗教活动场所信息登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170326_081012_create_religion_activity_place_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%religion_activity_place}}', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->notNull()->defaultValue(0)->comment('所属乡镇'),
            'name' => $this->string(30)->notNull()->comment('场所名称'),
            'address' => $this->string(100)->notNull()->comment('详细地址'),
            'staff_people_count' => $this->smallInteger()->notNull()->defaultValue(0)->comment('教职人员数'),
            'other_people_count' => $this->smallInteger()->notNull()->defaultValue(0)->comment('其他人员数'),
            'superintendent_informations' => $this->string(100)->notNull()->comment('负责人姓名及职务'),
            'superintendent_mobile_phone' => $this->string(12)->notNull()->comment('联系方式'),
            'certificate_number' => $this->string(30)->comment('登记证书号'),
            'certificate_issue_datetime' => $this->integer()->comment('发证日期'),
            'historical_relic_protected_level' => $this->string(30)->comment('文物保护级别'),
            'intro' => $this->text()->comment('场所情况简介'),
            'input_region_id' => $this->boolean()->notNull()->defaultValue(1)->comment('录入单位'),
            'enabled' => $this->boolean()->notNull()->defaultValue(1)->comment('激活'),
            'created_at' => $this->integer()->notNull()->comment('添加时间'),
            'created_by' => $this->integer()->notNull()->comment('添加人'),
            'updated_at' => $this->integer()->notNull()->comment('更新时间'),
            'updated_by' => $this->integer()->notNull()->comment('更新人'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%religion_activity_place}}');
    }

}
