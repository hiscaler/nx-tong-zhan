<?php

use yii\db\Migration;

/**
 * 宗教活动站所类型关联数据
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170326_084539_create_religion_activity_place_category_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%religion_activity_place_category}}', [
            'place_id' => $this->integer()->notNull()->comment('活动场所编号'),
            'category_id' => $this->integer()->notNull()->comment('类型编号'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%religion_activity_place_category}}');
    }

}
