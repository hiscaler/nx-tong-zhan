<?php

use yii\db\Migration;

/**
 * Handles the creation of table `religion_person_profile`.
 */
class m170312_113524_create_religion_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%religion_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'position_certificate_number' => $this->string(60)->comment('教职证书及编号'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%religion_person_profile}}');
    }

}
