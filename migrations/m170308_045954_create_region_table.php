<?php

use yii\db\Migration;

/**
 * 区域管理
 *
 */
class m170308_045954_create_region_table extends Migration
{

    public function up()
    {
        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(60)->notNull(),
            'name' => $this->string(30)->notNull(),
            'parent_id' => $this->integer()->notNull()->defaultValue(0),
            'level' => $this->smallInteger()->notNull()->defaultValue(0),
            'parent_ids' => $this->string(100),
            'parent_names' => $this->string(255),
            'ordering' => $this->smallInteger()->notNull()->defaultValue(0),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%region}}');
    }

}
