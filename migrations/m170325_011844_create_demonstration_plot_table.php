<?php

use yii\db\Migration;

/**
 * 示范点
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170325_011844_create_demonstration_plot_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%demonstration_plot}}', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->notNull()->defaultValue(1)->comment('类型'),
            'region_id' => $this->integer()->notNull()->defaultValue(0)->comment('所属乡镇（街道）、园区'),
            'name' => $this->string(30)->notNull()->comment('创建点名称'),
            'address' => $this->string(100)->notNull()->comment('详细地址'),
            'elements' => $this->string(200)->notNull()->comment('统战元素'),
            'county_level_awarding_datetime' => $this->integer()->comment('县级授牌时间'),
            'city_level_awarding_datetime' => $this->integer()->comment('市级授牌时间'),
            'province_level_awarding_datetime' => $this->integer()->comment('省级授牌时间'),
            'superintendent_informations' => $this->string(100)->notNull()->comment('负责人姓名及职务'),
            'superintendent_mobile_phone' => $this->string(12)->notNull()->comment('联系方式'),
            'honors_awards' => $this->text()->comment('所获荣誉奖励'),
            'intro' => $this->text()->notNull()->comment('创建工作情况简介'),
            'input_region_id' => $this->boolean()->notNull()->defaultValue(1)->comment('录入单位'),
            'enabled' => $this->boolean()->notNull()->defaultValue(1)->comment('激活'),
            'created_at' => $this->integer()->notNull()->comment('添加时间'),
            'created_by' => $this->integer()->notNull()->comment('添加人'),
            'updated_at' => $this->integer()->notNull()->comment('更新时间'),
            'updated_by' => $this->integer()->notNull()->comment('更新人'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%demonstration_plot}}');
    }

}
