<?php

use yii\db\Migration;

/**
 * 非公有制经济代表人士登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170312_124249_create_nonpublic_economic_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%nonpublic_economic_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'evaluation_date' => $this->string()->comment('综合评价时间'),
            'evaluation_level' => $this->string()->comment('综合评价等级'),
            'enterprise_intro' => $this->string()->comment('公司情况介绍'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%nonpublic_economic_person_profile}}');
    }

}
