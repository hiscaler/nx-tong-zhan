<?php

use yii\db\Migration;

/**
 * 台胞台属登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170312_152513_create_tai_wan_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%tai_wan_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'tel' => $this->string(20)->comment('固定电话'),
            'live_address' => $this->string(60)->comment('现居住地'),
            'speciality' => $this->string()->comment('熟悉专长或专攻方向')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%tai_wan_person_profile}}');
    }

}
