<?php

use yii\db\Migration;

/**
 * 民主党派组织
 */
class m170420_132835_create_democratic_party_organization_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%democratic_party_organization}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull()->comment('名称'),
            'found_datetime' => $this->integer()->notNull()->comment('成立时间'),
            'address' => $this->string(100)->notNull()->comment('办公（活动）场所'),
            'people_count' => $this->smallInteger()->notNull()->defaultValue(0)->comment('党（会）员人数'),
            'honour_reward' => $this->string()->comment('所获荣誉奖励'),
            'superintendent_username' => $this->string(16)->notNull()->comment('负责人姓名'),
            'superintendent_mobile_phone' => $this->string(12)->notNull()->comment('联系方式'),
            'superintendent_work_information' => $this->string(200)->notNull()->comment('负责人工作单位及职务'),
            'political_arrangements' => $this->string()->comment('政治安排'),
            'intro' => $this->text()->comment('简介'),
            'input_region_id' => $this->boolean()->notNull()->defaultValue(1)->comment('录入单位'),
            'enabled' => $this->boolean()->notNull()->defaultValue(1)->comment('激活'),
            'created_at' => $this->integer()->notNull()->comment('添加时间'),
            'created_by' => $this->integer()->notNull()->comment('添加人'),
            'updated_at' => $this->integer()->notNull()->comment('更新时间'),
            'updated_by' => $this->integer()->notNull()->comment('更新人'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%democratic_party_organization}}');
    }

}
