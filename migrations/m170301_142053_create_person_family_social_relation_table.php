<?php

use yii\db\Migration;

/**
 * 家庭主要成员及社会关系
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170301_142053_create_person_family_social_relation_table extends Migration
{

    public function up()
    {
        $this->createTable('{{%person_family_social_relation}}', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->notNull()->defaultValue(0)->comment('类型'),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'title' => $this->string(10)->notNull()->comment('称谓'),
            'username' => $this->string(10)->notNull()->comment('姓名'),
            'birthday' => $this->integer()->comment('出生年月'),
            'party' => $this->smallInteger()->notNull()->defaultValue(0)->comment('党派'),
            'work_information' => $this->string(255)->comment('工作单位及职务'),
            'tel' => $this->string(255)->comment('联系电话'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%person_family_social_relation}}');
    }

}
