<?php

use yii\db\Migration;

/**
 * 出国和归国留学人员信息登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170312_132418_create_study_abroad_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%study_abroad_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'country' => $this->string(10)->notNull()->comment('留学国家'),
            'school' => $this->string(100)->notNull()->comment('留学学校'),
            'type' => $this->smallInteger()->notNull()->defaultValue(0)->comment('留学类别'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%study_abroad_person_profile}}');
    }

}
