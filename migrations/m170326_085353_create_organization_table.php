<?php

use yii\db\Migration;

/**
 * 组织信息登记表
 */
class m170326_085353_create_organization_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%organization}}', [
            'id' => $this->primaryKey(),
            'ordering' => $this->integer()->notNull()->defaultValue(0)->comment('排序'),
            'type' => $this->smallInteger()->notNull()->defaultValue(1)->comment('类别'),
            'name' => $this->string(100)->notNull()->comment('中文名称'),
            'english_name' => $this->string(160)->notNull()->comment('英文名称'),
            'english_short_name' => $this->string(60)->notNull()->comment('英文缩写'),
            'address' => $this->string(100)->notNull()->comment('住所'),
            'company_members_count' => $this->integer()->notNull()->defaultValue(0)->comment('单位会员数'),
            'person_members_count' => $this->integer()->notNull()->defaultValue(0)->comment('个人会员数'),
            'director_count' => $this->integer()->notNull()->defaultValue(0)->comment('理事数'),
            'executive_director_count' => $this->integer()->notNull()->defaultValue(0)->comment('常务理事数'),
            'legal_person_username' => $this->string(20)->notNull()->comment('法定代表人姓名'),
            'legal_person_mobile_phone' => $this->string(12)->notNull()->comment('联系方式'),
            'legal_person_work_information' => $this->string(200)->notNull()->comment('法定代表人工作单位及职务'),
            'certificate_number' => $this->string(30)->comment('登记证书号'),
            'certificate_issue_datetime' => $this->integer()->comment('发证日期'),
            'intro' => $this->text()->comment('商会概况'),
            'input_region_id' => $this->boolean()->notNull()->defaultValue(1)->comment('录入单位'),
            'enabled' => $this->boolean()->notNull()->defaultValue(1)->comment('激活'),
            'created_at' => $this->integer()->notNull()->comment('添加时间'),
            'created_by' => $this->integer()->notNull()->comment('添加人'),
            'updated_at' => $this->integer()->notNull()->comment('更新时间'),
            'updated_by' => $this->integer()->notNull()->comment('更新人'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%organization}}');
    }

}
