<?php

use yii\db\Migration;

/**
 * 香港同胞、澳门同胞登记表
 *
 * @author hiscaler <hiscaler@gmail.com>
 */
class m170312_135105_create_hk_macao_person_profile_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%hk_macao_person_profile}}', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull()->comment('人员 id'),
            'main_live_address' => $this->string(100)->comment('现主要居住地'),
            'tel' => $this->string(20)->comment('固定电话'),
            'speciality' => $this->string(255)->comment('熟悉专业有何特长'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%hk_macao_person_profile}}');
    }

}
