<?php

namespace app\widgets;

use yii\base\Widget;

class Tabbar extends Widget
{

    public function run()
    {
        $controllerId = $this->view->context->id;
        $items = [
            [
                'label' => '首页',
                'url' => ['search/index'],
                'icon' => 'icon-homepage.png',
                'active' => $controllerId == 'search',
            ],
            [
                'label' => '统计',
                'url' => ['statistics/index'],
                'icon' => 'icon-statistics.png',
                'active' => $controllerId == 'statistics',
            ],
            [
                'label' => '统战阵地',
                'url' => ['positions/index'],
                'icon' => 'icon_tabbar.png',
                'active' => $controllerId == 'positions',
            ],
            [
                'label' => '用户中心',
                'url' => ['my/index'],
                'icon' => 'icon-user-center.png',
                'active' => $controllerId == 'my',
            ],
        ];

        return $this->render('Tabbar', [
                'items' => $items,
        ]);
    }

}
