<?php $baseUrl = Yii::$app->getRequest()->getBaseUrl(); ?>
<div class="tabbar">
    <div class="weui-tabbar">
        <?php foreach ($items as $item): ?>
            <a href="<?= \yii\helpers\Url::toRoute($item['url']) ?>" class="weui-tabbar__item<?= (isset($item['active']) && $item['active']) ? ' weui-bar__item_on' : '' ?>">
                <img src="<?= $baseUrl ?>/images/<?= $item['icon'] ?>" alt="" class="weui-tabbar__icon">
                <p class="weui-tabbar__label"><?= $item['label'] ?></p>
            </a>
        <?php endforeach; ?>
    </div>
</div>